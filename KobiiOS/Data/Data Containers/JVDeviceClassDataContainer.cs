using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVDeviceClassDataContainer : JVDataContainer
	{
		public const int MaxNumDeviceClasses = 6;

		private static int IndexIDIter = 0;

		// Events to tell Table Data Containers when Device Classes are added or removed
		public delegate void OnAddedDelegate();
		public event OnAddedDelegate OnAdded;
		public delegate void OnRemovedDelegate(int index);
		public event OnRemovedDelegate OnRemoved;

		public List<JVDeviceClassData> DeviceClassDataList { get; private set; }
		public int Count
		{
			get
			{
				return DeviceClassDataList.Count;
			}
		}

		public JVDeviceClassDataContainer()
		{
			DeviceClassDataList = new List<JVDeviceClassData>();
		}

		public JVDeviceClassDataContainer( JVDeviceClassDataContainer original ) : this()
		{
			if( original.DeviceClassDataList != null && original.DeviceClassDataList.Count > 0 )
			{
				for( int i = 0; i < original.DeviceClassDataList.Count; i++ )
				{
					JVDeviceClassData data = new JVDeviceClassData(original.DeviceClassDataList[i]);
					Add(data);
				}
			}
		}

		public void MigrateData( JVDeviceClassDataContainer migratingData )
		{
			if( migratingData.DeviceClassDataList != null && migratingData.DeviceClassDataList.Count > 0 )
			{
				int internalIter = 0;

				List<int> toRemove = new List<int>();
				List<JVDeviceClassData> isEqual = new List<JVDeviceClassData>();

				for( int migrationIter = 0; migrationIter < migratingData.Count; migrationIter++ )
				{
					for( ; internalIter < Count; internalIter++ )
					{
						HashSet<int> conflicts = new HashSet<int>();
						if( migratingData.GetData(migrationIter).IsEqualToOther(GetData(internalIter), conflicts) )
						{
							internalIter++;
							isEqual.Add(migratingData.GetData(migrationIter));
							break;
						}
						else
						{
							if( conflicts.Contains((int)JVUserData.ConflictTypes.IndexID) )
							{
								toRemove.Add(internalIter);
							}
							else
							{
								internalIter++;
								SetData(migrationIter, migratingData.GetData(migrationIter));
								isEqual.Add(migratingData.GetData(migrationIter));
								break;
							}
						}
					}
				}

				if( Count > migratingData.Count )
				{
					for( ; internalIter < Count; internalIter++ )
					{
						toRemove.Add(internalIter);
					}
				}

				for( int i = 0; i < toRemove.Count; i++ )
				{
					Remove(toRemove[i]);
				}
				for( int i = 0; i < migratingData.Count; i++ )
				{
					if( !isEqual.Contains(migratingData.DeviceClassDataList[i]) )
					{
						Add(migratingData.DeviceClassDataList[i]);
					}
				}
			}
		}

		public override bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVDeviceClassDataContainer) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVDeviceClassDataContainer other = (JVDeviceClassDataContainer)otherData;

				if( DeviceClassDataList.Count != other.DeviceClassDataList.Count )
				{
					return false;
				}

				for( int i = 0; i < DeviceClassDataList.Count; i++ )
				{
					JVDeviceClassData thisData = DeviceClassDataList[i];
					JVDeviceClassData oData = other.DeviceClassDataList[i];

					if( !thisData.IsEqualToOther(oData, conflicts) )
					{
						isEqual = false;
					}
				}

				return isEqual;
			}
			return false;
		}

		public bool Add( JVDeviceClassData data )
		{
			if( data != null )
			{
				if( data.IndexID == -1 )
				{
					data.IndexID = IndexIDIter++;
				}

				DeviceClassDataList.Add(data);
				if( OnAdded != null )
				{
					OnAdded();
				}
				return true;
			}
			return false;
		}

		public bool Add( string name, UIImage icon )
		{
			if( DeviceClassDataList.Count < MaxNumDeviceClasses && !string.IsNullOrEmpty(name) && icon != null )
			{
				JVDeviceClassData data = new JVDeviceClassData(name, icon);
				return Add(data);
			}
			return false;
		}

		public bool Remove( int index )
		{
			if( index < DeviceClassDataList.Count )
			{
				DeviceClassDataList.RemoveAt(index);
				if( OnRemoved != null )
				{
					OnRemoved(index);
				}
				return true;
			}
			return false;
		}

		public List<JVDeviceClassData> GetDataArray( HashSet<int> indexSet )
		{
			List<JVDeviceClassData> returnList = new List<JVDeviceClassData>();

			for( int i = 0; i < DeviceClassDataList.Count; i++ )
			{
				if( indexSet.Contains(i) )
				{
					returnList.Add(DeviceClassDataList[i]);
				}
			}
			return returnList;
		}

		public JVDeviceClassData GetData(int index)
		{
			if( index < DeviceClassDataList.Count )
			{
				return DeviceClassDataList[index];
			}
			return null;
		}

		public void SetData( int index, JVDeviceClassData deviceClassData )
		{
			DeviceClassDataList[index] = deviceClassData;
		}
	}
}

