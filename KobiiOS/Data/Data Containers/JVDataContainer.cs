using System;
using System.Collections.Generic;

namespace KobiiOS
{
	public abstract class JVDataContainer
	{
		public virtual bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( otherData == null )
			{
				conflicts.Add( JVData.BasicConflict );
				return false;
			}
			return true;
		}
	}
}

