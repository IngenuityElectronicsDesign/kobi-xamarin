﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVKobiBoxDataContainer : JVDataContainer
	{
		public const int MaxNumKobiBoxes = 6;

		private static int IndexIDIter = 0;

		public List<JVKobiBoxData> KobiBoxDataList { get; private set; }
		public int Count
		{
			get
			{
				return KobiBoxDataList.Count;
			}
		}

		public JVKobiBoxDataContainer()
		{
			KobiBoxDataList = new List<JVKobiBoxData>();
		}

		public JVKobiBoxDataContainer( JVKobiBoxDataContainer original ) : this()
		{
			if( original.KobiBoxDataList != null && original.KobiBoxDataList.Count > 0 )
			{
				for( int i = 0; i < original.KobiBoxDataList.Count; i++ )
				{
					JVKobiBoxData data = new JVKobiBoxData(original.KobiBoxDataList[i]);
					Add(data);
				}
			}
		}

		public void MigrateData( JVKobiBoxDataContainer migratingData )
		{
			if( migratingData.KobiBoxDataList != null && migratingData.KobiBoxDataList.Count > 0 )
			{
				int internalIter = 0;

				List<int> toRemove = new List<int>();
				List<JVKobiBoxData> isEqual = new List<JVKobiBoxData>();

				for( int migrationIter = 0; migrationIter < migratingData.Count; migrationIter++ )
				{
					for( ; internalIter < Count; internalIter++ )
					{
						HashSet<int> conflicts = new HashSet<int>();
						if( migratingData.GetData(migrationIter).IsEqualToOther(GetData(internalIter), conflicts) )
						{
							internalIter++;
							isEqual.Add(migratingData.GetData(migrationIter));
							break;
						}
						else
						{
							if( conflicts.Contains((int)JVUserData.ConflictTypes.IndexID) )
							{
								toRemove.Add(internalIter);
							}
							else
							{
								internalIter++;
								SetData(migrationIter, migratingData.GetData(migrationIter));
								isEqual.Add(migratingData.GetData(migrationIter));
								break;
							}
						}
					}
				}

				if( Count > migratingData.Count )
				{
					for( ; internalIter < Count; internalIter++ )
					{
						toRemove.Add(internalIter);
					}
				}

				for( int i = 0; i < toRemove.Count; i++ )
				{
					Remove(toRemove[i]);
				}
				for( int i = 0; i < migratingData.Count; i++ )
				{
					if( !isEqual.Contains(migratingData.KobiBoxDataList[i]) )
					{
						Add(migratingData.KobiBoxDataList[i]);
					}
				}
			}
		}

		public override bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVKobiBoxDataContainer) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVKobiBoxDataContainer other = (JVKobiBoxDataContainer)otherData;

				if( KobiBoxDataList.Count != other.KobiBoxDataList.Count )
				{
					return false;
				}

				for( int i = 0; i < KobiBoxDataList.Count; i++ )
				{
					JVKobiBoxData thisData = KobiBoxDataList[i];
					JVKobiBoxData oData = other.KobiBoxDataList[i];

					if( !thisData.IsEqualToOther(oData, conflicts) )
					{
						isEqual = false;
					}
				}

				return isEqual;
			}
			return false;
		}

		public bool Add( JVKobiBoxData data )
		{
			if( data != null )
			{
				if( data.IndexID == -1 )
				{
					data.IndexID = IndexIDIter++;
				}

				KobiBoxDataList.Add(data);
				return true;
			}
			return false;
		}

		public bool Add( string name, int uuid, JVKobiPortData port1, JVKobiPortData port2 )
		{
			if( KobiBoxDataList.Count < MaxNumKobiBoxes && !string.IsNullOrEmpty(name) && uuid != -1 && port1 != null && port2 != null )
			{
				JVKobiBoxData data = new JVKobiBoxData(name, uuid, port1, port2);
				return Add(data);
			}
			return false;
		}

		public bool Remove( int index )
		{
			if( index < KobiBoxDataList.Count )
			{
				KobiBoxDataList.RemoveAt(index);
				return true;
			}
			return false;
		}

		public List<JVKobiBoxData> GetDataArray( HashSet<int> indexSet )
		{
			List<JVKobiBoxData> returnList = new List<JVKobiBoxData>();

			for( int i = 0; i < KobiBoxDataList.Count; i++ )
			{
				if( indexSet.Contains(i) )
				{
					returnList.Add(KobiBoxDataList[i]);
				}
			}
			return returnList;
		}

		public JVKobiBoxData GetData(int index)
		{
			if( index != -1 && index < KobiBoxDataList.Count )
			{
				return KobiBoxDataList[index];
			}
			return null;
		}

		public void SetData( int index, JVKobiBoxData data )
		{
			KobiBoxDataList[index] = data;
		}
	}
}

