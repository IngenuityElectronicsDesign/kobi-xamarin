using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;

namespace KobiiOS
{
	public class JVQuotaDataContainer : JVTableDataContainer
	{
		public List<List<JVQuotaData>> QuotaDataList { get; private set; }

		public JVQuotaDataContainer()
		{
			QuotaDataList = new List<List<JVQuotaData>>();
		}

		public JVQuotaDataContainer(JVQuotaDataContainer original) : this()
		{
			if( original.QuotaDataList != null && original.QuotaDataList.Count > 0 )
			{
				for( int userIndex = 0; userIndex < original.QuotaDataList.Count; userIndex++ )
				{
					AddUser();
				}
				for( int deviceClassIndex = 0; deviceClassIndex < original.QuotaDataList[0].Count; deviceClassIndex++ )
				{
					AddDeviceClass();
				}

				for( int userIndex = 0; userIndex < original.QuotaDataList.Count; userIndex++ )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < original.QuotaDataList[userIndex].Count; deviceClassIndex++ )
					{
						JVQuotaData data = new JVQuotaData(original.QuotaDataList[userIndex][deviceClassIndex]);
						QuotaDataList[userIndex][deviceClassIndex] = data;
					}
				}
			}
		}

		#region Base Functions
		public override bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVQuotaDataContainer) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVQuotaDataContainer other = (JVQuotaDataContainer)otherData;

				if( QuotaDataList.Count <= 0 || QuotaDataList.Count != other.QuotaDataList.Count )
				{
					return false;
				}

				for( int userIndex = 0; userIndex < QuotaDataList.Count; userIndex++ )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < QuotaDataList[userIndex].Count; deviceClassIndex++ )
					{
						JVQuotaData thisData = QuotaDataList[userIndex][deviceClassIndex];
						JVQuotaData oData = other.QuotaDataList[userIndex][deviceClassIndex];

						if( !thisData.IsEqualToOther(oData, conflicts) )
						{
							isEqual = false;
						}
					}
				}

				return isEqual;
			}
			return false;
		}

		protected override bool Add( bool userEntry )
		{
			if( userEntry )
			{
				List<JVQuotaData> newList = new List<JVQuotaData>();

				if( QuotaDataList.Count > 0 && QuotaDataList[0] != null )
				{
					for( int i = 0; i < QuotaDataList[0].Count; i++ )
					{
						newList.Add( new JVQuotaData() );
					}
				}
				else
				{
					for( int i = 0; i < JVShared.Instance.DeviceClassDataContainer.Count; i++ )
					{
						newList.Add( new JVQuotaData() );
					}
				}

				QuotaDataList.Add( newList );

				return true;
			}
			else
			{
				for( int i = 0; i < QuotaDataList.Count; i++ )
				{
					if( QuotaDataList[i].Count <= JVShared.Instance.DeviceClassDataContainer.Count )
					{
						QuotaDataList[i].Add( new JVQuotaData() );
					}
				}

				return true;
			}
		}

		protected override bool Remove( bool userEntry, int index )
		{
			if( userEntry )
			{
				if( index < QuotaDataList.Count )
				{
					QuotaDataList.RemoveAt(index);
					return true;
				}
			}
			else
			{
				foreach( List<JVQuotaData> list in QuotaDataList )
				{
					if( index > list.Count )
					{
						return false;
					}
					else
					{
						list.RemoveAt(index);
					}
				}
				return true;
			}
			return false;
		}
		#endregion Base Functions

        public JVQuotaData[] GetDataArray(HashSet<int> userIndexSet, HashSet<int> deviceCLassIndexSet)
        {
			int userCount = JVShared.Instance.UserDataContainer.ChildUserDataList != null ? JVShared.Instance.UserDataContainer.ChildUserDataList.Count : 0;
			int deviceClassCount = JVShared.Instance.DeviceClassDataContainer != null ? JVShared.Instance.DeviceClassDataContainer.Count : 0;

            var returnData = new List<JVQuotaData>();
			for( int userIndex = 0; userIndex < userCount; userIndex++ )
            {
				if( userIndexSet.Contains(userIndex) )
                {
					for( int deviceClassIndex = 0; deviceClassIndex < deviceClassCount; deviceClassIndex++ )
                    {
						if( deviceCLassIndexSet.Contains(deviceClassIndex) )
                        {
                            returnData.Add(QuotaDataList[userIndex][deviceClassIndex]);
                        }
                    }
                }
            }

            return returnData.ToArray();
        }

		public JVQuotaData[] GetDataArray()
		{
			return GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet);
		}

		public JVQuotaData GetData(int userIndex, int deviceClassIndex)
		{
			if( userIndex < QuotaDataList.Count && deviceClassIndex < QuotaDataList[0].Count )
			{
				return QuotaDataList[userIndex][deviceClassIndex];
			}
			return null;
		}

		private bool IsConflicted(JVQuotaData[] dataArray, JVQuotaData.ConflictTypes type)
		{
			HashSet<int> conflicts = new HashSet<int>();

			for( int i = 0; i < dataArray.Length; i++ )
			{
				for( int j = i + 1; j < dataArray.Length; j++ )
				{
					var quota = dataArray[i];
					var otherQuota = dataArray[j];

					if( !quota.IsEqualToOther(otherQuota, conflicts) )
					{
						return conflicts.Contains((int)type);
					}
				}
			}
			return false;
		}

		#region KobiMode
        public JVQuotaData.KobiMode GetKobiMode()
        {
            var data = GetDataArray();
			if( data.Length <= 0 || IsConflicted(data, JVQuotaData.ConflictTypes.Mode) )
            {
                return JVQuotaData.KobiMode.Conflicted;
            }

            return data[0].Mode;
        }

        public void SetMode(JVQuotaData.KobiMode mode)
        {
			var data = GetDataArray();
			if( data.Length <= 0 )
            {
                return;
            }
			for( int i = 0; i < data.Length; i++ )
            {
                data[i].Mode = mode;
            }
        }
		#endregion KobiMode

		#region Weekly Quota
		public byte GetWeeklyQuota()
		{
			var data = GetDataArray();

			if( data.Length <= 0 || IsConflicted(data, JVQuotaData.ConflictTypes.WeeklyQuota) )
			{
				return 0;
			}

			return data[0].WeeklyQuota;
		}

		public void SetWeeklyQuota(byte weeklyQuota)
		{
			var data = GetDataArray();
			if( data.Length <= 0 )
			{
				return;
			}
			for( int i = 0; i < data.Length; i++ )
			{
				data[i].WeeklyQuota = weeklyQuota;
			}
		}
		#endregion Weekly Quota

		#region Daily Quota
		public byte GetDailyQuota()
		{
			var indicies = JVShared.Instance.SelectedDaysIndexSet.ToArray();
			var data = GetDataArray();

			if( data.Length <= 0 || IsConflicted(data, JVQuotaData.ConflictTypes.DailyQuotas) || data[0].IsDailyDataConflicted(indicies) )
			{
				return 0;
			}
			if( indicies.Length <= 0 )
			{
				return 1;
			}

			return data[0].DailyQuotas[indicies[0]];
		}

		public void SetDailyQuotas(byte dailyQuota)
		{
			var indicies = JVShared.Instance.SelectedDaysIndexSet.ToArray();
			var data = GetDataArray();
			if( data.Length <= 0 || indicies.Length <= 0 )
			{
				return;
			}
			for( int i = 0; i < data.Length; i++ )
			{
				for( int j = 0; j < indicies.Length; j++ )
				{
					data[i].DailyQuotas[indicies[j]] = dailyQuota;
				}
			}
		}
		#endregion Daily Quota

		#region Emergency Hours Quota
		public byte GetEmergencyHoursQuota()
		{
			var data = GetDataArray();

			if( data.Length <= 0 || IsConflicted(data, JVQuotaData.ConflictTypes.EmergencyQuota) )
			{
				return 0;
			}

			return data[0].EmergencyHoursQuota;
		}

		public void SetEmergencyHoursQuota(byte emergencyQuota)
		{
			var data = GetDataArray();
			if( data.Length <= 0 )
			{
				return;
			}
			for( int i = 0; i < data.Length; i++ )
			{
				data[i].EmergencyHoursQuota = emergencyQuota;
			}
		}
		#endregion Emergency Hours Quota
	}
}

