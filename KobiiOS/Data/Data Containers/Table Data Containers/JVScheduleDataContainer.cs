﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;

namespace KobiiOS
{
	public class JVScheduleDataContainer : JVTableDataContainer
	{
		public List<List<JVScheduleData>> ScheduleDataList { get; private set; }

		public JVScheduleDataContainer()
		{
			ScheduleDataList = new List<List<JVScheduleData>>();
		}

		public JVScheduleDataContainer(JVScheduleDataContainer original) : this()
		{
			if( original.ScheduleDataList != null && original.ScheduleDataList.Count > 0 )
			{
				for( int userIndex = 0; userIndex < original.ScheduleDataList.Count; userIndex++ )
				{
					AddUser();
				}
				for( int deviceClassIndex = 0; deviceClassIndex < original.ScheduleDataList[0].Count; deviceClassIndex++ )
				{
					AddDeviceClass();
				}

				for( int userIndex = 0; userIndex < original.ScheduleDataList.Count; userIndex++ )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < original.ScheduleDataList[0].Count; deviceClassIndex++ )
					{
						JVScheduleData data = new JVScheduleData(original.ScheduleDataList[userIndex][deviceClassIndex]);
						ScheduleDataList[userIndex][deviceClassIndex] = data;
					}
				}
			}
		}

		#region Base Functions
		public override bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVScheduleDataContainer) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVScheduleDataContainer other = (JVScheduleDataContainer)otherData;

				if( ScheduleDataList.Count <= 0 || ScheduleDataList.Count != other.ScheduleDataList.Count )
				{
					return false;
				}

				for( int userIndex = 0; userIndex < ScheduleDataList.Count; userIndex++ )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < ScheduleDataList[userIndex].Count; deviceClassIndex++ )
					{
						JVScheduleData thisData = ScheduleDataList[userIndex][deviceClassIndex];
						JVScheduleData oData = other.ScheduleDataList[userIndex][deviceClassIndex];

						if( !thisData.IsEqualToOther(oData, conflicts) )
						{
							isEqual = false;
						}
					}
				}

				return isEqual;
			}
			return false;
		}

		protected override bool Add( bool userEntry )
		{
			if( userEntry )
			{
				List<JVScheduleData> newList = new List<JVScheduleData>();
				ScheduleDataList.Add( newList );

				if( ScheduleDataList[0] != null )
				{
					for( int i = 0; i < ScheduleDataList[0].Count; i++ )
					{
						newList.Add( new JVScheduleData() );
					}
				}
				else
				{
					for( int i = 0; i < JVShared.Instance.DeviceClassDataContainer.Count; i++ )
					{
						newList.Add( new JVScheduleData() );
					}
				}

				return true;
			}
			else
			{
				foreach( List<JVScheduleData> list in ScheduleDataList )
				{
					list.Add( new JVScheduleData() );
				}
				return true;
			}
		}

		protected override bool Remove( bool userEntry, int index )
		{
			if( userEntry )
			{
				if( index < ScheduleDataList.Count )
				{
					ScheduleDataList.RemoveAt(index);
					return true;
				}
			}
			else
			{
				foreach( List<JVScheduleData> list in ScheduleDataList )
				{
					if( index > list.Count )
					{
						return false;
					}
					else
					{
						list.RemoveAt(index);
					}
				}
				return true;
			}
			return false;
		}
		#endregion Base Functions

		public JVScheduleData[] GetDataArray(HashSet<int> userIndexSet, HashSet<int> deviceClassIndexSet)
		{
			int userCount = JVShared.Instance.UserDataContainer.ChildUserDataList != null ? JVShared.Instance.UserDataContainer.ChildUserDataList.Count : 0;
			int deviceClassCount = JVShared.Instance.DeviceClassDataContainer != null ? JVShared.Instance.DeviceClassDataContainer.Count : 0;

			var returnData = new List<JVScheduleData>();
			for( int userIndex = 0; userIndex < userCount; userIndex++ )
			{
				if( userIndexSet.Contains(userIndex) )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < deviceClassCount; deviceClassIndex++ )
					{
						if( deviceClassIndexSet.Contains(deviceClassIndex) )
						{
							returnData.Add(ScheduleDataList[userIndex][deviceClassIndex]);
						}
					}
				}
			}

			return returnData.ToArray();
		}

		public JVScheduleData[] GetDataArray()
		{
			return GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet);
		}

		public JVScheduleData GetData(int userIndex, int deviceClassIndex)
		{
			if( userIndex < ScheduleDataList.Count && deviceClassIndex < ScheduleDataList[0].Count )
			{
				return ScheduleDataList[userIndex][deviceClassIndex];
			}
			return null;
		}

		private bool IsConflicted(JVScheduleData[] dataArray, JVScheduleData.ConflictTypes type)
		{
			HashSet<int> conflicts = new HashSet<int>();

			for( int i = 0; i < dataArray.Length; i++ )
			{
				for( int j = i + 1; j < dataArray.Length; j++ )
				{
					var schedule = dataArray[i];
					var otherSchedule = dataArray[j];

					if( !schedule.IsEqualToOther(otherSchedule, conflicts) )
					{
						return conflicts.Contains((int)type);
					}
				}
			}
			return false;
		}

		#region Daily Schedule
		public byte GetDailySchedule(int userIndex, int deviceIndex, int dayIndex, int blockIndex, bool startTime)
		{
			if( userIndex < ScheduleDataList.Count && deviceIndex < ScheduleDataList[userIndex].Count && dayIndex < ScheduleDataList[userIndex][deviceIndex].DailyBlockData.Count )
			{
				return (startTime ? ScheduleDataList[userIndex][deviceIndex].DailyBlockData[dayIndex][blockIndex].StartTime : ScheduleDataList[userIndex][deviceIndex].DailyBlockData[dayIndex][blockIndex].EndTime);
			}
			return 0;
		}

		public byte GetDailySchedule(int blockIndex, bool startTime)
		{
			var indicies = JVShared.Instance.SelectedDaysIndexSet.ToArray();
			var data = GetDataArray();

			if( data.Length <= 0 || IsConflicted(data, JVScheduleData.ConflictTypes.DailySchedule) || data[0].IsDailyDataConflicted(indicies, blockIndex, startTime) )
			{
				return 0;
			}
			if( indicies.Length <= 0 )
			{
				return 1;
			}

			return (startTime ? data[0].DailyBlockData[indicies[0]][blockIndex].StartTime : data[0].DailyBlockData[indicies[0]][blockIndex].EndTime);
		}

		public void SetDailySchedule(byte dailySchedule, int blockIndex, bool startTime)
		{
			var indicies = JVShared.Instance.SelectedDaysIndexSet.ToArray();
			var data = GetDataArray();
			if( data.Length <= 0 || indicies.Length <= 0 )
			{
				return;
			}
			for( int i = 0; i < data.Length; i++ )
			{
				for( int j = 0; j < indicies.Length; j++ )
				{
					if( startTime )
					{
						data[i].DailyBlockData[indicies[j]][blockIndex].StartTime = dailySchedule;
					}
					else
					{
						data[i].DailyBlockData[indicies[j]][blockIndex].EndTime = dailySchedule;
					}
				}
			}
		}
		#endregion Daily Schedule

//		public byte GetWeeklyScheduleTime(int blockIndex, bool startTime)
//		{
//			var data = GetDataArray();
//			if( data.Length <= 0 || IsConflicted(data, JVScheduleBlockData.ConflictTypes.StartTime))
//		}

		#region KobiMode
//		public JVQuotaData.KobiMode GetKobiMode()
//		{
//			var data = GetDataArray();
//			if( data.Length <= 0 || IsConflicted(data, JVScheduleData.ConflictTypes.MondayFirstBlock) )
//			{
//				return JVQuotaData.KobiMode.Conflicted;
//			}
//
//			return data[0].Mode;
//		}
//
//		public void SetMode(JVQuotaData.KobiMode mode)
//		{
//			var data = GetDataArray();
//			if( data.Length <= 0 )
//			{
//				return;
//			}
//			for( int i = 0; i < data.Length; i++ )
//			{
//				data[i].Mode = mode;
//			}
//		}
		#endregion KobiMode
	}
}

