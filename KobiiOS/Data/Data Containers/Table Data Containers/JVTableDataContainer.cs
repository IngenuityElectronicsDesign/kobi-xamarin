using System;
using System.Collections.Generic;
using MonoTouch.Foundation;

namespace KobiiOS
{
	public abstract class JVTableDataContainer : JVDataContainer
	{
		public JVTableDataContainer()
		{
			JVShared.Instance.UserDataContainer.OnAdded += AddUser;
			JVShared.Instance.UserDataContainer.OnRemoved += RemoveUser;
			JVShared.Instance.DeviceClassDataContainer.OnAdded += AddDeviceClass;
			JVShared.Instance.DeviceClassDataContainer.OnRemoved += RemoveDeviceClass;
		}

		~JVTableDataContainer()
		{
			JVShared.Instance.UserDataContainer.OnAdded -= AddUser;
			JVShared.Instance.UserDataContainer.OnRemoved -= RemoveUser;
			JVShared.Instance.DeviceClassDataContainer.OnAdded -= AddDeviceClass;
			JVShared.Instance.DeviceClassDataContainer.OnRemoved -= RemoveDeviceClass;
		}

		#region List Handling
		protected void AddUser()
		{
			Add(true);
		}

		protected void AddDeviceClass()
		{
			Add(false);
		}

		protected virtual bool Add( bool userEntry )
		{
			Console.WriteLine("Warning JVTableDataContainer.cs: Add not overloaded.");
			return false;
		}

		protected void RemoveUser( int index )
		{
			Remove(true, index);
		}

		protected void RemoveDeviceClass( int index )
		{
			Remove(false, index);
		}

		protected virtual bool Remove( bool userEntry, int index )
		{
			Console.WriteLine("Warning JVTableDataContainer.cs: Remove not overloaded.");
			return false;
		}
		#endregion List Handling
	}
}

