using System;
using System.Collections.Generic;
using MonoTouch.Foundation;

namespace KobiiOS
{
	public class JVUsageDataContainer : JVTableDataContainer
	{
		public List<List<JVUsageData>> UsageDataList { get; private set; }
		public int Count
		{
			get
			{
				return UsageDataList.Count;
			}
		}

		public JVUsageDataContainer()
		{
			UsageDataList = new List<List<JVUsageData>>();
		}

		public JVUsageDataContainer(JVUsageDataContainer original) : this()
		{
			if( original.UsageDataList != null && original.UsageDataList.Count > 0 )
			{
				for( int userIndex = 0; userIndex < original.UsageDataList.Count; userIndex++ )
				{
					AddUser();
				}
				for( int deviceClassIndex = 0; deviceClassIndex < original.UsageDataList[0].Count; deviceClassIndex++ )
				{
					AddDeviceClass();
				}

				for( int userIndex = 0; userIndex < original.UsageDataList.Count; userIndex++ )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < original.UsageDataList[userIndex].Count; deviceClassIndex++ )
					{
						JVUsageData data = new JVUsageData(original.UsageDataList[userIndex][deviceClassIndex]);
						UsageDataList[userIndex][deviceClassIndex] = data;
					}
				}
			}
		}

		#region Base Functions
		public override bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVUsageDataContainer) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVUsageDataContainer other = (JVUsageDataContainer)otherData;

				if( UsageDataList.Count <= 0 || UsageDataList.Count != other.UsageDataList.Count )
				{
					return false;
				}

				for( int userIndex = 0; userIndex < UsageDataList.Count; userIndex++ )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < UsageDataList[userIndex].Count; deviceClassIndex++ )
					{
						JVUsageData thisData = UsageDataList[userIndex][deviceClassIndex];
						JVUsageData oData = other.UsageDataList[userIndex][deviceClassIndex];

						if( !thisData.IsEqualToOther(oData, conflicts) )
						{
							isEqual = false;
						}
					}
				}

				return isEqual;
			}
			return false;
		}

		protected override bool Add( bool userEntry )
		{
			if( userEntry )
			{
				List<JVUsageData> newList = new List<JVUsageData>();
				UsageDataList.Add( newList );
				for( int i = 0; i < JVShared.Instance.DeviceClassDataContainer.Count; i++ )
				{
					newList.Add( new JVUsageData() );
				}
				return true;
			}
			else
			{
				foreach( List<JVUsageData> list in UsageDataList )
				{
					list.Add( new JVUsageData() );
				}
				return true;
			}
		}

		protected override bool Remove( bool userEntry, int index )
		{
			if( userEntry )
			{
				if( index < UsageDataList.Count )
				{
					UsageDataList.RemoveAt(index);
					return true;
				}
			}
			else
			{
				foreach( List<JVUsageData> list in UsageDataList )
				{
					if( index > list.Count )
					{
						return false;
					}
					else
					{
						list.RemoveAt(index);
					}
				}
				return true;
			}
			return false;
		}
		#endregion Base Functions

		public JVUsageData[] GetDataArray(HashSet<int> userIndexSet, HashSet<int> deviceClassIndexSet)
		{
			var userCount = JVShared.Instance.UserDataContainer.ChildUserDataList != null ? JVShared.Instance.UserDataContainer.ChildUserDataList.Count : 0;
			var deviceClassCount = JVShared.Instance.DeviceClassDataContainer != null ? JVShared.Instance.DeviceClassDataContainer.Count : 0;

			var returnData = new List<JVUsageData>();
			for( int userIndex = 0; userIndex < userCount; userIndex++ )
			{
				if( userIndexSet.Contains(userIndex) )
				{
					for( int deviceClassIndex = 0; deviceClassIndex < deviceClassCount; deviceClassIndex++ )
					{
						if( deviceClassIndexSet.Contains(deviceClassIndex) )
						{
							returnData.Add(UsageDataList[userIndex][deviceClassIndex]);
						}
					}
				}
			}

			return returnData.ToArray();
		}

		public JVUsageData[] GetDataArray()
		{
			return GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet);
		}

		public JVUsageData GetData(int userIndex, int deviceClassIndex)
		{
			if( userIndex < UsageDataList.Count && deviceClassIndex < UsageDataList[0].Count )
			{
				return UsageDataList[userIndex][deviceClassIndex];
			}
			return null;
		}

		private bool IsConflicted(JVUsageData[] dataArray, JVUsageData.ConflictTypes type)
		{
			HashSet<int> conflicts = new HashSet<int>();

			for( int i = 0; i < dataArray.Length; i++ )
			{
				for( int j = i + 1; j < dataArray.Length; j++ )
				{
					var Usage = dataArray[i];
					var otherUsage = dataArray[j];

					if( !Usage.IsEqualToOther(otherUsage, conflicts) )
					{
						return conflicts.Contains((int)type);
					}
				}
			}
			return false;
		}
	}
}

