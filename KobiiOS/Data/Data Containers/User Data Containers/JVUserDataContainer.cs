using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVUserDataContainer : JVDataContainer
	{
		public const int MaxNumAdminUsers = 2;
		public const int MaxNumChildUsers = 4;
		public const int MaxNumTotalUsers = MaxNumAdminUsers+MaxNumChildUsers;

		private static int IndexIDIter = 0;

		// Events to tell Table Data Containers when Users are added or removed
		public delegate void OnAddedDelegate();
		public event OnAddedDelegate OnAdded;
		public delegate void OnRemovedDelegate(int index);
		public event OnRemovedDelegate OnRemoved;

		public List<JVUserData> AdminUserDataList { get; private set; }
		public List<JVUserData> ChildUserDataList { get; private set; }
		public int Count
		{
			get
			{
				return AdminUserDataList.Count + ChildUserDataList.Count;
			}
		}

		public JVUserDataContainer()
		{
			AdminUserDataList = new List<JVUserData>();
			ChildUserDataList = new List<JVUserData>();
		}

		public JVUserDataContainer( JVUserDataContainer original ) : this()
		{
			if( original.AdminUserDataList != null && original.AdminUserDataList.Count > 0 )
			{
				for( int i = 0; i < original.AdminUserDataList.Count; i++ )
				{
					JVUserData data = new JVUserData(original.AdminUserDataList[i]);
					Add(data);
				}
			}
			if( original.ChildUserDataList != null && original.ChildUserDataList.Count > 0 )
			{
				for( int i = 0; i < original.ChildUserDataList.Count; i++ )
				{
					JVUserData data = new JVUserData(original.ChildUserDataList[i]);
					Add(data);
				}
			}
		}

		public void MigrateData( JVUserDataContainer migratingData )
		{
			if( migratingData.AdminUserDataList != null && migratingData.ChildUserDataList != null )
			{
				for( int a = 0; a < 2; a++ )
				{
					int internalIter = 0;
					bool isAdmin = (a == 0 ? true : false);

					List<JVUserData> toRemove = new List<JVUserData>();
					List<JVUserData> isEqual = new List<JVUserData>();

					int count = (isAdmin ? AdminUserDataList.Count : ChildUserDataList.Count );
					int migrationCount = (isAdmin ? migratingData.AdminUserDataList.Count : migratingData.ChildUserDataList.Count );

					for( int migrationIter = 0; migrationIter < migrationCount; migrationIter++ )
					{
						for( ; internalIter < count; internalIter++ )
						{
							HashSet<int> conflicts = new HashSet<int>();
							if( migratingData.GetData(migrationIter, isAdmin).IsEqualToOther(GetData(internalIter, isAdmin), conflicts) )
							{
								internalIter++;
								isEqual.Add(migratingData.GetData(migrationIter, isAdmin));
								break;
							}
							else
							{
								if( conflicts.Contains((int)JVUserData.ConflictTypes.IndexID) )
								{
									toRemove.Add(GetData(internalIter, isAdmin));
								}
								else
								{
									internalIter++;
									SetUserData(migrationIter, isAdmin, migratingData.GetData(migrationIter, isAdmin));
									isEqual.Add(migratingData.GetData(migrationIter, isAdmin));
									break;
								}
							}
						}
					}

					if( count > migrationCount )
					{
						for( ; internalIter < count; internalIter++ )
						{
							toRemove.Add(GetData(internalIter, isAdmin));
						}
					}

					for( int i = 0; i < toRemove.Count; i++ )
					{
						Remove(toRemove[i]);
					}
					for( int i = 0; i < migrationCount; i++ )
					{
						if( !isEqual.Contains(migratingData.GetData(i, isAdmin)) )
						{
							Add(migratingData.GetData(i, isAdmin));
						}
					}
				}
			}
		}

		public override bool IsEqualToOther( JVDataContainer otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVUserDataContainer) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVUserDataContainer other = (JVUserDataContainer)otherData;

				if( Count != other.Count || ChildUserDataList.Count != other.ChildUserDataList.Count )
				{
					return false;
				}

				for( int i = 0; i < AdminUserDataList.Count; i++ )
				{
					JVUserData thisData = AdminUserDataList[i];
					JVUserData oData = other.AdminUserDataList[i];

					if( !thisData.IsEqualToOther(oData, conflicts) )
					{
						isEqual = false;
					}
				}
				for( int i = 0; i < ChildUserDataList.Count; i++ )
				{
					JVUserData thisData = ChildUserDataList[i];
					JVUserData oData = other.ChildUserDataList[i];

					if( !thisData.IsEqualToOther(oData, conflicts) )
					{
						isEqual = false;
					}
				}

				return isEqual;
			}
			return false;
		}

		public bool Add( JVUserData userData )
		{
			if( userData != null )
			{
				if( userData.IndexID == -1 )
				{
					userData.IndexID = IndexIDIter++;
				}

				if( userData.IsAdmin )
				{
					AdminUserDataList.Add(userData);
				}
				else
				{
					ChildUserDataList.Add(userData);
					if( OnAdded != null )
					{
						OnAdded();
					}
				}
				return true;
			}
			return false;
		}

		public bool Add( bool isAdmin, string name, UIImage portrait, JVUserData.UserTintColor colorIndex, string pinCodeString )
		{
			if( isAdmin && AdminUserDataList.Count >= MaxNumAdminUsers )
			{
				return false;
			}
			else if( !isAdmin && ChildUserDataList.Count >= MaxNumChildUsers )
			{
				return false;
			}

			if( !string.IsNullOrEmpty(name) && portrait != null && !string.IsNullOrEmpty(pinCodeString) )
			{
				Add( new JVUserData(isAdmin, name, portrait, colorIndex, pinCodeString) );
				return true;
			}
			return false;
		}

		public bool Remove( JVUserData userData )
		{
			int index = -1;
			if( userData.IsAdmin )
			{
				index = AdminUserDataList.IndexOf(userData);
				AdminUserDataList.Remove(userData);
			}
			else
			{
				index = ChildUserDataList.IndexOf(userData);
				ChildUserDataList.Remove(userData);
				if( OnRemoved != null )
				{
					OnRemoved(index);
				}
			}
			return true;
		}

		public bool Remove( bool isAdmin, int index )
		{
			if( isAdmin )
			{
				AdminUserDataList.RemoveAt(index);
			}
			else
			{
				ChildUserDataList.RemoveAt(index);
				if( OnRemoved != null )
				{
					OnRemoved(index);
				}
			}
			return true;
		}

		public List<JVUserData> GetAllData()
		{
			List<JVUserData> retList = new List<JVUserData>();

			foreach( JVUserData userData in AdminUserDataList )
			{
				retList.Add(userData);
			}
			foreach( JVUserData userData in ChildUserDataList )
			{
				retList.Add(userData);
			}

			return retList;
		}

		public JVUserData GetData( int index )
		{
			if( index >= AdminUserDataList.Count + ChildUserDataList.Count )
			{
				Console.WriteLine("Error getting user: Index %d out of range.", index);
				return null;
			}

			if( index < AdminUserDataList.Count )
			{
				return AdminUserDataList[index];
			}
			else
			{
				return ChildUserDataList[index-AdminUserDataList.Count];
			}
		}

		public int GetIndex( JVUserData userData )
		{
			for( int i = 0; i < ChildUserDataList.Count; i++ )
			{
				if( userData == ChildUserDataList[i] )
				{
					return i;
				}
			}
			return -1;
		}

		public JVUserData GetData( int index, bool fromAdmin )
		{
			if( fromAdmin && index < AdminUserDataList.Count )
			{
				return AdminUserDataList[index];
			}
			else if( !fromAdmin && index < ChildUserDataList.Count )
			{
				return ChildUserDataList[index];
			}
			return null;
		}

		public void SetUserData( int index, bool fromAdmin, JVUserData userData )
		{
			if( fromAdmin && index < AdminUserDataList.Count )
			{
				AdminUserDataList[index] = userData;
			}
			else if( !fromAdmin && index < ChildUserDataList.Count )
			{
				ChildUserDataList[index] = userData;
			}
		}

		public bool GetIsNameUsed(JVUserData toTest)
		{
			HashSet<int> conflicts = new HashSet<int>();

			foreach( JVUserData userData in GetAllData() )
			{
				conflicts.Clear();

				userData.IsEqualToOther(toTest, conflicts);

				if( !conflicts.Contains((int)JVUserData.ConflictTypes.Name) )
				{
					return true;
				}
			}
			return false;
		}

		public bool GetIsTintColorUsed(JVUserData.UserTintColor tintColor)
		{
			HashSet<int> hashSet = new HashSet<int>();

			foreach( JVUserData userData in ChildUserDataList )
			{
				hashSet.Add((int)userData.TintColorIndex);
			}

			return hashSet.Contains((int)tintColor);
		}

		public bool GetIsPinUsed(JVPinCodeData toTest)
		{
			HashSet<int> conflicts = new HashSet<int>();

			foreach( JVUserData userData in GetAllData() )
			{
				conflicts.Clear();

				userData.IsEqualToOther(new JVUserData(toTest.GetPin()), conflicts);

				if( !conflicts.Contains((int)JVUserData.ConflictTypes.Pin) )
				{
					return true;
				}
			}
			return false;
		}
	}
}

