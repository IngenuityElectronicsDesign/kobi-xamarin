using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVShared
	{
		#region Const Defs
		// Icon Layout Defs
		public const int TitleBarHeight = 60;
		public const int IconHeight = 128;
		public const int IconWidth = 96;
		public const int IconEdgePadding = 43;
		public const int IconHorizSpacing = 42;
		public const int NumOfDays = 7;

		// Notification Strings
		public const string SharedDataNeedsRefresh = "sharedDataNeedsRefresh";
		#endregion Consts

		#region Properties
		// System
		public bool IsWidescreen { get; private set; }
		public UIStoryboard Storyboard { get; private set; }
		private string[] DayNamesByIndex;

		// Color
		public UIColor SystemTintColor { get; private set; }
		public UIColor SystemErrorTintColor { get; private set; }
		private UIColor[] UserTintColorsArray;

		// Users
		public JVUserDataContainer UserDataContainer { get; private set; }
		public JVUserData LoggedInUserData { get; set; }
		public HashSet<int> SelectedUsersIndexSet { get; set; }

		// Device Classes
		public JVDeviceClassDataContainer DeviceClassDataContainer { get; private set; }
		public HashSet<int> SelectedDeviceClassesIndexSet { get; set; }

		// Kobi Global Data
		public JVKobiBoxDataContainer KobiBoxDataContainer { get; private set; }
		public JVKobiGlobalData KobiGlobalData { get; set; }
		public HashSet<int> SelectedDaysIndexSet { get; set; }

		// Table Data
		public JVQuotaDataContainer QuotaDataContainer { get; set; }
		public JVScheduleDataContainer ScheduleDataContainer { get; set; }
		public JVUsageDataContainer UsageDataContainer { get; set; }

		#endregion Properties

		#region Singleton Instance
		private static JVShared m_Instance;
		public static JVShared Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new JVShared();
				}
				return m_Instance;
			}
		}
		#endregion Singeton Instance

		private JVShared ()
		{
			// System
			IsWidescreen = false;
			if( UIScreen.MainScreen.Bounds.Size.Height == 568 )
			{
				IsWidescreen = true;
			}
			Storyboard = UIStoryboard.FromName("Main", null);
			DayNamesByIndex = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

			// Color
			SystemTintColor = new UIColor(0.31f, 0.51f, 0.71f, 1.0f);
			SystemErrorTintColor = new UIColor(1.0f, 0.19f, 0.04f, 1.0f);

			UserTintColorsArray = new UIColor[]
			{
				SystemTintColor,
				new UIColor(1.0f, 0.22f, 0.22f, 1f),		// Red
				new UIColor(1.0f, 0.58f, 0.21f, 1.0f),		// Orange
				new UIColor(1.0f, 0.79f, 0.28f, 1.0f),		// Yellow
				new UIColor(0.27f, 0.85f, 0.46f, 1.0f),		// Green
				new UIColor(0.18f, 0.67f, 0.84f, 1.0f),		// Blue
				new UIColor(0.35f, 0.35f, 0.81f, 1.0f),		// Purple
				new UIColor(1.0f, 0.17f, 0.34f, 1.0f)		// Pink
			};

			// Users
			UserDataContainer = new JVUserDataContainer();
			SelectedUsersIndexSet = new HashSet<int>();

			// Device Classes
			DeviceClassDataContainer = new JVDeviceClassDataContainer();
			SelectedDeviceClassesIndexSet = new HashSet<int>();

			// Kobi Global Settings
			KobiBoxDataContainer = new JVKobiBoxDataContainer();
			KobiGlobalData = new JVKobiGlobalData();
			SelectedDaysIndexSet = new HashSet<int>();

//			var notification = new UILocalNotification();
//			notification.FireDate = DateTime.Now.AddSeconds(5);
//			notification.AlertAction = "Kobi Needs to Refresh";
//			notification.AlertBody = "The Kobi Network has been updated, and needs to sync.";
//			notification.ApplicationIconBadgeNumber = 1;
//			notification.SoundName = UILocalNotification.DefaultSoundName;
//			UIApplication.SharedApplication.ScheduleLocalNotification(notification);
		}

		public void SetupTableDataStructures()
		{
			// Hooks onto User and Device Class Container Events - Must instantiate after
			QuotaDataContainer = new JVQuotaDataContainer();
			UsageDataContainer = new JVUsageDataContainer();
			ScheduleDataContainer = new JVScheduleDataContainer();

			// Data has to be added after so Table Data Structures receive it
			FillWithDummyData();
		}

		public void FillWithDummyData()
		{
			// Test
//			JVQuotaDataContainer otherContainer = new JVQuotaDataContainer();

			// Users
			UserDataContainer.Add(true, "Dad", UIImage.FromFile("Dad.png"), JVUserData.UserTintColor.System, "1111");

			string[] childNames = new string[] { "Johnny", "Sally", "Billy" };
			UIImage[] childPortraits = new UIImage[] { UIImage.FromFile("Johnny.png"), UIImage.FromFile("Sally.png"), UIImage.FromFile("Billy.png") };
			JVUserData.UserTintColor[] childColors = new JVUserData.UserTintColor[] { JVUserData.UserTintColor.Orange, JVUserData.UserTintColor.Purple, JVUserData.UserTintColor.Red };
			string[] childPins = new string[] { "2222", "3333", "4444" };

			for( int i = 0; i < childNames.Length; i++ )
			{
				UserDataContainer.Add(false, childNames[i], childPortraits[i], childColors[i], childPins[i]);
			}

			SelectedUsersIndexSet.Add(0);
			SelectedUsersIndexSet.Add(1);

			// Device Classes
			string[] deviceNames = new string[] { "TV", "Games", "Phone", "Laptop" };
			UIImage[] devicePortraits = new UIImage[] { UIImage.FromFile("TV.png"), UIImage.FromFile("Controller.png"), UIImage.FromFile("Speakers.png"), UIImage.FromFile("Laptop.png") };

			for( int i = 0; i < deviceNames.Length; i++ )
			{
				DeviceClassDataContainer.Add(deviceNames[i],devicePortraits[i]);
			}

			SelectedDeviceClassesIndexSet.Add(0);

			// Kobi Boxes
			string[] boxNames = new string[] { "Johnny", "Living Room" };
			int[] boxUUIDs = new int[] { 1234, 5678 };
			JVKobiPortData[] port1s = new JVKobiPortData[] { new JVKobiPortData("TV", 0), new JVKobiPortData("TV", 0) };
			JVKobiPortData[] port2s = new JVKobiPortData[] { new JVKobiPortData("Xbox 360", 1), new JVKobiPortData("Family Laptop", 3) };

			for( int i = 0; i < 2; i++ )
			{
				KobiBoxDataContainer.Add(boxNames[i], boxUUIDs[i], port1s[i], port2s[i]);
			}
		}

		public string GetDayName( int index )
		{
			if( index < NumOfDays && index < DayNamesByIndex.Length )
			{
				return DayNamesByIndex[index];
			}
			return null;
		}

		public UIColor GetUserTintColor()
		{
			return GetUserTintColor(LoggedInUserData.TintColorIndex);
		}

		public UIColor GetUserTintColor( JVUserData.UserTintColor color )
		{
			if( (int)color < UserTintColorsArray.Length )
			{
				return UserTintColorsArray[(int)color];
			}
			return null;
		}
	}

	public static class JVTimeHandler
	{
		public const int MinuteRounding = 15;

		public static float RoundNumber(float number, float to)
		{
			if( number >= 0 )
			{
				return (float)(to * Math.Floor(number / to + 0.5f));
			}
			else
			{
				return (float)(to * Math.Ceiling(number / to - 0.5f));
			}
		}

		public static byte GetQuotaChar(int hours, int mins)
		{
			return (byte)((hours * 4) + (((float)mins/60) * 4));
		}

		public static void GetHoursAndMins(ref int hours, ref int mins, byte quota)
		{
			hours = quota / 4;
			mins = (quota % 4) * MinuteRounding;
		}

		public static float GetHoursAsDecimal(byte quota)
		{
			return (quota / 4) + (0.25f * (quota % 4));
		}
	}
}

