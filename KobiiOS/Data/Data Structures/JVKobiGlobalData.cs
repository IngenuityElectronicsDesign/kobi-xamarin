using System;
using System.Collections.Generic;

namespace KobiiOS
{
	public class JVKobiGlobalData : JVData
	{
		public enum ConflictTypes
		{
			QuotaTimePeriod = 0
		}

		public bool QuotaTimePeriodIsDaily { get; set; }

		public JVKobiGlobalData()
		{
			QuotaTimePeriodIsDaily = false;
		}

		public JVKobiGlobalData(JVKobiGlobalData original) : this()
		{
			QuotaTimePeriodIsDaily = original.QuotaTimePeriodIsDaily;
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVKobiGlobalData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVKobiGlobalData other = (JVKobiGlobalData)otherData;

				if( QuotaTimePeriodIsDaily != other.QuotaTimePeriodIsDaily )
				{
					conflicts.Add((int)ConflictTypes.QuotaTimePeriod);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}

