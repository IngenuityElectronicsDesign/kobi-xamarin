﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVKobiBoxData : JVData
	{
		public enum ConflictTypes
		{
			Name = 0,
			UUID,
			Port1,
			Port2,
			IndexID
		}

		public const int MaxNameLength = 12;

		public string Name { get; set; }
		public int UUID { get; set; }
		public JVKobiPortData Port1 { get; set; }
		public JVKobiPortData Port2 { get; set; }
		public int IndexID { get; set; }

		public JVKobiBoxData()
		{
			Name = "";
			UUID = -1;
			Port1 = null;
			Port2 = null;
			IndexID = -1;
		}

		public JVKobiBoxData( string name, int uuid, JVKobiPortData port1, JVKobiPortData port2 )
		{
			Name = name;
			UUID = uuid;
			Port1 = port1;
			Port2 = port2;
			IndexID = -1;
		}

		public JVKobiBoxData( JVKobiBoxData original )
		{
			Name = original.Name;
			UUID = original.UUID;
			Port1 = original.Port1;
			Port2 = original.Port2;
			IndexID = original.IndexID;
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVKobiBoxData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVKobiBoxData other = (JVKobiBoxData)otherData;

				if( Name != other.Name )
				{
					conflicts.Add((int)ConflictTypes.Name);
					isEqual = false;
				}
				if( UUID != other.UUID )
				{
					conflicts.Add((int)ConflictTypes.UUID);
					isEqual = false;
				}
				if( Port1 != other.Port1 )
				{
					conflicts.Add((int)ConflictTypes.Port1);
					isEqual = false;
				}
				if( Port2 != other.Port2 )
				{
					conflicts.Add((int)ConflictTypes.Port2);
					isEqual = false;
				}
				if( IndexID == -1 || IndexID != other.IndexID )
				{
					conflicts.Add((int)ConflictTypes.IndexID);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}

		public JVKobiPortData GetPort(int index)
		{
			if( index == 1 )
			{
				return Port1;
			}
			else if( index == 2 )
			{
				return Port2;
			}
			return null;
		}

		public void SetPort(int index, JVKobiPortData data)
		{
			if( index == 1 )
			{
				Port1 = data;
			}
			else if( index == 2 )
			{
				Port2 = data;
			}
		}
	}
}

