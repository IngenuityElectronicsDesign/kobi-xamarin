using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVUserData : JVData
	{
		public enum ConflictTypes
		{
			Admin = 0,
			Name,
			Portrait,
			Color,
			Pin,
			IndexID
		}

		public enum UserTintColor
		{
			System,
			Red,
			Orange,
			Yellow,
			Green,
			Blue,
			Purple,
			Pink
		};

		public const int MaxNameLength = 12;

		public bool IsAdmin { get; set; }
		public string Name { get; set; }
		public UIImage Portrait { get; set; }
		public UserTintColor TintColorIndex { get; set; }
		public JVPinCodeData PinCode { get; set; }
		public int IndexID { get; set; }

		public JVUserData()
		{
			PinCode = new JVPinCodeData();
			IndexID = -1;
		}

		public JVUserData( string pinCodeString )
		{
			PinCode = new JVPinCodeData(pinCodeString);
			IndexID = -1;
		}

		public JVUserData( bool isAdmin, string name, UIImage portrait, UserTintColor colorIndex, string pinCodeString )
		{
			IsAdmin = isAdmin;
			Name = name;
			Portrait = portrait;
			TintColorIndex = colorIndex;
			PinCode = new JVPinCodeData(pinCodeString);
			IndexID = -1;
		}

		public JVUserData( JVUserData original )
		{
			IsAdmin = original.IsAdmin;
			Name = original.Name;
			Portrait = original.Portrait;
			TintColorIndex = original.TintColorIndex;
			PinCode = new JVPinCodeData(original.PinCode.GetPin());
			IndexID = original.IndexID;
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVUserData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVUserData other = (JVUserData)otherData;

				if( IsAdmin != other.IsAdmin )
				{
					conflicts.Add((int)ConflictTypes.Admin);
					isEqual = false;
				}
				if( Name != other.Name )
				{
					conflicts.Add((int)ConflictTypes.Name);
					isEqual = false;
				}
				if( Portrait != other.Portrait )
				{
					conflicts.Add((int)ConflictTypes.Portrait);
					isEqual = false;
				}
				if( TintColorIndex != other.TintColorIndex )
				{
					conflicts.Add((int)ConflictTypes.Portrait);
					isEqual = false;
				}
				if( !PinCode.IsEqualToOther(other.PinCode, conflicts) )
				{
					conflicts.Add((int)ConflictTypes.Pin);
					isEqual = false;
				}
				if( IndexID == -1 || IndexID != other.IndexID )
				{
					conflicts.Add((int)ConflictTypes.IndexID);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}

