using System;
using System.Collections.Generic;

namespace KobiiOS
{
	public class JVPinCodeData : JVData
	{
		public const int PinCodeLength = 4;

		public enum ConflictTypes
		{
			Pin = 0
		}

		private bool IsSet = false;

		public byte Pin { get; private set; }

		public JVPinCodeData()
		{
			IsSet = false;
		}

		public JVPinCodeData( string pinString )
		{
			SetPin( pinString );
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVPinCodeData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVPinCodeData other = (JVPinCodeData)otherData;

				if( Pin != other.Pin )
				{
					conflicts.Add((int)ConflictTypes.Pin);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}

		public string GetPin()
		{
			if( !IsSet )
			{
				return "- - - -";
			}

			byte[] digits = new byte[PinCodeLength];

			for( int i = 0; i < PinCodeLength; i++ )
			{
				digits[i] = (byte)(Pin >> (6-i*2));

				digits[i] = (byte)((digits[i] & 0x03) + 0x31);

				switch( digits[i] )
				{
				case (byte)0:
					{
						digits[i] = (byte)'0';
						break;
					}
				case (byte)1:
					{
						digits[i] = (byte)'1';
						break;
					}
				case (byte)2:
					{
						digits[i] = (byte)'2';
						break;
					}
				case (byte)3:
					{
						digits[i] = (byte)'3';
						break;
					}
				}
			}

			return System.Text.Encoding.Default.GetString(digits);
		}

		public void SetPin( string pinString )
		{
			// Ensure length is 4
			if( pinString.Length != PinCodeLength )
			{
				Console.WriteLine( "Length is not equal to 4" );
				return;
			}

			byte[] digits = new byte[PinCodeLength];

			// Ensure component chars are in teh range '1' to '4' (0 to 3 after subtraction)
			for( int i = 0; i < PinCodeLength; i++ )
			{
				digits[i] = (byte)(pinString.ToCharArray()[i] - (byte)0x31);

				if( digits[i] < 0 || digits[i] > 3 )
				{
					Console.WriteLine( "Digit is not between 1 and 4" );
					return;
				}
			}

			Pin = (byte)( (digits[0] << 6) | (digits[1] << 4) | (digits[2] << 2) | (digits[3] << 0 ) );

			IsSet = true;
		}
	}
}

