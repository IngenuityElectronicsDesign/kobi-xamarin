using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVUsageData : JVData
	{
		public enum ConflictTypes
		{
			WeeklyUsage = 0,
			MondayUsage,
			TuesdayUsage,
			WednesdayUsage,
			ThursdayUsage,
			FridayUsage,
			SaturdayUsage,
			SundayUsage,
			EmergencyUsage
		}

		public byte WeeklyUsage { get; set; }
		public byte[] DailyUsage { get; set; }
		public byte EmergencyUsage { get; set; }

		public JVUsageData()
		{
			WeeklyUsage = (byte)new Random().Next(5, 35);
			DailyUsage = new byte[JVShared.NumOfDays];
			for( int i = 0; i < JVShared.NumOfDays; i++ )
			{
				DailyUsage[i] = 10;
			}
			EmergencyUsage = 0;
		}

		public JVUsageData(JVUsageData original) : this()
		{
			WeeklyUsage = original.WeeklyUsage;
			DailyUsage = new byte[JVShared.NumOfDays];
			for( int i = 0; i < JVShared.NumOfDays; i++ )
			{
				DailyUsage[i] = original.DailyUsage[i];
			}
			EmergencyUsage = original.EmergencyUsage;
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVUsageData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVUsageData other = (JVUsageData)otherData;

				if( WeeklyUsage != other.WeeklyUsage )
				{
					conflicts.Add((int)ConflictTypes.WeeklyUsage);
					isEqual = false;
				}
				if( DailyUsage[0] != other.DailyUsage[0] )
				{
					conflicts.Add((int)ConflictTypes.MondayUsage);
					isEqual = false;
				}
				if( DailyUsage[1] != other.DailyUsage[1] )
				{
					conflicts.Add((int)ConflictTypes.TuesdayUsage);
					isEqual = false;
				}
				if( DailyUsage[2] != other.DailyUsage[2] )
				{
					conflicts.Add((int)ConflictTypes.WednesdayUsage);
					isEqual = false;
				}
				if( DailyUsage[3] != other.DailyUsage[3] )
				{
					conflicts.Add((int)ConflictTypes.ThursdayUsage);
					isEqual = false;
				}
				if( DailyUsage[4] != other.DailyUsage[4] )
				{
					conflicts.Add((int)ConflictTypes.FridayUsage);
					isEqual = false;
				}
				if( DailyUsage[5] != other.DailyUsage[5] )
				{
					conflicts.Add((int)ConflictTypes.SaturdayUsage);
					isEqual = false;
				}
				if( DailyUsage[6] != other.DailyUsage[6] )
				{
					conflicts.Add((int)ConflictTypes.SundayUsage);
					isEqual = false;
				}
				if( EmergencyUsage != other.EmergencyUsage )
				{
					conflicts.Add((int)ConflictTypes.EmergencyUsage);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}

