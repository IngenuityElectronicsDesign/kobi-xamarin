﻿using System;

namespace KobiiOS
{
	public class JVScheduleBlockData : JVData
	{
		public enum ConflictTypes
		{
			StartTime = 0,
			EndTime
		}

		public byte StartTime;
		public byte EndTime;

		public JVScheduleBlockData()
		{
			StartTime = 0;
			EndTime = 0;
		}

		public JVScheduleBlockData(JVScheduleBlockData original)
		{
			StartTime = original.StartTime;
			EndTime = original.EndTime;
		}

		public override bool IsEqualToOther(JVData otherData, System.Collections.Generic.HashSet<int> conflicts)
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVScheduleBlockData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVScheduleBlockData other = (JVScheduleBlockData)otherData;

				if( StartTime != other.StartTime )
				{
					conflicts.Add((int)ConflictTypes.StartTime);
					isEqual = false;
				}
				if( EndTime != other.EndTime )
				{
					conflicts.Add((int)ConflictTypes.EndTime);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}
