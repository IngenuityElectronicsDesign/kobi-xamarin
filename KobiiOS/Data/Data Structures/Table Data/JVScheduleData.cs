﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVScheduleData : JVData
	{			
		public enum ConflictTypes
		{
			WeeklySchedule = 0,
			DailySchedule
		}

		public const int NumOfBlocks = 3;

		public JVScheduleBlockData[] WeeklyBlockData { get; set; }
		public List<JVScheduleBlockData[]> DailyBlockData { get; set; }

		public JVScheduleData()
		{
			WeeklyBlockData = new JVScheduleBlockData[NumOfBlocks];

			for( int i = 0; i < NumOfBlocks; i++ )
			{
				WeeklyBlockData[i] = new JVScheduleBlockData();
			}

			DailyBlockData = new List<JVScheduleBlockData[]>(JVShared.NumOfDays);

			for( int d = 0; d < JVShared.NumOfDays; d++ )
			{
				JVScheduleBlockData[] BlockArray = new JVScheduleBlockData[NumOfBlocks];

				for( int b = 0; b < NumOfBlocks; b++ )
				{
					BlockArray[b] = new JVScheduleBlockData();
				}

				DailyBlockData.Add(BlockArray);
			}
		}

		public JVScheduleData(JVScheduleData original) : this()
		{
			for( int i = 0; i < NumOfBlocks; i++ )
			{
				WeeklyBlockData[i] = new JVScheduleBlockData(original.WeeklyBlockData[i]);
			}
			for( int d = 0; d < JVShared.NumOfDays; d++ )
			{
				for( int b = 0; b < NumOfBlocks; b++ )
				{
					DailyBlockData[d][b] = new JVScheduleBlockData(original.DailyBlockData[d][b]);
				}
			}
		}

		public bool IsDailyDataConflicted(int[] indicies, int blockIndex, bool startTime)
		{
			bool isConflicted = false;
			for( int i = 0; i < indicies.Length-1; i++ )
			{
				HashSet<int> conflicts = new HashSet<int>();

				DailyBlockData[indicies[i]][blockIndex].IsEqualToOther(DailyBlockData[indicies[i+1]][blockIndex], conflicts);

				if( conflicts.Contains( (int)(startTime ? JVScheduleBlockData.ConflictTypes.StartTime : JVScheduleBlockData.ConflictTypes.EndTime) ) )
				{
					isConflicted = true;
				}
			}
			return isConflicted;
		}

		public override bool IsEqualToOther(JVData otherData, HashSet<int> conflicts)
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVScheduleData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVScheduleData other = (JVScheduleData)otherData;

				for( int i = 0; i < NumOfBlocks; i++ )
				{
					if( !WeeklyBlockData[i].IsEqualToOther(other.WeeklyBlockData[i], new HashSet<int>()) )
					{
						conflicts.Add( (int)ConflictTypes.WeeklySchedule );
						isEqual = false;
					}
				}

				for( int d = 0; d < JVShared.NumOfDays; d++ )
				{
					for( int b = 0; b < NumOfBlocks; b++ )
					{
						if( !DailyBlockData[d][b].IsEqualToOther(other.DailyBlockData[d][b], new HashSet<int>()) )
						{
							conflicts.Add( (int)ConflictTypes.DailySchedule );
							isEqual = false;
						}
					}
				}

				return isEqual;
			}
			return false;
		}
	}
}

