using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVQuotaData : JVData
	{
		public enum ConflictTypes
		{
			Mode,
			WeeklyQuota,
			DailyQuotas,
			EmergencyQuota
		}

		public enum KobiMode
		{
			Conflicted = -1,
			Monitor,
			Quota,
			Suspend
		}

		public KobiMode Mode { get; set; }
		public byte WeeklyQuota { get; set; }
		public byte[] DailyQuotas { get; set; }
		public byte EmergencyHoursQuota { get; set; }

		public JVQuotaData()
		{
			Mode = KobiMode.Monitor;
			WeeklyQuota = 40;

			DailyQuotas = new byte[JVShared.NumOfDays];
			for( int i = 0; i < JVShared.NumOfDays; i++ )
			{
				DailyQuotas[i] = 4;
			}
				
			EmergencyHoursQuota = 4;
		}

		public JVQuotaData(JVQuotaData original) : this()
		{
			Mode = original.Mode;
			WeeklyQuota = original.WeeklyQuota;

			for( int i = 0; i < JVShared.NumOfDays; i++ )
			{
				DailyQuotas[i] = original.DailyQuotas[i];
			}
				
			EmergencyHoursQuota = original.EmergencyHoursQuota;
		}

		public bool IsDailyDataConflicted(int[] indicies)
		{
			bool isConflicted = false;
			for( int i = 0; i < indicies.Length-1; i++ )
			{
				if( DailyQuotas[indicies[i]] != DailyQuotas[indicies[i+1]] )
				{
					isConflicted = true;
				}
			}
			return isConflicted;
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVQuotaData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVQuotaData other = (JVQuotaData)otherData;

				if( Mode != other.Mode )
				{
					conflicts.Add((int)ConflictTypes.Mode);
					isEqual = false;
				}
				if( WeeklyQuota != other.WeeklyQuota )
				{
					conflicts.Add((int)ConflictTypes.WeeklyQuota);
					isEqual = false;
				}
				for( int i = 0; i < DailyQuotas.Length; i++ )
				{
					if( DailyQuotas[i] != other.DailyQuotas[i] )
					{
						conflicts.Add((int)ConflictTypes.DailyQuotas);
						isEqual = false;
						break;
					}
				}
				if( EmergencyHoursQuota != other.EmergencyHoursQuota )
				{
					conflicts.Add((int)ConflictTypes.EmergencyQuota);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}

