﻿using System;
using System.Collections.Generic;

namespace KobiiOS
{
	public class JVKobiPortData : JVData
	{
		public enum ConflictTypes
		{
			Name = 0,
			DeviceClass
		}

		public string Name;
		public int DeviceClassIndex;

		public JVKobiPortData()
		{
			Name = "";
			DeviceClassIndex = -1;
		}

		public JVKobiPortData( string name, int deviceClassIndex )
		{
			Name = name;
			DeviceClassIndex = deviceClassIndex;
		}

		public JVKobiPortData(JVKobiPortData original ) : this( original.Name, original.DeviceClassIndex ) { }

		public override bool IsEqualToOther(JVData otherData, System.Collections.Generic.HashSet<int> conflicts)
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVKobiPortData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVKobiPortData other = (JVKobiPortData)otherData;

				if( Name != other.Name )
				{
					conflicts.Add((int)ConflictTypes.Name);
					isEqual = false;
				}
				if( DeviceClassIndex != other.DeviceClassIndex )
				{
					conflicts.Add((int)ConflictTypes.DeviceClass);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}

