using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVDeviceClassData : JVData
	{
		public enum ConflictTypes
		{
			Name = 0,
			Icon,
			IndexID
		}

		public const int MaxNameLength = 12;

		public string Name { get; set; }
		public UIImage Icon { get; set; }
		public int IndexID { get; set; }

		public JVDeviceClassData( string name, UIImage icon )
		{
			Name = name;
			Icon = icon;
			IndexID = -1;
		}

		public JVDeviceClassData( JVDeviceClassData original )
		{
			Name = original.Name;
			Icon = original.Icon;
			IndexID = original.IndexID;
		}

		public override bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( base.IsEqualToOther( otherData, conflicts ) )
			{
				if( otherData.GetType() != typeof(JVDeviceClassData) )
				{
					conflicts.Add( JVData.ClassTypeConflict );
					return false;
				}

				bool isEqual = true;
				JVDeviceClassData other = (JVDeviceClassData)otherData;

				if( Name != other.Name )
				{
					conflicts.Add((int)ConflictTypes.Name);
					isEqual = false;
				}
				if( Icon != other.Icon )
				{
					conflicts.Add((int)ConflictTypes.Icon);
					isEqual = false;
				}
				if( IndexID == -1 || IndexID != other.IndexID )
				{
					conflicts.Add((int)ConflictTypes.IndexID);
					isEqual = false;
				}

				return isEqual;
			}
			return false;
		}
	}
}

