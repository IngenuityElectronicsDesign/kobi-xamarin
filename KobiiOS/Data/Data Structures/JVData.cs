using System;
using System.Collections.Generic;

namespace KobiiOS
{
	public abstract class JVData
	{
		public const int BasicConflict = -2;
		public const int ClassTypeConflict = -1;

		public virtual bool IsEqualToOther( JVData otherData, HashSet<int> conflicts )
		{
			if( otherData == null )
			{
				conflicts.Add( BasicConflict );
				return false;
			}
			return true;
		}
	}
}

