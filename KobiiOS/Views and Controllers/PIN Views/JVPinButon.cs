using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace KobiiOS
{
	static class UIButtonExtentions
	{
		public static void CircularPinStyle(this UIButton button)
		{
			button.Layer.BorderWidth = 2f;
			button.Layer.CornerRadius = (Math.Min(button.Bounds.Width, button.Bounds.Height)/2);
			button.Layer.MasksToBounds = true;
			button.AdjustsImageWhenHighlighted = false;
			button.TitleLabel.Font = UIFont.SystemFontOfSize(28f);
			button.SetTitleColor(UIColor.Black, UIControlState.Normal);
		}

		public static void SetTintColor(this UIButton button, UIColor tintColor)
		{
			button.TintColor = tintColor;
			button.Layer.BorderColor = tintColor.CGColor;
			button.SetBackgroundImage(ButtonImage(button, tintColor.ColorWithAlpha(0.2f)), UIControlState.Highlighted);
		}

		public static UIImage ButtonImage(this UIButton button, UIColor color)
		{
			RectangleF rect = new RectangleF(0, 0, button.Frame.Width, button.Frame.Height);
			UIGraphics.BeginImageContext(rect.Size);
			CGContext context = UIGraphics.GetCurrentContext();
			context.SetFillColorWithColor(color.CGColor);
			context.FillRect(rect);
			UIImage img = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return img;
		}
	}
}
