using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace KobiiOS
{
	public class JVPinDigitIndicator : UIControl
	{
		public JVPinDigitIndicator(RectangleF frame) : base(frame)
		{
			TintColor = JVShared.Instance.SystemTintColor;

			Layer.BorderWidth = 2f;
			Layer.CornerRadius = (Math.Min(Bounds.Width, Bounds.Height)/2);
			Layer.MasksToBounds = true;
			Layer.BorderColor = TintColor.CGColor;
		}

		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;

				UIView.Animate( ( value ? 0.1f : 0.25f ), 0f, (UIViewAnimationOptions.CurveEaseInOut | UIViewAnimationOptions.AllowUserInteraction), () => // Animation
					{
						if( value )
						{
							Layer.BackgroundColor = TintColor.CGColor;
						}
						else
						{
							Layer.BackgroundColor = UIColor.Clear.CGColor;
						}
					}, () => // Completion
					{ });
			}
		}

		public override UIColor TintColor
		{
			get
			{
				return base.TintColor;
			}
			set
			{
				base.TintColor = value;
				Layer.BorderColor = value.CGColor;
				if( Enabled )
				{
					Layer.BackgroundColor = value.CGColor;
				}
			}
		}
	}
}

