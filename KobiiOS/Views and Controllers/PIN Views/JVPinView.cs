using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVPinView : UIView
	{
		const int PinEntryCount = 4;
		const int PinButtonsCount = 5;
		const int KeyDiameter = 64;
		const int KeySpacing = 26;
		const int DigitDiameter = 48;
		const int DigitSpacing = 12;
		const int DigitEdgeSpacing = 34;
		const int BackgroundHeight = 183;

		// Events to tell Table Data Containers when Users are added or removed
		public delegate void OnPinEnteredDelegate(bool success);
		public event OnPinEnteredDelegate OnPinEntered;
		public delegate void OnEnteredFirstNewPinDelegate(JVPinCodeData pinCode);
		public event OnEnteredFirstNewPinDelegate OnEnteredFirstNewPin;
		public delegate void OnEnteredSecondNewPinDelegate(bool success);
		public event OnEnteredSecondNewPinDelegate OnEnteredSecondNewPin;

		char[] PinEntryChars = new char[PinEntryCount];
		private JVPinCodeData m_TargetPinCode;
		public JVPinCodeData TargetPinCode
		{
			get
			{
				return m_TargetPinCode;
			}
			set
			{
				m_TargetPinCode = value;
			}
		}
		public JVPinCodeData OverridingPinCode { get; set; }
		int CurrentPinEntryIndex;
		List<JVPinDigitIndicator> PinDigitIndicatorsList;
		List<UIButton> PinButtonsList;

		public bool Overriding { get; private set; }
		public override UIColor TintColor
		{
			get
			{
				return base.TintColor;
			}
			set
			{
				base.TintColor = value;

				foreach( JVPinDigitIndicator digitIndicator in PinDigitIndicatorsList )
				{
					digitIndicator.TintColor = value;
				}
				foreach( UIButton button in PinButtonsList )
				{
					button.SetTintColor(value);
				}
			}
		}

		bool IncorrectOdd;

		public JVPinView(RectangleF frame, bool isOverriding) : base(frame)
		{
			Overriding = isOverriding;

			// Setup Buttons Background View
			UIView backgroundView = new UIView( new RectangleF(0, Frame.Height-BackgroundHeight, Frame.Width, BackgroundHeight) );
			backgroundView.BackgroundColor = UIColor.FromWhiteAlpha(0.95f, 0.95f);
			AddSubview(backgroundView);

			// Setup Digit Indicators
			PinDigitIndicatorsList = new List<JVPinDigitIndicator>();

			for( int i = 0; i < PinEntryCount; i++ )
			{
				JVPinDigitIndicator digit = new JVPinDigitIndicator( new RectangleF(DigitEdgeSpacing+DigitSpacing+((DigitSpacing+DigitDiameter)*i), 0, DigitDiameter, DigitDiameter));
				PinDigitIndicatorsList.Add(digit);
				AddSubview(digit);
			}

			// Setup Pin Buttons
			PinButtonsList = new List<UIButton>();

			for( int i = 0; i < PinButtonsCount; i++ )
			{
				UIButton button = new UIButton(UIButtonType.Custom);
				button.Frame = new RectangleF(((KeyDiameter+KeySpacing)*(i%3))+KeySpacing*1.5f, Frame.Height-BackgroundHeight + (KeyDiameter+KeySpacing*0.5f)*(i/3)+(KeySpacing*0.5f), KeyDiameter, KeyDiameter);
				button.CircularPinStyle();

				if( i == 2 )
				{
					button.SetTitle("⌫", UIControlState.Normal);
					button.Tag = 0;
				}
				else
				{
					button.SetTitle( ( i < 2 ? (i+1).ToString() : i.ToString() ), UIControlState.Normal );
					button.Tag = ( i < 2 ? i+1 : i );
				}

				button.TouchDown += (sender, e) =>
				{
					if( button.Tag != 0 )
					{
						WritePinDigit(button.Tag);
					}
					else
					{
						ErasePinDigit();
					}
				};

				PinButtonsList.Add(button);
				AddSubview(button);
			}

			//TargetPinCode = null;
			CurrentPinEntryIndex = 0;

			for( int i = 0; i < PinEntryCount; i++ )
			{
				PinEntryChars[i] = (char)99;
			}

			IncorrectOdd = false;
		}

		#region Pin Code Entry
		void WritePinDigit(int value)
		{
			if( CurrentPinEntryIndex >= PinEntryCount )
			{
				return;
			}
			if( PinEntryChars[CurrentPinEntryIndex] == (char)99 )
			{
				PinEntryChars[CurrentPinEntryIndex] = (char)(value + 0x30);
				PinDigitIndicatorsList[CurrentPinEntryIndex].Enabled = true;
				CurrentPinEntryIndex++;

				if( CurrentPinEntryIndex >= PinEntryCount )
				{
					if( !Overriding )
					{
						if( TargetPinCode.IsEqualToOther( new JVPinCodeData(new string(PinEntryChars)), new HashSet<int>() ) )
						{
							if( OnPinEntered != null )
							{
								OnPinEntered(true);
							}
						}
						else
						{
							if( OnPinEntered != null )
							{
								OnPinEntered(false);
							}
						}
					}
					else
					{
						if( OverridingPinCode == null )
						{
							OverridingPinCode = new JVPinCodeData(new string(PinEntryChars));
							if( OnEnteredFirstNewPin != null )
							{
								OnEnteredFirstNewPin(OverridingPinCode);
							}
						}
						else
						{
							if( OnEnteredSecondNewPin != null )
							{
								OnEnteredSecondNewPin( OverridingPinCode.IsEqualToOther(new JVPinCodeData(new string(PinEntryChars)), new HashSet<int>() ) );
							}
						}
						Console.Write("Over: {0} .. Entered: {1}", OverridingPinCode.GetPin(), PinEntryChars.ToString());
					}
				}
			}
		}

		void ErasePinDigit()
		{
			CurrentPinEntryIndex--;
			if( CurrentPinEntryIndex <= 0 )
			{
				CurrentPinEntryIndex = 0;
			}

			if( PinEntryChars[CurrentPinEntryIndex] != (char)99 )
			{
				PinEntryChars[CurrentPinEntryIndex] = (char)99;
				PinDigitIndicatorsList[CurrentPinEntryIndex].Enabled = false;
			}
		}

		public void ResetPinDigits(bool andOverride)
		{
			for( int i = 0; i < PinEntryCount; i++ )
			{
				PinEntryChars[i] = (char)99;
				PinDigitIndicatorsList[i].Enabled = false ;
			}

			CurrentPinEntryIndex = 0;

			if(andOverride)
			{
				OverridingPinCode = null;
			}
		}

		public void EnteredIncorrectPin()
		{
			foreach( JVPinDigitIndicator digitIndicator in PinDigitIndicatorsList )
			{
				RectangleF frame = digitIndicator.Frame;

				UIView.AnimateNotify( 1.25f, 0f, 0.2f, 2000f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
					{
						digitIndicator.Frame = new RectangleF( new PointF(frame.X + (IncorrectOdd ? 1f : -1f), frame.Y), frame.Size);
					}, (bool finished) => // Completion
					{
						UIView.Animate( 0.25f, () => // Animation
							{
								digitIndicator.Frame = new RectangleF( new PointF(frame.X + (IncorrectOdd ? -1f : 1f), frame.Y), frame.Size);
							});
					});
			}

			IncorrectOdd = !IncorrectOdd;
		}

		#endregion Pin Code Entry
	}
}

