using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVUseKobiTableViewController : JVTableViewController
	{
		public JVUseKobiTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(UITableViewCell), new NSString("UseKobiReuseCell") );
		}

		public override void RefreshView()
		{
			base.RefreshView();
		}
		#endregion View Controller Logic

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return JVShared.Instance.KobiBoxDataContainer.Count;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			return "Select a Kobi to Use";
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("UseKobiReuseCell"), indexPath);
			cell.TextLabel.Text = JVShared.Instance.KobiBoxDataContainer.GetData(indexPath.Row).Name;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			JVUseKobiPortTableViewController toPush = (JVUseKobiPortTableViewController)JVShared.Instance.Storyboard.InstantiateViewController("UsePortTVC");
			toPush.Setup(indexPath.Row);

			NavigationController.PushViewController(toPush, true);
		}

		#endregion TableView Logic
	}
}
