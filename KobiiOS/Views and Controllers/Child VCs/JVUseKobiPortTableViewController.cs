using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVUseKobiPortTableViewController : JVTableViewController
	{
		public int BoxIndex { get; private set; }

		public JVUseKobiPortTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup( int boxIndex )
		{
			BoxIndex = boxIndex;
			Title = JVShared.Instance.KobiBoxDataContainer.GetData(BoxIndex).Name;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVKobiPortCell), new NSString("KobiPortCell") );
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return 1;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("KobiPortCell"), indexPath);
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			((JVKobiPortCell)cell).Setup( (indexPath.Section == 0 ? JVShared.Instance.KobiBoxDataContainer.GetData(BoxIndex).Port1 : JVShared.Instance.KobiBoxDataContainer.GetData(BoxIndex).Port2) );

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			TableView.DeselectRow(indexPath, true);

			JVKobiPortData portData = (indexPath.Section == 0 ? JVShared.Instance.KobiBoxDataContainer.GetData(BoxIndex).Port1 : JVShared.Instance.KobiBoxDataContainer.GetData(BoxIndex).Port2);

			JVUseKobiBoxTableViewController toPush = (JVUseKobiBoxTableViewController)JVShared.Instance.Storyboard.InstantiateViewController("UseKobiBoxTVC");
			toPush.Setup(portData.DeviceClassIndex);
			NavigationController.PushViewController(toPush, true);
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return 88;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Select a Port to Use";
			}
			return "";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			return string.Format("PORT {0}", section+1);
		}
		#endregion Table View Logic
	}
}
