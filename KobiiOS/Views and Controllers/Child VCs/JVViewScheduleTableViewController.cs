using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVViewScheduleTableViewController : JVTableViewController, IJVDeviceClassSelectionCellDelegate, IJVDaySelectionCellDelegate
	{
		int SelectedUserIndex = -1;
		int SelectedDeviceClassIndex = 0;
		int SelectedDayIndex = 0;

		public JVViewScheduleTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse(typeof(JVDeviceClassSelectionCell), new NSString("DeviceClassSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVDaySelectionCell), new NSString("DaySelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVScheduleDisplayCell), new NSString("ScheduleDisplayCell"));
			TableView.RegisterClassForCellReuse(typeof(JVTimeInputCell), new NSString("TimeInputCell"));

			BlockSelectionSegmentedControl.SelectedSegment = 0;
			BlockSelectionSegmentedControl.TintColor = JVShared.Instance.GetUserTintColor();

			if( !JVShared.Instance.LoggedInUserData.IsAdmin )
			{
				SelectedUserIndex = JVShared.Instance.UserDataContainer.GetIndex(JVShared.Instance.LoggedInUserData);
			}
		}

		public override void RefreshView()
		{
			base.RefreshView();
		}
		#endregion View Controller Logic

		#region UI Actions
		partial void BlockSelectionSegmentedControlValueChanged(UISegmentedControl sender)
		{
			RefreshView();
		}
		#endregion UI Actions

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 3;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 2 )
			{
				return 4;
			}
			return 1;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Select a Device";
			}
			else if( section == 1 )
			{
				return "Select a Day";
			}
			else if( section == 2 )
			{
				return "View Schedule";
			}
			return "";
		}

		public override string TitleForFooter(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Tap to select a device.";
			}
			else if( section == 1 )
			{
				return "Tap to select a day.";
			}
			else if( section == 2 )
			{
				return "Tap to select a block to view precise times.";
			}
			return "";
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = null;

			if( indexPath.Section == 0 )
			{
				cell = TableView.DequeueReusableCell(new NSString("DeviceClassSelectionCell"), indexPath);
				((JVDeviceClassSelectionCell)cell).Setup(false);
				((JVDeviceClassSelectionCell)cell).Delegate = this;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			else if( indexPath.Section == 1 )
			{
				cell = TableView.DequeueReusableCell(new NSString("DaySelectionCell"), indexPath);
				((JVDaySelectionCell)cell).Setup(false);
				((JVDaySelectionCell)cell).Delegate = this;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			else if( indexPath.Section == 2 )
			{
				if( indexPath.Row == 0 )
				{
					cell = TableView.DequeueReusableCell(new NSString("ScheduleDisplayCell"), indexPath);
					byte[] startVals = new byte[3];
					byte[] endVals = new byte[3];
					for( int i = 0; i < JVScheduleData.NumOfBlocks; i++ )
					{
						startVals[i] = JVShared.Instance.ScheduleDataContainer.GetDailySchedule(SelectedUserIndex, SelectedDeviceClassIndex, SelectedDayIndex,  i, true);
						endVals[i] = JVShared.Instance.ScheduleDataContainer.GetDailySchedule(SelectedUserIndex, SelectedDeviceClassIndex, SelectedDayIndex, i, false);
					}
					((JVScheduleDisplayCell)cell).SetBlockView(startVals, endVals, BlockSelectionSegmentedControl.SelectedSegment);
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}
				else if( indexPath.Row == 1 )
				{
					cell = base.GetCell(TableView, indexPath);
				}
				else
				{
					cell = TableView.DequeueReusableCell(new NSString("TimeInputCell"), indexPath);
					((JVTimeInputCell)cell).SetupCell(indexPath.Row-2, indexPath.Row == 2 ? "Start Time" : "End Time", JVShared.Instance.ScheduleDataContainer.GetDailySchedule(SelectedUserIndex, SelectedDeviceClassIndex, SelectedDayIndex, BlockSelectionSegmentedControl.SelectedSegment, (indexPath.Row == 2)), 24, true);
					//((JVTimeInputCell)cell).Delegate = this;
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}
			}
			else
			{
				cell = base.GetCell(TableView, indexPath);
			}

			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
		}
		#endregion TableView Logic

		#region IJVDeviceClassSelectionCellDelegate implementation
		public void DeviceClassSelectionDidChange( int index )
		{
			SelectedDeviceClassIndex = index;
			RefreshView();
		}
		#endregion IJVDeviceClassSelectionCellDelegate implementation

		#region IJVDaySelectionCellDelegate implementation
		public void DaySelectionDidChange(int index)
		{
			SelectedDayIndex = index;
			RefreshView();
		}
		#endregion IJVDaySelectionCellDelegate implementation
	}
}
