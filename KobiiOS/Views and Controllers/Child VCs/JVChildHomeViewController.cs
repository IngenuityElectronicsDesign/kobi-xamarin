using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVChildHomeViewController : JVHomeViewController
	{
		public JVChildHomeViewController(IntPtr handle) : base(handle, 3)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			Titles = new string[] { "Use Kobi", "Schedule", "Usage" };
			ImageNames = new string[] { "Kobi.png", "Schedule.png", "Usage.png" };

			base.ViewDidLoad();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear(animated);

			NavigationController.NavigationBar.BarTintColor = JVShared.Instance.GetUserTintColor(JVShared.Instance.LoggedInUserData.TintColorIndex);
			Title = JVShared.Instance.LoggedInUserData.Name;
		}
		#endregion View Controller Logic

		#region IJVIconViewDelegate implementation
		public override void DidPressIconWithIndex(int index)
		{
			string pushString = "";

			switch( index )
			{
			case 0:
				{
					pushString = "UseKobiTVC";
					break;
				}
			case 1:
				{
					pushString = "ViewScheduleTVC";
					break;
				}
			case 2:
				{
					pushString = "ViewChildrensUsageTVC";
					break;
				}
			}

			UIViewController toPush = (UIViewController)JVShared.Instance.Storyboard.InstantiateViewController(pushString);
			NavigationController.PushViewController(toPush, true);
		}
		#endregion IJVIconViewDelegate implementation

		#region Layout Logic
		public override void LayoutIcons()
		{
			int x, y, vertSpacing;

			vertSpacing = ( JVShared.Instance.IsWidescreen ? 84 : 55 );

			for( int i = 0; i < MenuItemCount; i++ )
			{
				JVMenuIconView menuIcon = MenuIcons[i];

				x = 320;
				y = JVShared.TitleBarHeight + vertSpacing + ((JVShared.IconHeight+vertSpacing)*((i+1)/2));

				menuIcon.Frame = new RectangleF(x, y, menuIcon.Frame.Width, menuIcon.Frame.Height);

				if( i == 0 )
				{
					x = 112;
				}
				else
				{
					x = JVShared.IconEdgePadding + ((JVShared.IconWidth+JVShared.IconHorizSpacing)*(i%2));
				}

				UIView.AnimateNotify( 0.5f, 0.1f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
					{
						menuIcon.Frame = new RectangleF(x, y, menuIcon.Frame.Width, menuIcon.Frame.Height);
					}, (bool finished) => // Completion
					{ });
			}
		}
		#endregion Layout Logic
	}
}
