using System;
using System.Timers;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVUseKobiBoxTableViewController : JVTableViewController
	{
		private bool InUse;
		private NSTimer InUseTimer;
		private DateTime InUseTimerStartDate;
		private TimeSpan InUseTimerRunningSpan;
		private UITableViewCell InUseCell;

		private int SelectedUserIndex;
		private int SelectedDeviceIndex;

		public JVUseKobiBoxTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(int deviceIndex)
		{
			SelectedUserIndex = JVShared.Instance.UserDataContainer.GetIndex(JVShared.Instance.LoggedInUserData);
			SelectedDeviceIndex = deviceIndex;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			InUse = false;

			if( !JVShared.Instance.LoggedInUserData.IsAdmin )
			{
				UIBarButtonItem warnButton = new UIBarButtonItem("Warn", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
					{
						UIImageView imageView = new UIImageView(new RectangleF(0, 0, 120, 120));
						imageView.Image = UIImage.FromFile("Sad.png").ImageWithTintColor(JVShared.Instance.GetUserTintColor());

						XIManualAnimationViewWrapper[] animViewArray = new XIManualAnimationViewWrapper[] { new XIManualAnimationViewWrapper( imageView, new PointF(320f, 250f), new PointF(100f, 250f), new PointF(-120f, 250f)) };

						XIPopupPagingView scrollingView = new XIPopupPagingView(new SizeF(310, 538), 1, JVShared.Instance.GetUserTintColor().ColorWithAlpha(0.25f));
						scrollingView.SetupPage( 0, string.Format("Hey {0}, you're almost out of time with your {1}...", JVShared.Instance.LoggedInUserData.Name, JVShared.Instance.DeviceClassDataContainer.GetData(SelectedDeviceIndex).Name), "So you'd better wrap it up soon!", animViewArray, "Got it!" );
						//scrollingView.SetupPage( 1, "So you'd better wrap it up soon!", null, "Got it!" );
						scrollingView.OnDismissed += () => { scrollingView.RemoveFromSuperview(); };
						UIApplication.SharedApplication.Delegate.Window.AddSubview(scrollingView);
					});
				NavigationItem.SetRightBarButtonItem(warnButton, false);
			}

			TableView.RegisterClassForCellReuse(typeof(JVUsageCell), new NSString("UsageCell"));
		}
		#endregion View Controller Logic

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 3;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( JVShared.Instance.LoggedInUserData.IsAdmin && section == 0 )
			{
				return 0;
			}
			return 1;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			string deviceName = JVShared.Instance.DeviceClassDataContainer.GetData(SelectedDeviceIndex).Name;

			if( !JVShared.Instance.LoggedInUserData.IsAdmin && section == 0 )
			{
				return JVShared.Instance.KobiGlobalData.QuotaTimePeriodIsDaily ? string.Format("Today's {0} Usage", deviceName) : string.Format("This Week's {0} Usage", deviceName);
			}
			else if( section == 1 )
			{
				return string.Format("{0} Status", deviceName);
			}
			return "";
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 0 )
			{
				return 88;
			}
			return 44;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = base.GetCell(TableView, indexPath);

			if( indexPath.Section == 0 )
			{
				cell = TableView.DequeueReusableCell(new NSString("UsageCell"), indexPath);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				((JVUsageCell)cell).Setup(indexPath.Row, SelectedUserIndex, SelectedDeviceIndex);
			}
			else if( indexPath.Section == 1 )
			{
				InUseCell = cell;
				if( InUse )
				{
					cell.BackgroundColor = JVShared.Instance.GetUserTintColor();
					cell.TextLabel.TextColor = UIColor.White;
					InUseCell.TextLabel.Text = string.Format("In Use For {0:hh\\:mm\\:ss}", InUseTimerRunningSpan);
				}
				else
				{
					cell.BackgroundColor = UIColor.White;
					cell.TextLabel.TextColor = UIColor.Black;
					cell.TextLabel.Text = "Not In Use";
				}
			}
			else if( indexPath.Section == 2 )
			{
				string deviceName = JVShared.Instance.DeviceClassDataContainer.GetData(SelectedDeviceIndex).Name;
				cell.TextLabel.TextColor = JVShared.Instance.GetUserTintColor();
				if( InUse )
				{
					cell.TextLabel.Text = string.Format("Stop Using {0}", deviceName);
				}
				else
				{
					cell.TextLabel.Text = string.Format("Start Using {0}", deviceName);
				}
			}

			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			TableView.DeselectRow(indexPath, true);

			if( indexPath.Section == 2 )
			{
				RefreshView();

				if( InUse )
				{
					InUse = false;
					InUseTimerRunningSpan = new TimeSpan();
					InUseTimer.Invalidate();
					InUseTimer = null;
				}
				else
				{
					InUse = true;
					InUseTimerStartDate = DateTime.Now;

					InUseTimer = NSTimer.CreateRepeatingScheduledTimer(1f, () => // Timer Fired
						{
							InUseTimerRunningSpan = DateTime.Now.Subtract(InUseTimerStartDate);
							InUseCell.TextLabel.Text = string.Format("In Use For {0:hh\\:mm\\:ss}", InUseTimerRunningSpan);
						});
				}
			}
		}
		#endregion TableView Logic
	}
}
