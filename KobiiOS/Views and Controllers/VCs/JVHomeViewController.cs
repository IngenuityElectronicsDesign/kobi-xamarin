using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public abstract class JVHomeViewController : JVViewController, IJVIconViewDelegate
	{
		public readonly int MenuItemCount;

		public JVMenuIconView[] MenuIcons { get; protected set; }
		public string[] Titles { get; protected set; }
		public string[] ImageNames { get; protected set; }

		public JVHomeViewController(IntPtr handle, int menuItemCount ) : base (handle)
		{
			MenuItemCount = menuItemCount;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			MenuIcons = new JVMenuIconView[MenuItemCount];

			for( int i = 0; i < MenuItemCount; i++ )
			{
				JVMenuIconView menuIcon = new JVMenuIconView( new RectangleF(0, 0, JVShared.IconWidth, JVShared.IconHeight), Titles[i], UIImage.FromFile(ImageNames[i]), i);
				menuIcon.Delegate = this;
				View.AddSubview(menuIcon);
				MenuIcons[i] = menuIcon;
			}

			LayoutIcons();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear(animated);

			UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);

			NavigationController.NavigationBar.TintColor = UIColor.White;
			NavigationController.NavigationBar.SetTitleTextAttributes(new UITextAttributes() { TextColor = UIColor.White });

			NavigationItem.SetLeftBarButtonItem( new UIBarButtonItem("Log Out", UIBarButtonItemStyle.Plain, (sender, args) => // Bar Button Item Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				}), false);
		}
		#endregion View Controller Logic

		#region IJVIconViewDelegate implementation
		public virtual void DidPressIconWithIndex(int index) {}
		#endregion IJVIconViewDelegate implementation

		#region Layout Logic
		public virtual void LayoutIcons() {}
		#endregion Layout Logic
	}
}

