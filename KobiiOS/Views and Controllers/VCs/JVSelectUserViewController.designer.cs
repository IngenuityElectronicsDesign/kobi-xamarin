// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace KobiiOS
{
	[Register ("JVSelectUserViewController")]
	partial class JVSelectUserViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem CancelButton { get; set; }

		[Action ("CancelButtonPressed:")]
		partial void CancelButtonPressed (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (CancelButton != null) {
				CancelButton.Dispose ();
				CancelButton = null;
			}
		}
	}
}
