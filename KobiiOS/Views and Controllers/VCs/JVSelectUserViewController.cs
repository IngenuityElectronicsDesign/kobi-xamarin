using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVSelectUserViewController : JVViewController, IJVIconViewDelegate
	{
		public const int TallPinScreenHeight = 290;
		public const int ShortPinScreenHeight = 250;

		private List<JVUserIconView> UserIcons;

		private int ActiveUserIndex;
		private PointF ActiveUserOrigin;
		private PointF InactiveUserOrigin;
		private PointF OffScreenUserOrigin;

		public JVPinView PinView { get; set; }

		public JVSelectUserViewController(IntPtr handle) : base (handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			ActiveUserIndex = -1;

			CancelButton.Title = "";

			UserIcons = new List<JVUserIconView>();

			for( int i = 0; i < JVUserDataContainer.MaxNumTotalUsers; i++ )
			{
				JVUserIconView userIcon = new JVUserIconView(new RectangleF(0, 0, JVShared.IconWidth, JVShared.IconHeight), i);
				userIcon.Delegate = this;
				View.AddSubview(userIcon);
				UserIcons.Add(userIcon);
			}

			LayoutUserIcons(false);

			ActiveUserOrigin = new PointF(112, JVShared.Instance.IsWidescreen ? 120 : 100);
			InactiveUserOrigin = new PointF(112, 398);
			OffScreenUserOrigin = new PointF(112, 700);

			// Setup Pin View
			PinView  = new JVPinView( new RectangleF(0, View.Frame.Height, View.Frame.Width, ( JVShared.Instance.IsWidescreen ? TallPinScreenHeight : ShortPinScreenHeight )), false );
			View.AddSubview(PinView);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			// Set Nav Bar to Defaults
			UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.Default, true);

			NavigationController.NavigationBar.BarTintColor = null;
			NavigationController.NavigationBar.TintColor = JVShared.Instance.SystemTintColor;
			NavigationController.NavigationBar.SetTitleTextAttributes(new UITextAttributes() { TextColor = UIColor.Black });

			PinView.OnPinEntered += DidEnterPinCodeWithSuccess;
		}

		public override void ViewDidDisappear (bool animated)
		{
			PinView.OnPinEntered -= DidEnterPinCodeWithSuccess;

			base.ViewDidDisappear (animated);
		}

		public override void RefreshView()
		{
			CancelButtonPressed(null);

			for( int i = 0; i < JVUserDataContainer.MaxNumTotalUsers; i++ )
			{
				if( i < JVShared.Instance.UserDataContainer.Count )
				{
					UserIcons[i].SetUserData(JVShared.Instance.UserDataContainer.GetData(i));
				}
				else
				{
					UserIcons[i].SetBlank();
				}
			}
		}
		#endregion View Controller Logic

		#region IJVIconViewDelegate implementation
		public void DidPressIconWithIndex(int index)
		{
			ActiveUserIndex = index;
			PinViewShouldBeHidden(false);

			for( int i = 0; i < JVShared.Instance.UserDataContainer.Count; i++ )
			{
				JVUserIconView userIcon = UserIcons[i];

				if( i == index )
				{
					RectangleF activeUserFrame = new RectangleF(ActiveUserOrigin.X, ActiveUserOrigin.Y, userIcon.Frame.Size.Width, userIcon.Frame.Size.Height);

					Title = "Enter PIN";
					CancelButton.Title = "Cancel";

					UIView.AnimateNotify( 0.5f, 0f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
						{
							userIcon.Frame = activeUserFrame;
						}, (bool finished) => // Completion
						{ });
				}
				else
				{
					RectangleF inactiveUserFrame = new RectangleF(InactiveUserOrigin.X, InactiveUserOrigin.Y, userIcon.Frame.Size.Width, userIcon.Frame.Size.Height);

					UIView.AnimateNotify( 0.5f, 0f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
						{
							userIcon.Frame = inactiveUserFrame;
							userIcon.Alpha = 0f;
						}, (bool finished) => // Completion
						{ });
				}
			}
		}
		#endregion IJVIconViewDelegate implementation

		#region UI Actions
		partial void CancelButtonPressed(MonoTouch.Foundation.NSObject sender)
		{
			CancelButton.Title = "";
			Title = "Select User";
			// PinView.ResetDigits()
			PinViewShouldBeHidden(true);

			for( int i = 0; i < UserIcons.Count; i++ )
			{
				if( i != ActiveUserIndex )
				{
					UIView.AnimateNotify( 0.5f, 0f, 0.7f, 5f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
						{
							UserIcons[i].Alpha = 1f;
						}, (bool finished) => // Completion
						{ });
				}
			}

			ActiveUserIndex = -1;
			LayoutUserIcons(true);
		}
		#endregion UI Actions

		#region Layout Logic
		public void LayoutUserIcons(bool animated)
		{
			int x, y, vertSpacing;

			if( JVShared.Instance.UserDataContainer.Count > 4 )
			{
				vertSpacing = ( JVShared.Instance.IsWidescreen ? 31 : 9 );
			}
			else if( JVShared.Instance.UserDataContainer.Count > 2 )
			{
				vertSpacing = ( JVShared.Instance.IsWidescreen ? 84 : 55 );
			}
			else
			{
				vertSpacing = ( JVShared.Instance.IsWidescreen ? 190 : 146 );
			}

			for( int i = 0; i < JVUserDataContainer.MaxNumTotalUsers; i++ )
			{
				if( i == ActiveUserIndex )
				{
					continue;
				}

				JVUserIconView userIcon = UserIcons[i];

				if( i >= JVShared.Instance.UserDataContainer.Count )
				{
					x = (int)OffScreenUserOrigin.X;
					y = (int)OffScreenUserOrigin.Y;
				}
				else
				{
					if( i == JVShared.Instance.UserDataContainer.Count-1 && i % 2 == 0 )
					{
						x = 112;
					}
					else
					{
						x = JVShared.IconEdgePadding + ((JVShared.IconWidth+JVShared.IconHorizSpacing)*(i%2));
					}

					y = JVShared.TitleBarHeight + vertSpacing + ((JVShared.IconHeight+vertSpacing)*(i/2));
				}

				if( animated )
				{
					UIView.AnimateNotify( 0.5f, 0f, 0.7f, 5f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
						{
							userIcon.Frame = new RectangleF(x, y, userIcon.Frame.Width, userIcon.Frame.Height);
						}, (bool finished) => // Completion
						{ });
				}
				else
				{
					userIcon.Frame = new RectangleF(x, y, userIcon.Frame.Width, userIcon.Frame.Height);
				}
			}
		}
		#endregion Layout Logic

		#region Pin View Logic
		void PinViewShouldBeHidden(bool hidden)
		{
			UIView.AnimateNotify( 0.5f, 0f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
				{
					if( hidden )
					{
						PinView.ResetPinDigits(false);
						PinView.Frame = new RectangleF(0, View.Frame.Height, View.Frame.Width, ( JVShared.Instance.IsWidescreen ? TallPinScreenHeight : ShortPinScreenHeight ));
					}
					else
					{
						Console.WriteLine( "Index: {0} with info: {0}", ActiveUserIndex, JVShared.Instance.UserDataContainer.GetData(ActiveUserIndex) );
						PinView.TintColor = JVShared.Instance.GetUserTintColor( JVShared.Instance.UserDataContainer.GetData(ActiveUserIndex).TintColorIndex );
						CancelButton.TintColor = JVShared.Instance.GetUserTintColor( JVShared.Instance.UserDataContainer.GetData(ActiveUserIndex).TintColorIndex );
						PinView.TargetPinCode = JVShared.Instance.UserDataContainer.GetData(ActiveUserIndex).PinCode;
						PinView.Frame = new RectangleF(0, ( JVShared.Instance.IsWidescreen ? TallPinScreenHeight : ShortPinScreenHeight ), View.Frame.Width, ( JVShared.Instance.IsWidescreen ? TallPinScreenHeight : ShortPinScreenHeight ));
					}
				}, (bool finished) => // Completion
				{ });
		}
		#endregion Pin View Logic

		#region Pin View Delegate
		void DidEnterPinCodeWithSuccess(bool success)
		{
			if(success)
			{
				NSTimer.CreateScheduledTimer(0.75f, EnteredCorrectPin);
				Title = "Correct PIN";
				CancelButton.Title = "";
			}
			else
			{
				NSTimer.CreateScheduledTimer(0.75f, EnteredIncorrectPin);
				Title = "Incorrect PIN";
				PinView.EnteredIncorrectPin();
			}
		}

		void EnteredCorrectPin()
		{
			PinViewShouldBeHidden(true);
			PinView.ResetPinDigits(false);
			JVShared.Instance.LoggedInUserData = JVShared.Instance.UserDataContainer.GetData(ActiveUserIndex);

			UIViewController toPush = null;

			if( JVShared.Instance.LoggedInUserData.IsAdmin )
			{
				toPush = (UIViewController)JVShared.Instance.Storyboard.InstantiateViewController("ParentHomeView");
			}
			else
			{
				toPush = (UIViewController)JVShared.Instance.Storyboard.InstantiateViewController("ChildHomeView");
			}

			NavigationController.PushViewController(toPush, true);
		}

		void EnteredIncorrectPin()
		{
			Title = "Enter PIN";
			PinView.ResetPinDigits(false);
		}
		#endregion Pin View Delegate
	}
}
