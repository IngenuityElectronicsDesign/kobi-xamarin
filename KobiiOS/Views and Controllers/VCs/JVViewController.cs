using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public abstract class JVViewController : UIViewController
	{
		public JVViewController(IntPtr handle) : base (handle)
		{
		}

		public override void ViewWillAppear( bool animated )
		{
			base.ViewWillAppear(animated);
			NSNotificationCenter.DefaultCenter.AddObserver(this, new MonoTouch.ObjCRuntime.Selector("dataNeedsRefresh:"), JVShared.SharedDataNeedsRefresh, null);
			// TODO: Possibly replace NC with events and delegates...

			RefreshView();
		}

		public virtual void RefreshView() {}

		public override void ViewWillDisappear (bool animated)
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(this);
			base.ViewWillDisappear(animated);
		}

		[Export ("dataNeedsRefresh:")]
		private void DataNeedsRefresh( NSNotification notification )
		{
			RefreshView();
		}
	}
}

