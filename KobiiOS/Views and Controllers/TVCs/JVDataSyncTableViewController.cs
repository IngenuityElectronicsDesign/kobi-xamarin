using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace KobiiOS
{
	public class JVDataSyncTableViewController : JVTableViewController
	{
		public UIBarButtonItem SaveButton { get; set; }
		public UIBarButtonItem DiscardButton { get; set; }

        public JVDataSyncTableViewController(IntPtr handle)
            : base(handle)
        {
        }

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			SaveButton = new UIBarButtonItem("Save", UIBarButtonItemStyle.Plain, this, new Selector("saveButtonPressed"));
			DiscardButton = new UIBarButtonItem("Discard", UIBarButtonItemStyle.Plain, this, new Selector("discardButtonPressed"));
		}

		public void DataModified( bool modified )
		{
			if( NavigationController != null )
			{
				if( modified && NavigationItem.RightBarButtonItem != SaveButton )
				{
					NavigationItem.SetRightBarButtonItem(SaveButton, true);
					NavigationItem.SetLeftBarButtonItem(DiscardButton, true);
				}
				else if( !modified && NavigationItem.RightBarButtonItem == SaveButton )
				{
					NavigationItem.SetRightBarButtonItem(null, true);
					NavigationItem.SetLeftBarButtonItem(null, true);
				}
			}
		}

		[Export ("saveButtonPressed")]
		protected virtual void SaveButtonPressed()
		{
			Console.WriteLine("saveButtonPressed not overridden");
		}

		[Export ("discardButtonPressed")]
		protected virtual void DiscardButtonPressed()
		{
			Console.WriteLine("discardButtonPressed not overridden");
		}
	}
}

