using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVAssignQuotaTableViewController : JVDataSyncTableViewController, IJVTimeInputCell, IJVUserSelectionCellDelegate, IJVDeviceClassSelectionCellDelegate, IJVDaySelectionCellDelegate
	{
		private JVQuotaDataContainer QuotaDataContainerInstance;
		private JVKobiGlobalData KobiGlobalDataInstance;

		public int SelectedDataSetsCount
		{
			get
			{
				return (int)JVShared.Instance.QuotaDataContainer.GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet).Length;
			}
		}

		public JVAssignQuotaTableViewController(IntPtr handle) : base(handle)
		{
			QuotaDataContainerInstance = new JVQuotaDataContainer(JVShared.Instance.QuotaDataContainer);
			KobiGlobalDataInstance = new JVKobiGlobalData(JVShared.Instance.KobiGlobalData);
		}

        #region View Controller Logic
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            TableView.RegisterClassForCellReuse(typeof(JVUserSelectionCell), new NSString("UserSelectionCell"));
            TableView.RegisterClassForCellReuse(typeof(JVDeviceClassSelectionCell), new NSString("DeviceClassSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVDaySelectionCell), new NSString("DaySelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVTimeInputCell), new NSString("TimeInputCell"));

            RefreshView();
        }

        public override void RefreshView()
		{
			GlobalTimePeriodSegmentedControl.SelectedSegment = KobiGlobalDataInstance.QuotaTimePeriodIsDaily ? 1 : 0;
			KobiModeSegmentedControl.SelectedSegment = (int)QuotaDataContainerInstance.GetKobiMode();

            CompareUsers();

            base.RefreshView();
        }
        #endregion View Controller Logic

		#region UI Actions
		partial void GlobalTimePeriodSegmentedControlValueChanged(UISegmentedControl sender)
        {
			KobiGlobalDataInstance.QuotaTimePeriodIsDaily = (sender.SelectedSegment != 0);

            RefreshView();
        }

		partial void KobiModeSegmentedControlValueChanged(UISegmentedControl sender)
        {
			QuotaDataContainerInstance.SetMode((KobiiOS.JVQuotaData.KobiMode)sender.SelectedSegment);

            RefreshView();
        }
		#endregion UI Actions

		#region TableView Logic
        public override int NumberOfSections(UITableView tableView)
        {
			if( SelectedDataSetsCount > 0 )
            {
				if( KobiModeSegmentedControl.SelectedSegment == 1 )
                {
                    return 5;
                }
                return 3;
            }
            return 2;
        }

        public override int RowsInSection(UITableView tableview, int section)
        {
			switch( section )
            {
                case 0:
				{
                    return 1;
				}
                case 1:
				{
                    return 2;
				}
                case 2:
				{
                    return 1;
				}
                case 3:
				{
					if( KobiGlobalDataInstance.QuotaTimePeriodIsDaily && JVShared.Instance.SelectedDaysIndexSet.Count > 0 )
					{
						return 2;
					}
                    else
					{
                        return 1;
					}
				}
                case 4:
				{
                    return 1;
				}
                default:
				{
                    return 0;
				}
            }
        }

        public override string TitleForHeader(UITableView tableView, int section)
        {
			switch( section )
            {
				case 0:
				{
					return "Select Global Time Period";
				}
                case 1:
				{
					return "Select Children and Devices";
				}
                case 2:
				{
					if( KobiModeSegmentedControl.SelectedSegment == (int)KobiiOS.JVQuotaData.KobiMode.Conflicted )
					{
						return "Conflicted Kobi Modes";
					}
                    return "Select Kobi Mode";
				}
                case 3:
				{
					if( KobiGlobalDataInstance.QuotaTimePeriodIsDaily )
					{
						if( QuotaDataContainerInstance.GetDailyQuota() == 0 )
						{
							return "Conflicted Daily Usage Quotas";
						}
						else
						{
							return "Assign Daily Usage Quotas";
						}
					}
                    else
					{
						if( QuotaDataContainerInstance.GetWeeklyQuota() == 0 )
						{
							return "Conflicted Weekly Usage Quotas";
						}
						else
						{
							return "Assign Weekly Usage Quota";
						}
					}
				}
                case 4:
				{
					if( QuotaDataContainerInstance.GetEmergencyHoursQuota() == 0 )
					{
						return "Conflicted Emergency Hours";
					}
					else
					{
						return "Assign Weekly Emergency Hours";
					}
				}
                default:
				{
                    return "";
				}
            }
        }

        public override string TitleForFooter(UITableView tableView, int section)
        {
			if( section == 0 )
			{
				//return "The selected time period will be used for all Children and Devices.";
			}
			else if( section == 1 )
            {
				if( SelectedDataSetsCount == 0 )
                {
					bool userSelected = JVShared.Instance.SelectedUsersIndexSet.Count > 0;
					bool deviceClassSelected = JVShared.Instance.SelectedDeviceClassesIndexSet.Count > 0;

					if( !userSelected && !deviceClassSelected )
					{
						return "Tap to select one or more children & devices.";
					}
					else if( userSelected && !deviceClassSelected )
					{
						return "Tap to select one or more devices.";
					}
					else if( !userSelected && deviceClassSelected )
					{
                        return "Tap to select one or more children.";
					}
                }
				else
				{
					return "Tap to select or deselect children or devices.";
				}
            }
			else if( section == 2 )
            {
				if( KobiModeSegmentedControl.SelectedSegment == (int)KobiiOS.JVQuotaData.KobiMode.Conflicted )
				{
					return "Selecting a new mode will override the existing modes of all selected Children's Devices.";
				}
				else
				{
					switch( KobiModeSegmentedControl.SelectedSegment )
					{
					case 0:
						{
							return "Monitor allows unrestricted use of Kobis.";
						}
					case 2:
						{
							return "Suspension prevents all use of Kobis.";
						}
					}
				}
            }
			else if( section == 3 )
            {
				if( KobiGlobalDataInstance.QuotaTimePeriodIsDaily && QuotaDataContainerInstance.GetDailyQuota() == 0 )
				{
					return "Assigning hours will override the existing hours of all selected Children's Devices.\nMinutes will be rounded to quarter hours.";
				}
				if( !KobiGlobalDataInstance.QuotaTimePeriodIsDaily && QuotaDataContainerInstance.GetWeeklyQuota() == 0 )
				{
					return "Assigning hours will override the existing hours of all selected Children's Devices.\nMinutes will be rounded to quarter hours.";
				}
                return "Minutes will be rounded to quarter hours.";
            }
			else if( section == 4 )
			{
				return "These hours can be used by your child if they run out of normal device hours.\nMinutes will be rounded to quarter hours.";
			}
            return "";
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = null;

			if( indexPath.Section == 1 )
            {
				if( indexPath.Row == 0 )
                {
					cell = TableView.DequeueReusableCell(new NSString("UserSelectionCell"), indexPath);
					((JVUserSelectionCell)cell).Setup(true);
					((JVUserSelectionCell)cell).Delegate = this;
                }
				else if( indexPath.Row == 1 )
				{
					cell = TableView.DequeueReusableCell(new NSString("DeviceClassSelectionCell"), indexPath);
					((JVDeviceClassSelectionCell)cell).Setup(true);
					((JVDeviceClassSelectionCell)cell).Delegate = this;
				}
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            }
			else if( indexPath.Section == 3 )
			{
				if( !KobiGlobalDataInstance.QuotaTimePeriodIsDaily || (KobiGlobalDataInstance.QuotaTimePeriodIsDaily && indexPath.Row == 1) )
				{
					cell = TableView.DequeueReusableCell(new NSString("TimeInputCell"), indexPath);
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
					((JVTimeInputCell)cell).Delegate = this;

					if( KobiGlobalDataInstance.QuotaTimePeriodIsDaily ) // Daily
					{
						((JVTimeInputCell)cell).SetupCell(indexPath.Row, "Assign Daily Hours", QuotaDataContainerInstance.GetDailyQuota(), 24);
					}
					else // Weekly
					{
						((JVTimeInputCell)cell).SetupCell(0, "Assign Weekly Hours", QuotaDataContainerInstance.GetWeeklyQuota());
					}
				}
				else
				{
					cell = TableView.DequeueReusableCell(new NSString("DaySelectionCell"), indexPath);
					((JVDaySelectionCell)cell).Setup(true);
					((JVDaySelectionCell)cell).Delegate = this;
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}

			}
			else if( indexPath.Section == 4 )
			{
				cell = TableView.DequeueReusableCell(new NSString("TimeInputCell"), indexPath);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				((JVTimeInputCell)cell).Delegate = this;
				((JVTimeInputCell)cell).SetupCell(99, "Assign Emergency Hours", QuotaDataContainerInstance.GetEmergencyHoursQuota(), 24);
			}
			else
			{
				cell = base.GetCell(TableView, indexPath);
			}

			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
        }
		#endregion TableView Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			JVShared.Instance.QuotaDataContainer = new JVQuotaDataContainer(QuotaDataContainerInstance);
			JVShared.Instance.KobiGlobalData = new JVKobiGlobalData(KobiGlobalDataInstance);
			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		private void CompareUsers()
		{
			bool isModified = false;

			HashSet<int> quotaConflicts = new HashSet<int>();
			HashSet<int> kobiGlobalConflicts = new HashSet<int>();

			if( !JVShared.Instance.QuotaDataContainer.IsEqualToOther(QuotaDataContainerInstance, quotaConflicts) )
			{
				isModified = true;
			}
			if( !JVShared.Instance.KobiGlobalData.IsEqualToOther(KobiGlobalDataInstance, kobiGlobalConflicts) )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs

		#region IJVUserSelectionCellDelegate implementation
		public void UserSelectionDidChange( int index )
		{
			RefreshView();
		}
		#endregion IJVUserSelectionCellDelegate implementation

		#region IJVDeviceClassSelectionCellDelegate implementation
		public void DeviceClassSelectionDidChange( int index )
		{
			RefreshView();
		}
		#endregion IJVDeviceClassSelectionCellDelegate implementation

		#region IJVDaySelectionCellDelegate implementation
		public void DaySelectionDidChange(int index)
		{
			RefreshView();
		}
		#endregion IJVDaySelectionCellDelegate implementation

		#region IJVTimeInputCell implementation
		public void TimeValueChanged(JVTimeInputCell cell, byte quota)
		{
			int index = ((JVTimeInputCell)cell).Index;

			if( index == 99 )
			{
				QuotaDataContainerInstance.SetEmergencyHoursQuota(quota);
			}
			else if( !KobiGlobalDataInstance.QuotaTimePeriodIsDaily && index == 0 )
			{
				QuotaDataContainerInstance.SetWeeklyQuota(quota);
			}
			else
			{
				QuotaDataContainerInstance.SetDailyQuotas(quota);
			}

			RefreshView();
		}
		#endregion IJVTimeInputCell implementation
    }
}
