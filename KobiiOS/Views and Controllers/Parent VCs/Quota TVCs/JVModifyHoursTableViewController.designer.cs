// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace KobiiOS
{
	[Register ("JVModifyHoursTableViewController")]
	partial class JVModifyHoursTableViewController
	{
		[Outlet]
		MonoTouch.UIKit.UISegmentedControl HoursTypeSegmentedControl { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView MessageTextField { get; set; }

		[Action ("HoursTypeSegmentedControlValueChanged:")]
		partial void HoursTypeSegmentedControlValueChanged (MonoTouch.UIKit.UISegmentedControl sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (HoursTypeSegmentedControl != null) {
				HoursTypeSegmentedControl.Dispose ();
				HoursTypeSegmentedControl = null;
			}

			if (MessageTextField != null) {
				MessageTextField.Dispose ();
				MessageTextField = null;
			}
		}
	}
}
