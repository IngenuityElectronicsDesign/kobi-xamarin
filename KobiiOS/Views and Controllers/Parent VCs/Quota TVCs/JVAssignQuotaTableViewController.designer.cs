// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace KobiiOS
{
	[Register ("JVAssignQuotaTableViewController")]
	partial class JVAssignQuotaTableViewController
	{
		[Outlet]
		MonoTouch.UIKit.UISegmentedControl GlobalTimePeriodSegmentedControl { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl KobiModeSegmentedControl { get; set; }

		[Action ("GlobalTimePeriodSegmentedControlValueChanged:")]
		partial void GlobalTimePeriodSegmentedControlValueChanged (MonoTouch.UIKit.UISegmentedControl sender);

		[Action ("KobiModeSegmentedControlValueChanged:")]
		partial void KobiModeSegmentedControlValueChanged (MonoTouch.UIKit.UISegmentedControl sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (GlobalTimePeriodSegmentedControl != null) {
				GlobalTimePeriodSegmentedControl.Dispose ();
				GlobalTimePeriodSegmentedControl = null;
			}

			if (KobiModeSegmentedControl != null) {
				KobiModeSegmentedControl.Dispose ();
				KobiModeSegmentedControl = null;
			}
		}
	}
}
