using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVModifyHoursTableViewController : JVTableViewController, IJVUserSelectionCellDelegate, IJVDeviceClassSelectionCellDelegate, IJVTimeInputCell
	{
		private byte Quota = 0;

		public int SelectedDataSetsCount
		{
			get
			{
				return (int)JVShared.Instance.QuotaDataContainer.GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet).Length;
			}
		}

		public JVModifyHoursTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse(typeof(JVUserSelectionCell), new NSString("UserSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVDeviceClassSelectionCell), new NSString("DeviceClassSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVTimeInputCell), new NSString("TimeInputCell"));

			UIBarButtonItem submitButton = new UIBarButtonItem("Submit", UIBarButtonItemStyle.Plain, (buttonSender, buttonE) => // Button Pressed
				{
					UIAlertView alertView = new UIAlertView("Submit Hours?", "Are you sure you want to submit these hours?", null, "No", "Yes" );
					alertView.Clicked += (sender, e) =>
					{
						if( e.ButtonIndex == 1 )
						{
							Quota = 0;
							MessageTextField.Text = "";
							RefreshView();
						}
					};
					alertView.Show();
				});
			NavigationItem.SetRightBarButtonItem(submitButton, false);

			MessageTextField.InputAccessoryView = new XIKeyboardToolbar(new RectangleF(0f, 0f, View.Frame.Width, 44.0f), View, null, null);
		}

		public override void RefreshView()
		{
			base.RefreshView();
		}
		#endregion View Controller Logic

		#region UI Actions
		partial void HoursTypeSegmentedControlValueChanged(UISegmentedControl sender)
		{
			RefreshView();
		}
		#endregion UI Actions

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			if( SelectedDataSetsCount > 0 )
			{
				return 4;
			}
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 0 )
			{
				return 2;
			}
			return 1;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Select Children and Devices";
			}
			else if( section == 2 )
			{
				return (HoursTypeSegmentedControl.SelectedSegment == 0 ? "Add Bonus Hours" : "Take Penalty Hours Away");
			}
			else if( section == 3 )
			{
				return "Add a Message";
			}
			return "";
		}

		public override string TitleForFooter(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				if( SelectedDataSetsCount == 0 )
				{
					bool userSelected = JVShared.Instance.SelectedUsersIndexSet.Count > 0;
					bool deviceClassSelected = JVShared.Instance.SelectedDeviceClassesIndexSet.Count > 0;

					if( !userSelected && !deviceClassSelected )
					{
						return "Tap to select one or more children & devices.";
					}
					else if( userSelected && !deviceClassSelected )
					{
						return "Tap to select one or more devices.";
					}
					else if( !userSelected && deviceClassSelected )
					{
						return "Tap to select one or more children.";
					}
				}
				else
				{
					return "Tap to select or deselect children or devices.";
				}
			}
			return "";
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = null;

			if( indexPath.Section == 0 )
			{
				if( indexPath.Row == 0 )
				{
					cell = TableView.DequeueReusableCell(new NSString("UserSelectionCell"), indexPath);
					((JVUserSelectionCell)cell).Setup(true);
					((JVUserSelectionCell)cell).Delegate = this;
				}
				else if( indexPath.Row == 1 )
				{
					cell = TableView.DequeueReusableCell(new NSString("DeviceClassSelectionCell"), indexPath);
					((JVDeviceClassSelectionCell)cell).Setup(true);
					((JVDeviceClassSelectionCell)cell).Delegate = this;
				}
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			else if( indexPath.Section == 2 )
			{
				cell = TableView.DequeueReusableCell(new NSString("TimeInputCell"), indexPath);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				((JVTimeInputCell)cell).Delegate = this;
				((JVTimeInputCell)cell).SetupCell(99, (JVShared.Instance.KobiGlobalData.QuotaTimePeriodIsDaily ? "Hours for Today" : "Hours for Week"), Quota, 24);
			}
			else
			{
				cell = base.GetCell(TableView, indexPath);
			}

			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
		}
		#endregion TableView Logic

		#region IJVUserSelectionCellDelegate implementation
		public void UserSelectionDidChange( int index )
		{
			RefreshView();
		}
		#endregion IJVUserSelectionCellDelegate implementation

		#region IJVDeviceClassSelectionCellDelegate implementation
		public void DeviceClassSelectionDidChange( int index )
		{
			RefreshView();
		}
		#endregion IJVDeviceClassSelectionCellDelegate implementation

		#region IJVTimeInputCell implementation
		public void TimeValueChanged(JVTimeInputCell cell, byte quota)
		{
			int index = ((JVTimeInputCell)cell).Index;
			Quota = quota;

			RefreshView();
		}
		#endregion IJVTimeInputCell implementation
	}
}
