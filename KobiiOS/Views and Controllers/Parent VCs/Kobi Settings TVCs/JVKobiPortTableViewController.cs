using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVKobiPortTableViewController : JVDataSyncTableViewController, IJVKobiNameFieldCellDelegate
	{
		private JVKobiPortData KobiPortDataInstance;

		public JVKobiBoxData KobiBoxDataInstance { get; private set; }
		public int PortIndex { get; private set; }
		private NSIndexPath SelectedIndexPath;
		private bool NewPort;

		public JVKobiPortTableViewController(IntPtr handle) : base (handle) { }

		public void Setup( JVKobiBoxData kobiBoxDataInstance, int portIndex, bool newPort )
		{
			PortIndex = portIndex;
			NewPort = newPort;

			KobiBoxDataInstance = kobiBoxDataInstance;
			KobiPortDataInstance = NewPort ? new JVKobiPortData() : new JVKobiPortData(KobiBoxDataInstance.GetPort(PortIndex));

			Title = string.Format( "Edit {0}", KobiBoxDataInstance.Name );
			SelectedIndexPath = NSIndexPath.FromRowSection(KobiPortDataInstance.DeviceClassIndex, 1);
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVKobiBoxNameFieldCell), new NSString("KobiBoxNameFieldCell") );
			TableView.RegisterClassForCellReuse( typeof(JVDeviceClassCell), new NSString("DeviceClassCell") );
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			RefreshView();
		}

		public override void RefreshView()
		{
			if( !NewPort )
			{
				Title = string.Format( "Edit {0}", KobiPortDataInstance.Name );
			}

			CompareUsers();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 1 )
			{
				return JVShared.Instance.DeviceClassDataContainer.Count;
			}
			return 1;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = base.GetCell(TableView, indexPath);;

			if( indexPath.Section == 0 )
			{
				cell = TableView.DequeueReusableCell(new NSString("KobiBoxNameFieldCell"), indexPath);
				((JVKobiBoxNameFieldCell)cell).Setup(KobiPortDataInstance.Name);
				((JVKobiBoxNameFieldCell)cell).Delegate = this;
			}
			else
			{
				cell = TableView.DequeueReusableCell(new NSString("DeviceClassCell"), indexPath);
				((JVDeviceClassCell)cell).Setup( JVShared.Instance.DeviceClassDataContainer.GetData(indexPath.Row), indexPath.Row, false);

				if( indexPath.Row == SelectedIndexPath.Row )
				{
					cell.Accessory = UITableViewCellAccessory.Checkmark;
				}
				else
				{
					cell.Accessory = UITableViewCellAccessory.None;
				}
			}

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			TableView.DeselectRow(indexPath, true);

			if( indexPath.Section == 1 )
			{
				SelectedIndexPath = indexPath;
				KobiPortDataInstance.DeviceClassIndex = indexPath.Row;
				RefreshView();
			}
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Port Device Name";
			}
			return "Port Device Class";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Tap to change the name.";
			}
			return "Tap to select a class.";
		}
		#endregion Table View Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			KobiBoxDataInstance.SetPort(PortIndex, new JVKobiPortData(KobiPortDataInstance));

			if( NewPort )
			{
				NavigationController.PopViewControllerAnimated(true);
			}

			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		private void CompareUsers()
		{
			bool isModified = false;

			HashSet<int> conflicts = new HashSet<int>();

			if( !NewPort && !KobiBoxDataInstance.GetPort(PortIndex).IsEqualToOther(KobiPortDataInstance, conflicts) )
			{
				isModified = true;
			}
			else if( NewPort && !string.IsNullOrEmpty(KobiPortDataInstance.Name) && KobiPortDataInstance.DeviceClassIndex != -1 )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs

		#region IJVKobiNameFieldCellDelegate implementation
		public void NameChanged(string name)
		{
			KobiPortDataInstance.Name = name;

			RefreshView();
		}
		#endregion IJVKobiNameFieldCellDelegate implementation
	}
}
