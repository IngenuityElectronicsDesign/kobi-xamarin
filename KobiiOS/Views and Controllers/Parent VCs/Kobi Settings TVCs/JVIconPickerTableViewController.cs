using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVIconPickerTableViewController : UITableViewController, IJVIconPickerCellDelegate
	{
		// Events to tell Table Data Containers when Users are added or removed
		public delegate void OnCancelledDelegate();
		public event OnCancelledDelegate OnCancelled;
		public delegate void OnPickedDelegate(string iconImageName);
		public event OnPickedDelegate OnPicked;

		public JVIconPickerTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVIconPickerCell), new NSString("IconPickerCell") );

			Title = "Icons";

			UIBarButtonItem cancelButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Plain, (buttonSender, buttonE) => // Button Pressed
				{
					OnCancelled();
				});
			NavigationItem.SetRightBarButtonItem(cancelButton, false);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return 2;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("IconPickerCell"), indexPath);
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			((JVIconPickerCell)cell).Setup( indexPath.Row == 0 ? new string[] {"TV.png", "Controller.png", "Speakers.png", "Laptop.png"} : new string[] {"Usage.png", "Quota.png", "Schedule.png", "Cog.png"} );
			((JVIconPickerCell)cell).Delegate = this;

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override string TitleForFooter(UITableView tableView, int section)
		{
			return "Tap to select an icon.";
		}
		#endregion Table View Logic

		#region IJVIconPickerCellDelegate implementation
		public void IconPicked(string iconImageName)
		{
			OnPicked(iconImageName);
		}
		#endregion IJVIconPickerCellDelegate implementation
	}
}

