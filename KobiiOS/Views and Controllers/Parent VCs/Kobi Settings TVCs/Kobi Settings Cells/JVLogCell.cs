﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVLogCell : UITableViewCell
	{
		public UILabel LogTextLabel { get; private set; }
		public UILabel TimestampLabel { get; private set; }
		public UIView HighlightBand { get; private set; }

		public JVLogCell(IntPtr handle) : base(handle)
		{
			LogTextLabel = new UILabel(new RectangleF(10, 18, 290, 20));
			LogTextLabel.Font = UIFont.SystemFontOfSize(16f);
			LogTextLabel.TextColor = UIColor.Black;
			AddSubview(LogTextLabel);

			TimestampLabel = new UILabel(new RectangleF(10, 6, 290, 8));
			TimestampLabel.Font = UIFont.SystemFontOfSize(10f);
			TimestampLabel.TextColor = UIColor.Gray;
			TimestampLabel.TextAlignment = UITextAlignment.Right;
			AddSubview(TimestampLabel);

			HighlightBand = new UIView(new RectangleF(310, 0, 10, Bounds.Height));
			HighlightBand.BackgroundColor = UIColor.DarkGray;
			AddSubview(HighlightBand);
		}

		public void Setup(string logText, string timestampText, UIColor highlightBandColor, bool isError)
		{
			LogTextLabel.Text = logText;
			TimestampLabel.Text = timestampText;

			if( highlightBandColor != null )
			{
				HighlightBand.BackgroundColor = highlightBandColor;
			}

			if( isError )
			{
				HighlightBand.BackgroundColor = UIColor.DarkGray;
				LogTextLabel.TextColor = JVShared.Instance.SystemErrorTintColor;
			}
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}
