﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public interface IJVIconPickerCellDelegate
	{
		void IconPicked(string iconImageName);
	}

	public class JVIconPickerCell : UITableViewCell
	{
		private const int NumOfIcons = 4;
		private const int Spacing = 8;
		private const int IconSize = 64;

		public IJVIconPickerCellDelegate Delegate { get; set; }

		public List<string> IconImageNames { get; private set; }
		public List<UIButton> IconButtonList { get; private set; }
		public List<UIView> LinesList { get; private set; }

		public JVIconPickerCell(IntPtr handle) : base(handle)
		{
			IconImageNames = new List<string>();
			IconButtonList = new List<UIButton>();
			LinesList = new List<UIView>();

			for( int i = 0; i < NumOfIcons; i++ )
			{
				UIButton iconButton = new UIButton(new RectangleF(Bounds.Width/NumOfIcons*i + Spacing, Spacing, IconSize, IconSize));
				iconButton.Tag = i;
				iconButton.TouchUpInside += (sender, e) => // Button Pressed
				{
					if( Delegate != null )
					{
						Delegate.IconPicked(IconImageNames[((UIButton)sender).Tag]);
					}
				};
				AddSubview(iconButton);
				IconButtonList.Add(iconButton);
			}

			for( int i = 1; i < NumOfIcons; i++ )
			{
				UIView line = new UIView(new RectangleF((IconSize+Spacing*2)*i, 0, 1, Bounds.Height));
				line.BackgroundColor = UIColor.FromRGBA(199/255f, 199/255f, 204/255f, 1f);
				AddSubview(line);
				LinesList.Add(line);
			}
		}

		public void Setup( string[] iconImageNames )
		{
			for( int i = 0; i < NumOfIcons; i++ )
			{
				IconImageNames.Add(iconImageNames[i]);
				IconButtonList[i].SetImage(UIImage.FromFile(iconImageNames[i]).ImageWithTintColor(JVShared.Instance.GetUserTintColor()), UIControlState.Normal);
			}
		}
	}
}

