﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public interface IJVKobiNameFieldCellDelegate
	{
		void NameChanged(string name);
	} 

	public class JVKobiBoxNameFieldCell  : UITableViewCell
	{
		public IJVKobiNameFieldCellDelegate Delegate { get; set; }

		public UITextField NameTextField { get; set; }

		public JVKobiBoxNameFieldCell(IntPtr handle) : base(handle)
		{
			NameTextField = new UITextField(new RectangleF(15, 6, 290, 32));
			NameTextField.TextColor = UIColor.Black;
			NameTextField.Font = UIFont.SystemFontOfSize(17f);
			NameTextField.Placeholder = "Enter Name...";
			NameTextField.ReturnKeyType = UIReturnKeyType.Done;
			NameTextField.EditingChanged += (sender, args) => // TextField Value Changed
			{
				string text = ((UITextField)sender).Text;
				((UITextField)sender).Text = text.Substring(0, Math.Min(text.Length, JVKobiBoxData.MaxNameLength));
			};
			NameTextField.ShouldReturn += (textField) => // TextField Should Return
			{
				textField.ResignFirstResponder();
				return false;
			};
			NameTextField.EditingDidBegin += (sender, args) => // TextField Did Begin Editing
			{

			};
			NameTextField.EditingDidEnd += (sender, args) => // TextField Did End Editing
			{
				if( Delegate != null )
				{
					Delegate.NameChanged( ((UITextField)sender).Text );
				}
			};
			AddSubview(NameTextField);
		}

		public void Setup(string name)
		{
			NameTextField.Text = name;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}

