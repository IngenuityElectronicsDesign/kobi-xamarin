using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public interface IJVDeviceClassCellDelegate
	{
		void DeviceClassChangedName(int index, string name);
		void DeviceIconPressed(int index);
	} 

	public partial class JVDeviceClassCell : UITableViewCell
	{
		public IJVDeviceClassCellDelegate Delegate { get; set; }

		public bool IsEditing { get; private set; }
		public int Index { get; set; }

		public UILabel ClassNameLabel { get; set; }
		public UITextField ClassNameTextField { get; set; }
		public UIImageView ClassIconView { get; set; }
		public UIButton ClassIconButton { get; set; }

		public JVDeviceClassCell(IntPtr handle) : base(handle)
		{
			ClassNameLabel = new UILabel(new RectangleF(62, 0, 196, 44));
			ClassNameLabel.TextColor = UIColor.Black;
			ClassNameLabel.Font = UIFont.SystemFontOfSize(18f);
			ClassNameLabel.Hidden = true;
			AddSubview(ClassNameLabel);

			ClassNameTextField = new UITextField(new RectangleF(62, 0, 196, 44));
			ClassNameTextField.TextColor = UIColor.Black;
			ClassNameTextField.Font = UIFont.SystemFontOfSize(17f);
			ClassNameTextField.Hidden = true;
			ClassNameTextField.Placeholder = "Add New Class...";
			ClassNameTextField.ReturnKeyType = UIReturnKeyType.Done;
			ClassNameTextField.ValueChanged += (sender, args) => // TextField  Value Changed
			{
				string text = ((UITextField)sender).Text;
				((UITextField)sender).Text = text.Substring(0, Math.Min(text.Length, JVDeviceClassData.MaxNameLength));
			};
			ClassNameTextField.ShouldReturn += (textField) => // TextField Should Return
			{
				textField.ResignFirstResponder();
				return false;
			};
			ClassNameTextField.EditingDidBegin += (sender, args) => // TextField Did Begin Editing
			{
				
			};
			ClassNameTextField.EditingDidEnd += (sender, args) => // TextField Did End Editing
			{
				if( Delegate != null )
				{
					Delegate.DeviceClassChangedName(Index, ((UITextField)sender).Text);
				}
			};
			AddSubview(ClassNameTextField);

			ClassIconView = new UIImageView(new RectangleF(15, 6, 32, 32));
			ClassIconView.Hidden = true;
			AddSubview(ClassIconView);

			ClassIconButton = new UIButton(new RectangleF(15, 6, 32, 32));
			ClassIconButton.Hidden = true;
			ClassIconButton.TouchUpInside += (sender, e) => // Button Pressed
			{
				if( Delegate != null )
				{
					Delegate.DeviceIconPressed(Index);
				}
			};
			AddSubview(ClassIconButton);
		}

		public void Setup(JVDeviceClassData deviceClass, int index, bool isEditing)
		{
			if( deviceClass != null )
			{
				ClassIconView.Image = deviceClass.Icon;
				ClassIconButton.SetImage(deviceClass.Icon, UIControlState.Normal);
				ClassIconButton.SetImage(deviceClass.Icon, UIControlState.Selected);
				ClassNameLabel.Text = deviceClass.Name;
				ClassNameTextField.Text = deviceClass.Name;
			}
			else
			{
				ClassIconView.Image = null;
				ClassIconButton.SetImage(null, UIControlState.Normal);
				ClassIconButton.SetImage(null, UIControlState.Selected);
				ClassNameLabel.Text = "";
				ClassNameTextField.Text = "";
			}

			Index = index;
			SetEditingMode(isEditing);
		}

		public void SetEditingMode(bool isEditing)
		{
			IsEditing = isEditing;

			ClassIconView.Hidden = IsEditing;
			ClassIconButton.Hidden = !IsEditing;
			ClassNameLabel.Hidden = IsEditing;
			ClassNameTextField.Hidden = !IsEditing;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}
