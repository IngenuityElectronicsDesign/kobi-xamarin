﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVKobiPortCell : UITableViewCell
	{
		public UIImageView IconImageView { get; private set; }

		public UILabel DeviceNameLabel { get; private set; }
		public UILabel DeviceTypeLabel { get; private set; }

		public JVKobiPortCell(IntPtr handle) : base (handle)
		{
			IconImageView = new UIImageView(new RectangleF(15, 12, 64, 64));
			AddSubview(IconImageView);

			DeviceNameLabel = new UILabel(new RectangleF(94, 19, 200, 21));
			DeviceNameLabel.Font = UIFont.BoldSystemFontOfSize(17f);
			DeviceNameLabel.TextColor = UIColor.Black;
			AddSubview(DeviceNameLabel);

			DeviceTypeLabel = new UILabel(new RectangleF(94, 48, 200, 21));
			DeviceTypeLabel.Font = UIFont.SystemFontOfSize(17f);
			DeviceTypeLabel.TextColor = UIColor.Black;
			AddSubview(DeviceTypeLabel);
		}

		public void Setup(JVKobiPortData data)
		{
			if( data == null )
			{
				return;
			}

			IconImageView.Image = JVShared.Instance.DeviceClassDataContainer.GetData(data.DeviceClassIndex).Icon.ImageWithTintColor(JVShared.Instance.GetUserTintColor());

			DeviceNameLabel.Text = data.Name;
			DeviceTypeLabel.Text = JVShared.Instance.DeviceClassDataContainer.GetData(data.DeviceClassIndex).Name;
		}
	}
}

