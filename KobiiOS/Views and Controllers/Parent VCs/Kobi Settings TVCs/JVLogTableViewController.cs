using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVLogTableViewController : JVTableViewController
	{
		string[] textArray;
		UIColor[] colorArray;
		bool[] errorArray;

		public JVLogTableViewController(IntPtr handle) : base(handle)
		{
			textArray = new string[] {"+2 Hours added to Johnny's Games", "+4 Hours added to everyone's TVs", "Dad's PIN was changed", "Billy's Kobi lost connection for 253 seconds", "Billy's Kobi added to the network"};
			colorArray = new UIColor[] {JVShared.Instance.GetUserTintColor(JVShared.Instance.UserDataContainer.GetData(0, false).TintColorIndex), JVShared.Instance.SystemTintColor, UIColor.DarkGray, UIColor.DarkGray, UIColor.DarkGray};
			errorArray = new bool[] {false, false, true, true, true};
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse(typeof(JVLogCell), new NSString("LogCell"));

			UIBarButtonItem clearButton = new UIBarButtonItem("Clear", UIBarButtonItemStyle.Plain, (buttonSender, buttonE) => // Button Pressed
				{
					UIAlertView alertView = new UIAlertView("Clear Security Log?", "Are you sure you want to clear the security log? This cannot be undone.", null, "No", "Yes" );
					alertView.Clicked += (sender, e) =>
					{
						if( e.ButtonIndex == 1 )
						{
							textArray = null;
							RefreshView();
						}
					};
					alertView.Show();
				});
			NavigationItem.SetRightBarButtonItem(clearButton, false);

			RefreshView();
		}

		public override void RefreshView()
		{
			base.RefreshView();
		}
		#endregion View Controller Logic

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( textArray == null )
			{
				return 0;
			}
			return textArray.Length;
		}

		public override string TitleForFooter(UITableView tableView, int section)
		{
			if( textArray == null )
			{
				return "";
			}
			return "Tap for full message with timestamp.";
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("LogCell"), indexPath);;
			((JVLogCell)cell).Setup(textArray[indexPath.Row], "23/02/2014", colorArray[indexPath.Row], errorArray[indexPath.Row] );

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.SeparatorInset = new UIEdgeInsets(0,0,0,2);

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			UIAlertView alertView = new UIAlertView("", string.Format("{0}.\n\nAt 13:15:43 on 21/02/2014", textArray[indexPath.Row]), null, "Done", null );
			alertView.Show();
		}
		#endregion TableView Logic
	}
}
