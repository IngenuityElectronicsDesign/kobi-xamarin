using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVManageDeviceClassTableViewController : JVDataSyncTableViewController, IJVDeviceClassCellDelegate
	{
		private JVDeviceClassDataContainer DeviceClassDataContainerInstance;

		private UIActionSheet DeleteConfirmationActionSheet;
		private int DeletionIndex;

		public JVManageDeviceClassTableViewController(IntPtr handle) : base(handle)
		{
			DeviceClassDataContainerInstance = new JVDeviceClassDataContainer(JVShared.Instance.DeviceClassDataContainer);
		}

		#region View Controller Logic
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVDeviceClassCell), new NSString("DeviceClassCell") );
		}

		public override void RefreshView()
		{
			CompareData();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return Math.Min(DeviceClassDataContainerInstance.Count+1, JVDeviceClassDataContainer.MaxNumDeviceClasses);
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("DeviceClassCell"), indexPath);
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			((JVDeviceClassCell)cell).Setup( ( indexPath.Row == DeviceClassDataContainerInstance.Count ? null : DeviceClassDataContainerInstance.DeviceClassDataList[indexPath.Row] ), indexPath.Row, true );
			((JVDeviceClassCell)cell).Delegate = this;

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			return "Tap to change an icon or to rename a device class. Stick to short, simple names! Swipe to remove a device class.";
		}

		public override string TitleForDeleteConfirmation (UITableView tableView, NSIndexPath indexPath)
		{
			return string.Format("Remove {0}", DeviceClassDataContainerInstance.DeviceClassDataList[indexPath.Row].Name);
		}

		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Row == 0 && DeviceClassDataContainerInstance.Count == 1 )
			{
				return false;
			}
			if( indexPath.Row != DeviceClassDataContainerInstance.Count )
			{
				return true;
			}
			return false;
		}

		public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			if( editingStyle == UITableViewCellEditingStyle.Delete )
			{
				DeletionIndex = indexPath.Row;
				string name = DeviceClassDataContainerInstance.DeviceClassDataList[DeletionIndex].Name;
				DeleteConfirmationActionSheet = new UIActionSheet(string.Format("Are you sure you want to remove {0}?\nThis will permanantly delete this device class, and any associated quotas and schedules.\nThis cannot be undone.", name), 
					null, "Cancel", string.Format("Remove {0}", name));
				DeleteConfirmationActionSheet.Clicked += (sender, e) => // ActionSheet Button Pressed
				{
					if( e.ButtonIndex == 0 ) // Delete
					{
						DeviceClassDataContainerInstance.Remove(DeletionIndex);
					}

					TableView.ReloadSections(new NSIndexSet(0), UITableViewRowAnimation.Fade);
					RefreshView();
				};
				DeleteConfirmationActionSheet.ShowInView(View);
			}
		}
		#endregion Table View Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			JVShared.Instance.DeviceClassDataContainer.MigrateData(DeviceClassDataContainerInstance);
			DeviceClassDataContainerInstance = new JVDeviceClassDataContainer(JVShared.Instance.DeviceClassDataContainer);
			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		private void CompareData()
		{
			bool isModified = false;

			HashSet<int> conflicts = new HashSet<int>();

			if( !JVShared.Instance.DeviceClassDataContainer.IsEqualToOther(DeviceClassDataContainerInstance, conflicts) )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs

		#region IJVDeviceClassCellDelegate implementation
		public void DeviceClassChangedName(int index, string name)
		{
			if( index < DeviceClassDataContainerInstance.Count )
			{
				JVDeviceClassData deviceClass = DeviceClassDataContainerInstance.DeviceClassDataList[index];
				deviceClass.Name = name;
			}
			else
			{
				DeviceClassDataContainerInstance.Add(name, UIImage.FromFile("TV.png"));
			}

			RefreshView();
		}

		public void DeviceIconPressed(int index)
		{
			JVIconPickerTableViewController IconPickerController = (JVIconPickerTableViewController)JVShared.Instance.Storyboard.InstantiateViewController("IconPickerTVC");

			UINavigationController NavController = new UINavigationController();
			NavController.Title = "Icons";
			NavController.AddChildViewController(IconPickerController);

			IconPickerController.OnCancelled += () => // Cancelled
			{
				DismissViewController(true, null);
			};
			IconPickerController.OnPicked += (iconImageName) => // Icon Picked
			{
				JVDeviceClassData deviceClass = DeviceClassDataContainerInstance.DeviceClassDataList[index];
				deviceClass.Icon = UIImage.FromFile(iconImageName).ImageWithTintColor(JVShared.Instance.GetUserTintColor());
				DismissViewController(true, null);
				RefreshView();
			};
			PresentViewController(NavController, true, null);
		}
		#endregion IJVDeviceClassCellDelegate implementation
	}
}
