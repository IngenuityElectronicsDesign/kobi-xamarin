using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVKobiNetworkTableViewController : JVDataSyncTableViewController
	{
		private JVKobiBoxDataContainer KobiBoxDataContainerInstance;

		private UIActionSheet DeleteConfirmationActionSheet;
		private int DeletionIndex;

		public JVKobiNetworkTableViewController(IntPtr handle) : base(handle)
		{
			KobiBoxDataContainerInstance = new JVKobiBoxDataContainer(JVShared.Instance.KobiBoxDataContainer);
		}

		#region View Controller Logic
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(UITableViewCell), new NSString("KobiNetworkReuseCell") );
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			RefreshView();
		}

		public override void RefreshView()
		{
			CompareData();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return Math.Min(KobiBoxDataContainerInstance.Count+1, JVKobiBoxDataContainer.MaxNumKobiBoxes);
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("KobiNetworkReuseCell"), indexPath);

			JVKobiBoxData data = ( indexPath.Row == KobiBoxDataContainerInstance.Count ? null : KobiBoxDataContainerInstance.GetData(indexPath.Row) );

			cell.TextLabel.Text = ( data != null ? data.Name : "Add New Kobi Box" );
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			if( data == null )
			{
				cell.TextLabel.Font = UIFont.BoldSystemFontOfSize(17f);
			}
			else
			{
				cell.TextLabel.Font = UIFont.SystemFontOfSize(17f);
			}

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			JVKobiBoxTableViewController toPush = (JVKobiBoxTableViewController)JVShared.Instance.Storyboard.InstantiateViewController("KobiBoxTVC");

			if( indexPath.Row == KobiBoxDataContainerInstance.Count )
			{
				toPush.Setup(KobiBoxDataContainerInstance);
			}
			else
			{
				toPush.Setup(KobiBoxDataContainerInstance, indexPath.Row);
			}

			NavigationController.PushViewController(toPush, true);
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			return "Tap to edit a Kobi Box. Swipe to remove.";
		}

		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Row != KobiBoxDataContainerInstance.Count )
			{
				return true;
			}
			return false;
		}

		public override string TitleForDeleteConfirmation (UITableView tableView, NSIndexPath indexPath)
		{
			return string.Format("Remove {0}", KobiBoxDataContainerInstance.GetData(indexPath.Row).Name);
		}

		public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			if( editingStyle == UITableViewCellEditingStyle.Delete )
			{
				DeletionIndex = indexPath.Row;
				string name = KobiBoxDataContainerInstance.GetData(DeletionIndex).Name;
				DeleteConfirmationActionSheet = new UIActionSheet(string.Format("Are you sure you want to remove {0}?\nThis will permanatly remove this Kobi Box from your network. This cannot be undone.", name), 
					null, "Cancel", string.Format("Remove {0}", name));
				DeleteConfirmationActionSheet.Clicked += (sender, e) => // ActionSheet Button Pressed
				{
					if( e.ButtonIndex == 0 ) // Delete
					{
						KobiBoxDataContainerInstance.Remove(DeletionIndex);
					}
					RefreshView();
				};
				DeleteConfirmationActionSheet.ShowInView(View);
			}
		}
		#endregion Table View Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			JVShared.Instance.KobiBoxDataContainer.MigrateData(KobiBoxDataContainerInstance);
			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			UIAlertView alertView = new UIAlertView("Discard Unsaved Changes?", "Are you sure you want to discard all of your unsaved changes?", null, "Discard Changes and Exit", "Keep Changes", "Save Changes and Exit");
			alertView.Clicked += (sender, e) => // Alert View Pressed
			{
				if( e.ButtonIndex == 0 ) // Discard
				{
					NavigationController.PopViewControllerAnimated(true);
				}
				if( e.ButtonIndex == 2 ) // Save
				{
					SaveButtonPressed();
					NavigationController.PopViewControllerAnimated(true);
				}
			};
			alertView.Show();
		}

		private void CompareData()
		{
			bool isModified = false;

			HashSet<int> conflicts = new HashSet<int>();

			if( !JVShared.Instance.KobiBoxDataContainer.IsEqualToOther(KobiBoxDataContainerInstance, conflicts) )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs
	}
}
