using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVTimeSyncTableViewController : UITableViewController
	{
		NSTimer Timer;
		DateTime KobiTime;

		public JVTimeSyncTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			KobiTime = DateTime.Today;

			PhoneTimeLabel.Text = string.Format("{0:hh\\:mm\\:ss}", DateTime.Now);
			KobiTimeLabel.Text = string.Format("{0:hh\\:mm\\:ss}", KobiTime);

			TableView.RegisterClassForCellReuse(typeof(JVUsageCell), new NSString("UsageCell"));
		}

		public override void ViewWillAppear(bool animated)
		{
			Timer = NSTimer.CreateRepeatingScheduledTimer(1f, () => // Timer Fired
				{
					KobiTime = KobiTime.AddSeconds(1);
					PhoneTimeLabel.Text = string.Format("{0:hh\\:mm\\:ss}", DateTime.Now);
					KobiTimeLabel.Text = string.Format("{0:hh\\:mm\\:ss}", KobiTime);

				});
		}

		public override void ViewWillDisappear(bool animated)
		{
			Timer.Invalidate();
			Timer = null;
		}
		#endregion View Controller Logic

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 0 )
			{
				return 2;
			}
			return 1;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = base.GetCell(TableView, indexPath);

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			TableView.DeselectRow(indexPath, true);
			KobiTime = DateTime.Now;
		}
		#endregion TableView Logic
	}
}
