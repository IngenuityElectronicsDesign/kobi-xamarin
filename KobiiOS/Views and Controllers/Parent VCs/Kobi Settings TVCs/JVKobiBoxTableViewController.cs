using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVKobiBoxTableViewController : JVDataSyncTableViewController, IJVKobiNameFieldCellDelegate
	{
		private JVKobiBoxData KobiBoxDataInstance;

		public JVKobiBoxDataContainer KobiBoxDataContainerInstance { get; private set; }
		public int BoxIndex { get; private set; }

		public JVKobiBoxTableViewController(IntPtr handle) : base (handle) { }

		public void Setup( JVKobiBoxDataContainer kobiBoxDataContainer, int dataIndex )
		{
			BoxIndex = dataIndex;

			KobiBoxDataContainerInstance = kobiBoxDataContainer;
			KobiBoxDataInstance = new JVKobiBoxData(KobiBoxDataContainerInstance.GetData(BoxIndex));

			Title = string.Format( "Edit {0}", KobiBoxDataInstance.Name );
		}

		public void Setup( JVKobiBoxDataContainer kobiBoxDataContainer )
		{
			BoxIndex = -1;

			KobiBoxDataContainerInstance = kobiBoxDataContainer;
			KobiBoxDataInstance = new JVKobiBoxData();

			Title = "Setup New Kobi Box";
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVKobiBoxNameFieldCell), new NSString("KobiBoxNameFieldCell") );
			TableView.RegisterClassForCellReuse( typeof(JVKobiPortCell), new NSString("KobiPortCell") );
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			RefreshView();
		}

		public override void RefreshView()
		{
			if( BoxIndex != -1 )
			{
				Title = string.Format( "Edit {0}", KobiBoxDataInstance.Name );
			}

			CompareUsers();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 3;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return 1;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = base.GetCell(TableView, indexPath);;

			if( indexPath.Section == 0 )
			{
				cell = TableView.DequeueReusableCell(new NSString("KobiBoxNameFieldCell"), indexPath);
				((JVKobiBoxNameFieldCell)cell).Setup(KobiBoxDataInstance.Name);
				((JVKobiBoxNameFieldCell)cell).Delegate = this;
			}
			else
			{
				cell = TableView.DequeueReusableCell(new NSString("KobiPortCell"), indexPath);
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				((JVKobiPortCell)cell).Setup( (indexPath.Section == 1 ? KobiBoxDataInstance.Port1 : KobiBoxDataInstance.Port2) );
			}

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			TableView.DeselectRow(indexPath, true);

			if( indexPath.Section != 0 )
			{
				JVKobiPortTableViewController toPush = (JVKobiPortTableViewController)JVShared.Instance.Storyboard.InstantiateViewController("KobiPortTVC");
				if( BoxIndex != -1 )
				{
					toPush.Setup(KobiBoxDataInstance, indexPath.Section, false);
				}
				else
				{
					toPush.Setup(KobiBoxDataInstance, indexPath.Section, true);
				}
				NavigationController.PushViewController(toPush, true);
			}
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section != 0 )
			{
				return 88;
			}
			return 44;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if( section != 0 )
			{
				return string.Format("Configure Port {0}", section);
			}
			return "Kobi Name";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Tap to change the name.";
			}
			return "";
		}
		#endregion Table View Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			if( KobiBoxDataContainerInstance.GetData(BoxIndex) != null )
			{
				KobiBoxDataContainerInstance.SetData(BoxIndex, new JVKobiBoxData(KobiBoxDataInstance));
			}
			else
			{
				KobiBoxDataContainerInstance.Add(KobiBoxDataInstance);
				NavigationController.PopViewControllerAnimated(true);
			}

			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		private void CompareUsers()
		{
			bool isModified = false;

			HashSet<int> conflicts = new HashSet<int>();

			if( BoxIndex != -1 && !KobiBoxDataContainerInstance.GetData(BoxIndex).IsEqualToOther(KobiBoxDataInstance, conflicts) )
			{
				isModified = true;
			}
			else if( BoxIndex == -1 && !string.IsNullOrEmpty(KobiBoxDataInstance.Name) && KobiBoxDataInstance.Port1 != null && KobiBoxDataInstance.Port2 != null )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs

		#region IJVKobiNameFieldCellDelegate implementation
		public void NameChanged(string name)
		{
			KobiBoxDataInstance.Name = name;

			RefreshView();
		}
		#endregion IJVKobiNameFieldCellDelegate implementation
	}
}
