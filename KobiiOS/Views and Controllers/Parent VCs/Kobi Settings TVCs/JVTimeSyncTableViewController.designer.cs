// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace KobiiOS
{
	[Register ("JVTimeSyncTableViewController")]
	partial class JVTimeSyncTableViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel KobiTimeLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel PhoneTimeLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (PhoneTimeLabel != null) {
				PhoneTimeLabel.Dispose ();
				PhoneTimeLabel = null;
			}

			if (KobiTimeLabel != null) {
				KobiTimeLabel.Dispose ();
				KobiTimeLabel = null;
			}
		}
	}
}
