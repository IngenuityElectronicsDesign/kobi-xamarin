using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVAssignBlockoutTableViewController : JVDataSyncTableViewController, IJVTimeInputCell, IJVUserSelectionCellDelegate, IJVDeviceClassSelectionCellDelegate, IJVDaySelectionCellDelegate
	{
		private JVScheduleDataContainer ScheduleDataContainerInstance;

		public int SelectedDataSetsCount
		{
			get
			{
				return (int)JVShared.Instance.ScheduleDataContainer.GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet).Length;
			}
		}

		public JVAssignBlockoutTableViewController(IntPtr handle) : base(handle)
		{
			ScheduleDataContainerInstance = new JVScheduleDataContainer(JVShared.Instance.ScheduleDataContainer);
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse(typeof(JVUserSelectionCell), new NSString("UserSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVDeviceClassSelectionCell), new NSString("DeviceClassSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVDaySelectionCell), new NSString("DaySelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVScheduleDisplayCell), new NSString("ScheduleDisplayCell"));
			TableView.RegisterClassForCellReuse(typeof(JVTimeInputCell), new NSString("TimeInputCell"));

			BlockSelectionSegmentedControl.SelectedSegment = 0;

			RefreshView();
		}

		public override void RefreshView()
		{
			CompareUsers();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region UI Actions
		partial void BlockSelectionSegmentedControlValueChanged(UISegmentedControl sender)
		{
			RefreshView();
		}
		#endregion UI Actions

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			if( SelectedDataSetsCount > 0 )
			{
				if( JVShared.Instance.SelectedDaysIndexSet.Count > 0 )
				{
					return 3;
				}
				return 2;
			}
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 0 )
			{
				return 2;
			}
			else if( section == 1 )
			{
				return 1;
			}
			return 4;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Select Children and Devices";
			}
			else if( section == 1 )
			{
				return "Select Days";
			}
			else if( section == 2 )
			{
				if( ScheduleDataContainerInstance.GetDailySchedule(BlockSelectionSegmentedControl.SelectedSegment, true) == 0 || ScheduleDataContainerInstance.GetDailySchedule(BlockSelectionSegmentedControl.SelectedSegment, false) == 0 )
				{
					return "Conflicted Usage Blocks";
				}
				return "Manage Usage Blocks";
			}
			return "";
		}

		public override string TitleForFooter(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				if( SelectedDataSetsCount == 0 )
				{
					bool userSelected = JVShared.Instance.SelectedUsersIndexSet.Count > 0;
					bool deviceClassSelected = JVShared.Instance.SelectedDeviceClassesIndexSet.Count > 0;

					if( !userSelected && !deviceClassSelected )
					{
						return "Tap to select one or more children & devices.";
					}
					else if( userSelected && !deviceClassSelected )
					{
						return "Tap to select one or more devices.";
					}
					else if( !userSelected && deviceClassSelected )
					{
						return "Tap to select one or more children.";
					}
				}
				else
				{
					return "Tap to select or deselect children or devices.";
				}
			}
			else if( section == 1 )
			{
				return "Tap to select or deselect days to manage.";
			}
			else if( section == 2 )
			{
				if( ScheduleDataContainerInstance.GetDailySchedule(BlockSelectionSegmentedControl.SelectedSegment, true) == 0 || ScheduleDataContainerInstance.GetDailySchedule(BlockSelectionSegmentedControl.SelectedSegment, false) == 0 )
				{
					return "Assigning times will override the existing schdeule of all selected Children's Devices.\nMinutes will be rounded to quarter hours.";
				}
				return "The end time must be greater than the start time. Minutes will be rounded to quarter hours.";
			}
			return "";
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = null;

			if( indexPath.Section == 0 )
			{
				if( indexPath.Row == 0 )
				{
					cell = TableView.DequeueReusableCell(new NSString("UserSelectionCell"), indexPath);
					((JVUserSelectionCell)cell).Setup(true);
					((JVUserSelectionCell)cell).Delegate = this;
				}
				else if( indexPath.Row == 1 )
				{
					cell = TableView.DequeueReusableCell(new NSString("DeviceClassSelectionCell"), indexPath);
					((JVDeviceClassSelectionCell)cell).Setup(true);
					((JVDeviceClassSelectionCell)cell).Delegate = this;
				}
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			else if( indexPath.Section == 1 )
			{
				cell = TableView.DequeueReusableCell(new NSString("DaySelectionCell"), indexPath);
				((JVDaySelectionCell)cell).Setup(true);
				((JVDaySelectionCell)cell).Delegate = this;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			else if( indexPath.Section == 2 )
			{
				if( indexPath.Row == 0 )
				{
					cell = TableView.DequeueReusableCell(new NSString("ScheduleDisplayCell"), indexPath);
					byte[] startVals = new byte[3];
					byte[] endVals = new byte[3];
					for( int i = 0; i < JVScheduleData.NumOfBlocks; i++ )
					{
						startVals[i] = ScheduleDataContainerInstance.GetDailySchedule(i, true);
						endVals[i] = ScheduleDataContainerInstance.GetDailySchedule(i, false);
					}
					((JVScheduleDisplayCell)cell).SetBlockView(startVals, endVals, BlockSelectionSegmentedControl.SelectedSegment);
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}
				else if( indexPath.Row == 1 )
				{
					cell = base.GetCell(TableView, indexPath);
				}
				else
				{
					cell = TableView.DequeueReusableCell(new NSString("TimeInputCell"), indexPath);
					((JVTimeInputCell)cell).SetupCell(indexPath.Row-2, indexPath.Row == 2 ? "Start Time" : "End Time", ScheduleDataContainerInstance.GetDailySchedule(BlockSelectionSegmentedControl.SelectedSegment, (indexPath.Row == 2)), 24);
					((JVTimeInputCell)cell).Delegate = this;
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}
			}
			else
			{
				cell = base.GetCell(TableView, indexPath);
			}

			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
		}
		#endregion TableView Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			JVShared.Instance.ScheduleDataContainer = new JVScheduleDataContainer(ScheduleDataContainerInstance);

			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		private void CompareUsers()
		{
			bool isModified = false;

			HashSet<int> scheduleConflicts = new HashSet<int>();

			if( !JVShared.Instance.ScheduleDataContainer.IsEqualToOther(ScheduleDataContainerInstance, scheduleConflicts) )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs

		#region IJVUserSelectionCellDelegate implementation
		public void UserSelectionDidChange( int index )
		{
			RefreshView();
		}
		#endregion IJVUserSelectionCellDelegate implementation

		#region IJVDeviceClassSelectionCellDelegate implementation
		public void DeviceClassSelectionDidChange( int index )
		{
			RefreshView();
		}
		#endregion IJVDeviceClassSelectionCellDelegate implementation

		#region IJVDaySelectionCellDelegate implementation
		public void DaySelectionDidChange(int index)
		{
			RefreshView();
		}
		#endregion IJVDaySelectionCellDelegate implementation

		#region IJVTimeInputCell implementation
		public void TimeValueChanged(JVTimeInputCell cell, byte quota)
		{
			int index = ((JVTimeInputCell)cell).Index;

			if( index == 1 && quota != 0 && ScheduleDataContainerInstance.GetDailySchedule(BlockSelectionSegmentedControl.SelectedSegment, false) >= quota )
			{

			}
			else
			{
				ScheduleDataContainerInstance.SetDailySchedule(quota, BlockSelectionSegmentedControl.SelectedSegment, (index == 0));
			}

			RefreshView();
		}
		#endregion IJVTimeInputCell implementation
	}
}
