using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVViewUsageTableViewController : JVTableViewController, IJVUserSelectionCellDelegate
	{
		private int SelectedUserIndex;

		public JVViewUsageTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse(typeof(JVUserSelectionCell), new NSString("UserSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVUsageCell), new NSString("UsageCell"));

			RefreshView();

			SelectedUserIndex = 0;

			if( !JVShared.Instance.LoggedInUserData.IsAdmin )
			{
				SelectedUserIndex = JVShared.Instance.UserDataContainer.GetIndex(JVShared.Instance.LoggedInUserData);
			}

			historyGraphImageView.Image = historyGraphImageView.Image.ImageWithTintColor(JVShared.Instance.GetUserTintColor());
		}

		public override void RefreshView()
		{
			base.RefreshView();
		}
		#endregion View Controller Logic

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 3;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 0 && !JVShared.Instance.LoggedInUserData.IsAdmin )
			{
				return 0;
			}
			if( section == 1 )
			{
				return JVShared.Instance.DeviceClassDataContainer.Count;
			}
			return 1;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			string period = JVShared.Instance.KobiGlobalData.QuotaTimePeriodIsDaily ? "Today" : "This Week";
			string name = JVShared.Instance.LoggedInUserData.IsAdmin ? string.Format(JVShared.Instance.UserDataContainer.ChildUserDataList[SelectedUserIndex].Name + "'s") : "My";

			switch( section )
			{
				case 0:
				{
					if( JVShared.Instance.LoggedInUserData.IsAdmin )
					{
						return "Select A Child to View";
					}
					return "";
				}
				case 1:
				{
					return string.Join( "", name, " Usage Overview ", period);
				}
				case 2:
				{
					return string.Join( "", name, " Usage History ", period);
				}
				default:
				{
					return "";
				}
			}
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 1 || indexPath.Section == 2 )
			{
				return 88;
			}
			return 44;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = null;

			if( indexPath.Section == 0 )
			{
				cell = TableView.DequeueReusableCell(new NSString("UserSelectionCell"), indexPath);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				((JVUserSelectionCell)cell).Setup(false);
				((JVUserSelectionCell)cell).Delegate = this;
			}
			else if( indexPath.Section == 1 )
			{
				cell = TableView.DequeueReusableCell(new NSString("UsageCell"), indexPath);
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				((JVUsageCell)cell).Setup(indexPath.Row, SelectedUserIndex, indexPath.Row);
			}
			else
			{
				cell = base.GetCell(TableView, indexPath);
			}

			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
		}
		#endregion TableView Logic

		#region IJVUserSelectionCellDelegate implementation
		public void UserSelectionDidChange(int index)
		{
			SelectedUserIndex = index;
			RefreshView();
		}
		#endregion
	}
}
