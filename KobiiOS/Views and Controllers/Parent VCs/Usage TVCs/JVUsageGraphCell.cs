﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVUsageGraphCell : UITableViewCell
	{
		const int Padding = 20;

		private XIBarGraphView BarGraphView;

		public JVUsageGraphCell(IntPtr handle) : base (handle)
		{
			BarGraphView = new XIBarGraphView(new RectangleF(Padding, Padding, Bounds.Width-(Padding*2), Bounds.Height-(Padding*2)));
			AddSubview(BarGraphView);
		}

		public void Setup(float[] dataArray, string[] labelTextArray)
		{
			BarGraphView.DrawBarsInGraph(dataArray, labelTextArray);
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}

