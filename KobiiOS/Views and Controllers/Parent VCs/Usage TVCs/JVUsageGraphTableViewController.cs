using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVUsageGraphTableViewController : JVTableViewController, IJVUserSelectionCellDelegate, IJVDeviceClassSelectionCellDelegate
	{
		private int SelectedUserIndex;
		private int SelectedDeviceIndex;

		public int SelectedDataSetsCount
		{
			get
			{
				return (int)JVShared.Instance.QuotaDataContainer.GetDataArray(JVShared.Instance.SelectedUsersIndexSet, JVShared.Instance.SelectedDeviceClassesIndexSet).Length;
			}
		}

		public JVUsageGraphTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse(typeof(JVUserSelectionCell), new NSString("UserSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVDeviceClassSelectionCell), new NSString("DeviceClassSelectionCell"));
			TableView.RegisterClassForCellReuse(typeof(JVUsageGraphCell), new NSString("UsageGraphCell"));

			RefreshView();
		}

		public override void RefreshView()
		{
			base.RefreshView();
		}
		#endregion View Controller Logic

		#region TableView Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 0 )
			{
				if( JVShared.Instance.LoggedInUserData.IsAdmin )
				{
					return 2;
				}
				else
				{
					return 1;
				}
			}
			return 1;
		}

		public override string TitleForHeader(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				if( JVShared.Instance.LoggedInUserData.IsAdmin )
				{
					return "Select A Child and A Device";
				}
				else
				{
					return "Select A Device";
				}
			}
			return "Usage History Graph";
		}

		public override string TitleForFooter(UITableView tableView, int section)
		{
			if( section == 0 )
			{
				if( JVShared.Instance.LoggedInUserData.IsAdmin )
				{
					return "Tap to select a child and a device.";
				}
				else
				{
					return "Tap to select a device.";
				}

			}
			return "";
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 1 )
			{
				return JVShared.Instance.IsWidescreen ? 430 : 340;
			}
			return 44;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = null;

			if( indexPath.Section == 0 )
			{
				if( JVShared.Instance.LoggedInUserData.IsAdmin && indexPath.Row == 0 )
				{
					cell = TableView.DequeueReusableCell(new NSString("UserSelectionCell"), indexPath);
					((JVUserSelectionCell)cell).Setup(false);
					((JVUserSelectionCell)cell).Delegate = this;
				}
				else
				{
					cell = TableView.DequeueReusableCell(new NSString("DeviceClassSelectionCell"), indexPath);
					((JVDeviceClassSelectionCell)cell).Setup(false);
					((JVDeviceClassSelectionCell)cell).Delegate = this;
				}
			}
			else
			{
				cell = TableView.DequeueReusableCell(new NSString("UsageGraphCell"), indexPath);
				((JVUsageGraphCell)cell).Setup(new float[] {12f, 14f, 16f, 18f, 10f, 12f, 14f}, new string[] {"Mon", "Tues", "Wed", "Thur", "Fri", "Sat", "Sun"});
			}

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			cell.SeparatorInset = new UIEdgeInsets(0,0,0,0);

			return cell;
		}
		#endregion TableView Logic

		#region IJVUserSelectionCellDelegate implementation
		public void UserSelectionDidChange(int index)
		{
			SelectedUserIndex = index;
			RefreshView();
		}
		#endregion

		#region IJVDeviceClassSelectionCellDelegate implementation
		public void DeviceClassSelectionDidChange (int index)
		{
			SelectedDeviceIndex = index;
			RefreshView();
		}
		#endregion
	}
}
