// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace KobiiOS
{
	[Register ("JVViewUsageTableViewController")]
	partial class JVViewUsageTableViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView historyGraphImageView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (historyGraphImageView != null) {
				historyGraphImageView.Dispose ();
				historyGraphImageView = null;
			}
		}
	}
}
