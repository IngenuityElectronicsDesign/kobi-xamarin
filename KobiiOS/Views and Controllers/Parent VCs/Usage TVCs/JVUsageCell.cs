using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVUsageCell : UITableViewCell
	{
		public UILabel DeviceNameLabel { get; private set; }
		public UILabel HoursUsedLabel { get; private set; }
		public UILabel HoursRemainLabel { get; private set; }
		public UILabel EmergencyHoursUpperLabel { get; private set; }
		public UILabel EmergencyHoursLowerLabel { get; private set; }
		public UIImageView IconImageView { get; private set; }
		public XIProgressRingView UsageRingView { get; private set; }

		public JVUsageCell(IntPtr handle) : base (handle)
		{
			DeviceNameLabel = new UILabel(new RectangleF(87, 12, 146, 21));
			DeviceNameLabel.Font = UIFont.BoldSystemFontOfSize(17f);
			DeviceNameLabel.TextColor = UIColor.Black;
			DeviceNameLabel.TextAlignment = UITextAlignment.Center;
			AddSubview(DeviceNameLabel);

			HoursUsedLabel = new UILabel(new RectangleF(87, 33, 146, 21));
			HoursUsedLabel.Font = UIFont.SystemFontOfSize(16f);
			HoursUsedLabel.TextColor = UIColor.Black;
			HoursUsedLabel.TextAlignment = UITextAlignment.Center;
			AddSubview(HoursUsedLabel);

			HoursRemainLabel = new UILabel(new RectangleF(87, 54, 146, 21));
			HoursRemainLabel.Font = UIFont.SystemFontOfSize(16f);
			HoursRemainLabel.TextColor = JVShared.Instance.GetUserTintColor();
			HoursRemainLabel.TextAlignment = UITextAlignment.Center;
			AddSubview(HoursRemainLabel);

			EmergencyHoursUpperLabel = new UILabel(new RectangleF(87, 33, 146, 21));
			EmergencyHoursUpperLabel.Font = UIFont.SystemFontOfSize(16f);
			EmergencyHoursUpperLabel.TextColor = JVShared.Instance.SystemErrorTintColor;
			EmergencyHoursUpperLabel.TextAlignment = UITextAlignment.Center;
			EmergencyHoursUpperLabel.Hidden = true;
			AddSubview(EmergencyHoursUpperLabel);

			EmergencyHoursLowerLabel = new UILabel(new RectangleF(87, 54, 146, 21));
			EmergencyHoursLowerLabel.Font = UIFont.SystemFontOfSize(16f);
			EmergencyHoursLowerLabel.TextColor = JVShared.Instance.SystemErrorTintColor;
			EmergencyHoursLowerLabel.TextAlignment = UITextAlignment.Center;
			EmergencyHoursLowerLabel.Hidden = true;
			EmergencyHoursLowerLabel.Text = "hours used";
			AddSubview(EmergencyHoursLowerLabel);

			IconImageView = new UIImageView(new RectangleF(15, 12, 64, 64));
			AddSubview(IconImageView);

			UsageRingView = new XIProgressRingView(new RectangleF(241, 12, 64, 64));
			AddSubview(UsageRingView);
		}

		public void Setup(int index, int userIndex, int deviceIndex)
		{
			JVShared jvs = JVShared.Instance;
			JVDeviceClassData deviceData = jvs.DeviceClassDataContainer.GetData(deviceIndex);
			JVUsageData usageData = jvs.UsageDataContainer.GetData(userIndex, deviceIndex);
			JVQuotaData quotaData = jvs.QuotaDataContainer.GetData(userIndex, deviceIndex);

			float hoursUsed;
			float hoursRemain;
			float emergencyHours;

			if( JVShared.Instance.KobiGlobalData.QuotaTimePeriodIsDaily )
			{
				hoursUsed = JVTimeHandler.GetHoursAsDecimal(usageData.DailyUsage[0]);
				hoursRemain = JVTimeHandler.GetHoursAsDecimal(quotaData.DailyQuotas[0])-JVTimeHandler.GetHoursAsDecimal(usageData.DailyUsage[0]);
				emergencyHours = JVTimeHandler.GetHoursAsDecimal(usageData.EmergencyUsage);
			}
			else
			{
				hoursUsed = JVTimeHandler.GetHoursAsDecimal(usageData.WeeklyUsage);
				hoursRemain = JVTimeHandler.GetHoursAsDecimal(quotaData.WeeklyQuota)-JVTimeHandler.GetHoursAsDecimal(usageData.WeeklyUsage);
				emergencyHours = JVTimeHandler.GetHoursAsDecimal(usageData.EmergencyUsage);
			}

			UIColor tintColor = jvs.GetUserTintColor();

			IconImageView.Image = deviceData.Icon.ImageWithTintColor(JVShared.Instance.GetUserTintColor());
			DeviceNameLabel.Text = deviceData.Name;
			HoursUsedLabel.Text = string.Format("{0} hours used",  hoursUsed);
			HoursRemainLabel.Text = string.Format("{0} hours remain", hoursRemain);
			EmergencyHoursUpperLabel.Text = string.Format("{0} emergency", emergencyHours);

			if( emergencyHours == 0 )	// Normal hours in use
			{
				UsageRingView.Create((float)(hoursUsed/(hoursUsed+hoursRemain)), tintColor, UIColor.FromWhiteAlpha(0f, 0.5f), 12f, 0.5f, 0.15f*(index+1) );
			}
			else 						// Emergency hours in use
			{
				UsageRingView.Create(1f, UIColor.DarkGray, JVShared.Instance.SystemErrorTintColor, 12f, 1f, 0.15f*(index+1) );
				HoursUsedLabel.Hidden = true;
				HoursRemainLabel.Hidden = true;
				EmergencyHoursUpperLabel.Hidden = false;
				EmergencyHoursLowerLabel.Hidden = false;
			}
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}

