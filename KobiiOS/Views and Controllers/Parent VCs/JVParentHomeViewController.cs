using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVParentHomeViewController : JVHomeViewController
	{
		public JVParentHomeViewController(IntPtr handle) : base(handle, 6)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			Titles = new string[] { "Use Kobi", "Manage", "Quotas", "Schedule", "Usage", "Hours" };
			ImageNames = new string[] { "Kobi.png", "Cog.png", "Quota.png", "Schedule.png", "Usage.png", "Hours.png" };

			base.ViewDidLoad();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear(animated);

			NavigationController.NavigationBar.BarTintColor = JVShared.Instance.SystemTintColor;
		}
		#endregion View Controller Logic

		#region IJVIconViewDelegate implementation
		public override void DidPressIconWithIndex(int index)
		{
			string pushString = "";

			switch( index )
			{
			case 0:
				{
					pushString = "UseKobiTVC";
					break;
				}
			case 1:
				{
					pushString = "ManageTVC";
					break;
				}
			case 2:
				{
					pushString = "AssignQuotaTVC";
					break;
				}
			case 3:
				{
					pushString = "AssignBlockoutTVC";
					break;
				}
			case 4:
				{
					pushString = "ViewChildrensUsageTVC";
					break;
				}
			case 5:
				{
					pushString = "ModifyHoursTVC";
					break;
				}
			}

			UIViewController toPush = (UIViewController)JVShared.Instance.Storyboard.InstantiateViewController(pushString);
			NavigationController.PushViewController(toPush, true);
		}
		#endregion IJVIconViewDelegate implementation

		#region Layout Logic
		public override void LayoutIcons()
		{
			int x, y, vertSpacing;

			vertSpacing = ( JVShared.Instance.IsWidescreen ? 31 : 9 );

			for( int i = 0; i < MenuItemCount; i++ )
			{
				JVMenuIconView menuIcon = MenuIcons[i];

				x = 320;
				y = JVShared.TitleBarHeight + vertSpacing + ((JVShared.IconHeight+vertSpacing)*(i/2));

				menuIcon.Frame = new RectangleF(x, y, menuIcon.Frame.Width, menuIcon.Frame.Height);

				x = JVShared.IconEdgePadding + ((JVShared.IconWidth+JVShared.IconHorizSpacing)*(i%2));

				UIView.AnimateNotify( 0.5f, 0.1f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
					{
						menuIcon.Frame = new RectangleF(x, y, menuIcon.Frame.Width, menuIcon.Frame.Height);
					}, (bool finished) => // Completion
					{ });
			}
		}
		#endregion Layout Logic
	}
}
