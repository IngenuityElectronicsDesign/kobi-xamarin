using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVManageUsersTableViewController : JVDataSyncTableViewController
	{
		private JVUserDataContainer UserDataContainerInstance;

		private UIActionSheet DeleteConfirmationActionSheet;
		private int DeletionIndex;
		private bool DeletionIsAdmin;

		public JVManageUsersTableViewController(IntPtr handle) : base(handle)
		{
			UserDataContainerInstance = new JVUserDataContainer(JVShared.Instance.UserDataContainer);
		}

		#region View Controller Logic
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVUserCell), new NSString("UserCell") );
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			RefreshView();
		}

		public override void RefreshView()
		{
			CompareUsers();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 0 )
			{
				return Math.Min(UserDataContainerInstance.AdminUserDataList.Count+1, JVUserDataContainer.MaxNumAdminUsers);
			}
			else
			{
				return Math.Min(UserDataContainerInstance.ChildUserDataList.Count+1, JVUserDataContainer.MaxNumChildUsers);
			}
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = TableView.DequeueReusableCell(new NSString("UserCell"), indexPath);

			JVUserData data;
			if( indexPath.Section == 0 )
			{
				data = ( indexPath.Row == UserDataContainerInstance.AdminUserDataList.Count ? null : UserDataContainerInstance.AdminUserDataList[indexPath.Row] );
			}
			else
			{
				data = ( indexPath.Row == UserDataContainerInstance.ChildUserDataList.Count ? null : UserDataContainerInstance.ChildUserDataList[indexPath.Row] );
			}

			((JVUserCell)cell).Setup( data, indexPath.Row, false );

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			JVEditUserTableViewController toPush = (JVEditUserTableViewController)JVShared.Instance.Storyboard.InstantiateViewController("editUserTVC");

			if( indexPath.Section == 0 )
			{
				if( indexPath.Row == UserDataContainerInstance.AdminUserDataList.Count )
				{
					toPush.Setup(UserDataContainerInstance, true);
				}
				else
				{
					toPush.Setup(UserDataContainerInstance, true, indexPath.Row);
				}
			}
			else
			{
				if( indexPath.Row == UserDataContainerInstance.ChildUserDataList.Count )
				{
					toPush.Setup(UserDataContainerInstance, false);
				}
				else
				{
					toPush.Setup(UserDataContainerInstance, false, indexPath.Row);
				}
			}

			NavigationController.PushViewController(toPush, true);
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return 88;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Manage Parents (Administrators)";
			}
			else
			{
				return "Manage Children";
			}
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				return "Tap to edit a Parent.";
			}
			else
			{
				return "Tap to edit a Child. Swipe to remove.";
			}
		}

		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 0 )
			{
				if( indexPath.Row != UserDataContainerInstance.AdminUserDataList.Count )
				{
					if( JVShared.Instance.LoggedInUserData.Name != UserDataContainerInstance.AdminUserDataList[indexPath.Row].Name )
					{
						return true;
					}
				}
			}
			else
			{
				if( indexPath.Row != UserDataContainerInstance.ChildUserDataList.Count )
				{
					return true;
				}
			}
			return false;
		}

		public override string TitleForDeleteConfirmation (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 0 )
			{
				return string.Format("Remove {0}", UserDataContainerInstance.AdminUserDataList[indexPath.Row].Name);
			}
			else
			{
				return string.Format("Remove {0}", UserDataContainerInstance.ChildUserDataList[indexPath.Row].Name);
			}
		}

		public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			if( editingStyle == UITableViewCellEditingStyle.Delete )
			{
				DeletionIsAdmin = (indexPath.Section == 0 ? true : false);
				DeletionIndex = indexPath.Row;
				string name =  (indexPath.Section == 0 ? UserDataContainerInstance.AdminUserDataList[DeletionIndex].Name : UserDataContainerInstance.ChildUserDataList[DeletionIndex].Name);
				DeleteConfirmationActionSheet = new UIActionSheet(string.Format("Are you sure you want to remove {0}?\nThis will permanatly delete this account, its data, and its history. This cannot be undone.", name), 
					null, "Cancel", string.Format("Remove {0}", name));
				DeleteConfirmationActionSheet.Clicked += (sender, e) => // ActionSheet Button Pressed
				{
					if( e.ButtonIndex == 0 ) // Delete
					{
						UserDataContainerInstance.Remove(DeletionIsAdmin, DeletionIndex);
					}

					if( DeletionIsAdmin )
					{
						TableView.ReloadSections(new NSIndexSet(0), UITableViewRowAnimation.Fade);
					}
					else
					{
						TableView.ReloadSections(new NSIndexSet(1), UITableViewRowAnimation.Fade);
					}
					RefreshView();
				};
				DeleteConfirmationActionSheet.ShowInView(View);
			}
		}
		#endregion Table View Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			JVShared.Instance.UserDataContainer.MigrateData(UserDataContainerInstance);
			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			UIAlertView alertView = new UIAlertView("Discard Unsaved Changes?", "Are you sure you want to discard all of your unsaved changes?", null, "Discard Changes and Exit", "Keep Changes", "Save Changes and Exit");
			alertView.Clicked += (sender, e) => // Alert View Pressed
			{
				if( e.ButtonIndex == 0 ) // Discard
				{
					NavigationController.PopViewControllerAnimated(true);
				}
				if( e.ButtonIndex == 2 ) // Save
				{
					SaveButtonPressed();
					NavigationController.PopViewControllerAnimated(true);
				}
			};
			alertView.Show();
		}

		private void CompareUsers()
		{
			bool isModified = false;

			HashSet<int> conflicts = new HashSet<int>();

			if( !JVShared.Instance.UserDataContainer.IsEqualToOther(UserDataContainerInstance, conflicts) )
			{
				isModified = true;
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs
	}
}

