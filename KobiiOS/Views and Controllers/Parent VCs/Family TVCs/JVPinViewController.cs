using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class JVPinViewController : UIViewController
	{
		const int TallPinScreenHeight = 300;
		const int ShortPinScreenHeight = 255;

		private JVPinView PinView;

		public JVUserDataContainer UserDataContainerInstance { get; set; }
		public JVPinCodeData PinCodeData { get; set; }

		public JVPinViewController(IntPtr handle) : base (handle)
		{
		}

		#region View Controller Logic
		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			PinView = new JVPinView(new RectangleF(0, View.Frame.Height-(JVShared.Instance.IsWidescreen ? TallPinScreenHeight : ShortPinScreenHeight), View.Frame.Width, (JVShared.Instance.IsWidescreen ? TallPinScreenHeight : ShortPinScreenHeight)), true);
			PinView.ResetPinDigits(true);
			PinView.TintColor = JVShared.Instance.SystemTintColor;
			View.AddSubview(PinView);

			PinView.OnEnteredFirstNewPin += EnteredFirstNewPin;
			PinView.OnEnteredSecondNewPin += EnteredSecondNewPin;
		}

		public override void ViewDidDisappear (bool animated)
		{
			PinView.OnEnteredFirstNewPin -= EnteredFirstNewPin;
			PinView.OnEnteredSecondNewPin -= EnteredSecondNewPin;

			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		void EnteredCorrectPin()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		void EnteredIncorrectPin()
		{
			Title = "Enter New PIN";
			PinView.ResetPinDigits(true);
		}

		#region PinView Events
		void EnteredFirstNewPin(JVPinCodeData pinCodeData)
		{
			if( UserDataContainerInstance.GetIsPinUsed(pinCodeData) )
			{
				Title = "PIN Already Used";
				PinView.EnteredIncorrectPin();
				NSTimer.CreateScheduledTimer(0.75f, EnteredIncorrectPin);
				return;
			}

			Title = "Confirm New PIN";
			PinView.ResetPinDigits(false);
		}

		void EnteredSecondNewPin(bool success)
		{
			if( success )
			{
				Title = "New PIN Saved";
				PinCodeData.SetPin(PinView.OverridingPinCode.GetPin());
				NSTimer.CreateScheduledTimer(0.75f, EnteredCorrectPin);
			}
			else
			{
				Title = "PINs Didn't Match";
				PinView.EnteredIncorrectPin();
				NSTimer.CreateScheduledTimer(0.75f, EnteredIncorrectPin);
			}
		}
		#endregion PinView Events
	}
}
