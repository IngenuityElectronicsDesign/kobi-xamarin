using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace KobiiOS
{
	public partial class JVEditUserTableViewController : JVDataSyncTableViewController, IJVUserCellDelegate
	{
		private JVUserData UserDataInstance;

		public JVUserDataContainer UserDataContainerInstance { get; private set; }
		public bool IsAdmin { get; private set; }
		public int UserIndex { get; private set; }

		private NSIndexPath SelectedIndexPath;
		private string OriginalName;

		private UIAlertView InputErrorAlertView;
		private UIActionSheet ImagePickerActionSheet;
		private UIImagePickerController ImagePickerController;

		public JVEditUserTableViewController(IntPtr handle) : base(handle) { }

		public void Setup( JVUserDataContainer userDataContainer, bool isAdmin, int userIndex )
		{
			IsAdmin = isAdmin;
			UserIndex = userIndex;

			UserDataContainerInstance = userDataContainer;
			UserDataInstance = new JVUserData(UserDataContainerInstance.GetData(UserIndex, IsAdmin));

			OriginalName = UserDataInstance.Name;

			Title = string.Format( "Edit {0}", UserDataInstance.Name );
			SelectedIndexPath = NSIndexPath.FromRowSection((int)UserDataInstance.TintColorIndex-1, 2);
		}

		public void Setup( JVUserDataContainer userDataContainer, bool isAdmin )
		{
			IsAdmin = isAdmin;
			UserIndex = -1;

			UserDataContainerInstance = userDataContainer;
			UserDataInstance = new JVUserData();
			UserDataInstance.IsAdmin = IsAdmin;

			Title = ( IsAdmin ? "Add New Parent" : "Add New Child");
			SelectedIndexPath = NSIndexPath.FromRowSection(-1, 2);
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(JVUserCell), new NSString("UserCell") );
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			RefreshView();
		}

		public override void RefreshView()
		{
			if( UserIndex != -1 )
			{
				Title = string.Format( "Edit {0}", UserDataInstance.Name );
			}

			CompareUsers();

			base.RefreshView();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			if( IsAdmin )
			{
				return 2;
			}
			return 3;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( section == 2 )
			{
				return 7;
			}
			return 1;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = base.GetCell(TableView, indexPath);;

			if( indexPath.Section == 0 )
			{
				cell = TableView.DequeueReusableCell(new NSString("UserCell"), indexPath);
				((JVUserCell)cell).Setup( UserDataInstance, indexPath.Row, true );
				((JVUserCell)cell).Delegate = this;
			}
			else if( indexPath.Section == 1 )
			{
				cell.TextLabel.Text = ( UserIndex != -1 ? "Change PIN" : "Assign PIN" );
				cell.DetailTextLabel.Text = ( UserDataInstance.PinCode != null ? UserDataInstance.PinCode.GetPin() : "- - - -" );
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			else if( indexPath.Section == 2 )
			{
				if( indexPath.Row == SelectedIndexPath.Row )
				{
					cell.Accessory = UITableViewCellAccessory.Checkmark;
				}
				else
				{
					cell.Accessory = UITableViewCellAccessory.None;
				}

				if( UserDataContainerInstance.GetIsTintColorUsed((JVUserData.UserTintColor)indexPath.Row+1) )
				{
					cell.DetailTextLabel.Text = "(Already Taken)";

					if( UserIndex != -1 && UserDataContainerInstance.GetData(UserIndex, IsAdmin).TintColorIndex == (JVUserData.UserTintColor)indexPath.Row+1 )
					{
						cell.DetailTextLabel.Text = string.Format("({0}'s Colour)", UserDataInstance.Name);
					}
				}
				else
				{
					cell.DetailTextLabel.Text = "";
				}
			}

			cell.SeparatorInset = new UIEdgeInsets(0, 0, 0, 0);

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			TableView.DeselectRow(indexPath, true);

			if( indexPath.Section == 1 )
			{
				JVPinViewController toPush = (JVPinViewController)JVShared.Instance.Storyboard.InstantiateViewController("AssignPinVC");
				toPush.UserDataContainerInstance = UserDataContainerInstance;
				toPush.PinCodeData = UserDataInstance.PinCode;
				NavigationController.PushViewController(toPush, true);
			}
			else if( indexPath.Section == 2 )
			{
				if( !UserDataContainerInstance.GetIsTintColorUsed((JVUserData.UserTintColor)indexPath.Row+1) || (UserIndex != -1 && UserDataContainerInstance.GetData(UserIndex, IsAdmin).TintColorIndex == (JVUserData.UserTintColor)indexPath.Row+1) )
				{
					SelectedIndexPath = indexPath;
					UserDataInstance.TintColorIndex = (JVUserData.UserTintColor)indexPath.Row+1;
					RefreshView();
				}
			}
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 0 )
			{
				return 88;
			}
			return 44;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if( section == 2 )
			{
				return string.Format("Pick {0}'s Favourite Colour", ( !string.IsNullOrEmpty(UserDataInstance.Name) ? UserDataInstance.Name : "Child" ));
			}
			return "";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			if( section == 0 )
			{
				if( UserIndex != -1 )
				{
					return "Tap to change the name or the portrait.";
				}
				else
				{
					return "Tap to add a name and a portrait.";
				}
			}
			return "";
		}
		#endregion Table View Logic

		#region Parent Funcs
		protected override void SaveButtonPressed()
		{
			if( UserIndex != -1 && UserDataContainerInstance.GetData(UserIndex, IsAdmin) != null )
			{
				UserDataContainerInstance.SetUserData(UserIndex, IsAdmin, new JVUserData(UserDataInstance));
			}
			else
			{
				UserDataContainerInstance.Add(UserDataInstance);
				NavigationController.PopViewControllerAnimated(true);
			}

			RefreshView();
		}

		protected override void DiscardButtonPressed()
		{
			NavigationController.PopViewControllerAnimated(true);
		}

		private void CompareUsers()
		{
			bool isModified = false;

			HashSet<int> conflicts = new HashSet<int>();

			if( UserIndex != -1 )
			{
				if( !UserDataContainerInstance.GetData(UserIndex, IsAdmin).IsEqualToOther(UserDataInstance, conflicts) )
				{
					isModified = true;
				}
			}
			else
			{
				if( !string.IsNullOrEmpty(UserDataInstance.Name) /*&& UserDataInstance.Portrait != null*/ && UserDataInstance.PinCode.GetPin() != "- - - -" && ( IsAdmin ? UserDataInstance.TintColorIndex == JVUserData.UserTintColor.System : UserDataInstance.TintColorIndex != JVUserData.UserTintColor.System ) )
				{
					isModified = true;
				}
			}

			DataModified(isModified);
		}
		#endregion Parent Funcs

		#region IJVUserCellDelegate implementation
		public void UserChangedName(int index, string name)
		{
			if( name == OriginalName )
			{
				UserDataInstance.Name = name;
			}
			else
			{
				string temp = UserDataInstance.Name;
				UserDataInstance.Name = name;

				if( UserDataContainerInstance.GetIsNameUsed(UserDataInstance) )
				{
					UserDataInstance.Name = temp;

					InputErrorAlertView = new UIAlertView(string.Format("The Name \"{0}\" is Taken", name), "Please enter a unique name.", null, "Got it!", null);
					InputErrorAlertView.Show();
				}
			}

			RefreshView();
		}

		public void UserPortraitPressed(int index)
		{
			if( UIImagePickerController.IsSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) )
			{
				ImagePickerActionSheet = new UIActionSheet("", null, "Cancel", null, "Take New Photo", "Choose From Library");
				ImagePickerActionSheet.Clicked += (sender, e) => // ActionSheet Button Pressed
				{
					if( e.ButtonIndex == 0 )
					{
						DisplayImagePickerWithSourceType(UIImagePickerControllerSourceType.Camera);
					}
					else
					{
						DisplayImagePickerWithSourceType(UIImagePickerControllerSourceType.PhotoLibrary);
					}
				};
				ImagePickerActionSheet.ShowInView(View);
			}
			else
			{
				DisplayImagePickerWithSourceType(UIImagePickerControllerSourceType.PhotoLibrary);
			}

		}
		#endregion IJVUserCellDelegate implementation

		void DisplayImagePickerWithSourceType(UIImagePickerControllerSourceType type)
		{
			ImagePickerController = new UIImagePickerController();
			ImagePickerController.SourceType = type;
			ImagePickerController.FinishedPickingMedia += (sender, e) => // Image Picker Finished
			{
				UIImage originalImage = (UIImage)e.Info.ObjectForKey(UIImagePickerController.OriginalImage);

				float width = originalImage.Size.Width*0.5f;
				float height = originalImage.Size.Height*0.5f;

				SizeF size = new SizeF(width, height);
				UIGraphics.BeginImageContext(size);

				originalImage.Draw(new RectangleF(0, 0, size.Width, size.Height));
				UIImage image = UIGraphics.GetImageFromCurrentImageContext();
				UIGraphics.EndImageContext();

				if( width != height )
				{
					float length = Math.Min(width, height);
					float widthOffset = (width - length) / 2;
					float heightOffset = (height - length) / 2;

					UIGraphics.BeginImageContextWithOptions(new SizeF(length, length), false, 0f);
					image.Draw(new PointF(-widthOffset, -heightOffset), CGBlendMode.Copy, 1f);
					image = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext();
				}

				UserDataInstance.Portrait = image;

				RefreshView();
				DismissViewController(true, null);
			};
			ImagePickerController.Canceled += (sender, e) => // Image Picker Cancelled
			{
				DismissViewController(true, null);
			};
			PresentViewController(ImagePickerController, true, null);
		}
	}
}
