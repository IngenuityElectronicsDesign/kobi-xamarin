using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public interface IJVUserCellDelegate
	{
		void UserChangedName(int index, string name);
		void UserPortraitPressed(int index);
	} 

	public class JVUserCell : UITableViewCell
	{
		public IJVUserCellDelegate Delegate { get; set; }

		public bool IsEditing { get; private set; }
		public int Index { get; private set; }

		private string UserName;

		public UILabel UserNameLabel { get; private set; }
		public UITextField UserNameTextField { get; private set; }
		public UIImageView PortraitImageView { get; private set; }
		public UIButton PortraitButton { get; private set; }
		public UIImageView AccessoryImageView { get; private set; }

		public JVUserCell(IntPtr handle) : base(handle)
		{
			UserNameLabel = new UILabel(new RectangleF(15, 22, 200, 44));
			UserNameLabel.TextColor = UIColor.Black;
			UserNameLabel.Font = UIFont.BoldSystemFontOfSize(18f);
			UserNameLabel.Hidden = true;
			AddSubview(UserNameLabel);

			UserNameTextField = new UITextField(new RectangleF(15, 22, 220, 44));
			UserNameTextField.TextColor = UIColor.Black;
			UserNameTextField.Font = UIFont.BoldSystemFontOfSize(18f);
			UserNameTextField.Placeholder = "Tap to Enter Name";
			UserNameTextField.ReturnKeyType = UIReturnKeyType.Done;
			UserNameTextField.EditingChanged += (sender, args) => // TextField Value Changed
			{
				string text = ((UITextField)sender).Text;
				((UITextField)sender).Text = text.Substring(0, Math.Min(text.Length, JVUserData.MaxNameLength));
			};
			UserNameTextField.ShouldReturn += (textField) => // TextField Should Return
			{
				textField.ResignFirstResponder();
				return false;
			};
			UserNameTextField.EditingDidBegin += (sender, args) => // TextField Did Begin Editing
			{

			};
			UserNameTextField.EditingDidEnd += (sender, args) => // TextField Did End Editing
			{
				string text = ((UITextField)sender).Text;
				if( string.IsNullOrEmpty(text) )
				{
					((UITextField)sender).Text = UserName;
				}
				else
				{
					UserName = ((UITextField)sender).Text;
				}

				if( Delegate != null )
				{
					Delegate.UserChangedName(Index, ((UITextField)sender).Text);
				}
			};
			AddSubview(UserNameTextField);

			PortraitImageView = new UIImageView(new RectangleF(223, 12, 64, 64));
			PortraitImageView.Hidden = true;
			AddSubview(PortraitImageView);

			PortraitButton = new UIButton(new RectangleF(241, 12, 64, 64));
			PortraitButton.Hidden = true;
			PortraitButton.TouchUpInside += (sender, e) => // Button Pressed
			{
				if( Delegate != null )
				{
					Delegate.UserPortraitPressed(Index);
				}
			};
			AddSubview(PortraitButton);

			AccessoryImageView = new UIImageView(new RectangleF(0, 0, 9, 16));
			AccessoryImageView.Image = UIImage.FromFile("ButtonBarArrowRight.png");
		}

		public void Setup(JVUserData user, int index, bool isEditing)
		{
			Index = index;
			SetEditingMode(isEditing);

			if( user != null && !string.IsNullOrEmpty(user.Name) )
			{
				UIImage portrait = user.Portrait != null ? user.Portrait : UIImage.FromFile("Admin.png");

				PortraitImageView.Image = portrait;
				PortraitImageView.ImageCroppedToCircle( 2.5f, JVShared.Instance.GetUserTintColor(user.TintColorIndex) );
				PortraitButton.SetImage(portrait, UIControlState.Normal);
				PortraitButton.SetImage(portrait, UIControlState.Selected);
				PortraitButton.ImageView.ImageCroppedToCircle( 2.5f, JVShared.Instance.GetUserTintColor(user.TintColorIndex) );
				UserNameLabel.Text = user.Name;
				UserNameTextField.Text = user.Name;
				UserName = user.Name;

				if( user == JVShared.Instance.LoggedInUserData )
				{
					UserNameLabel.Text = UserNameLabel.Text + " (Me)";
				}

				AccessoryImageView.Image = AccessoryImageView.Image.ImageWithTintColor(JVShared.Instance.GetUserTintColor(user.TintColorIndex));
			}
			else
			{
				PortraitImageView.Image = null;
				PortraitImageView.ImageCroppedToCircle( 2.5f, UIColor.Clear );
				PortraitButton.SetImage(UIImage.FromFile("Admin.png"), UIControlState.Normal);
				PortraitButton.SetImage(UIImage.FromFile("Admin.png"), UIControlState.Selected);
				PortraitButton.ImageView.ImageCroppedToCircle( 2.5f, UIColor.Black );
				UserNameLabel.Text = "Add New...";
				UserNameTextField.Text = "";
				UserName = "";

				AccessoryImageView.Image = AccessoryImageView.Image.ImageWithTintColor(UIColor.Black);
			}
		}

		public void SetEditingMode(bool isEditing)
		{
			IsEditing = isEditing;

			PortraitImageView.Hidden = IsEditing;
			PortraitButton.Hidden = !IsEditing;
			UserNameLabel.Hidden = IsEditing;
			UserNameTextField.Hidden = !IsEditing;

			AccessoryView = (IsEditing ? null : AccessoryImageView);
		}
	}
}

