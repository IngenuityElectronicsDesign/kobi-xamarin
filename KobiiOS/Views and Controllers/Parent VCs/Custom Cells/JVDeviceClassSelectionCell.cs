using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace KobiiOS
{
	public interface IJVDeviceClassSelectionCellDelegate
	{
		void DeviceClassSelectionDidChange(int index);
	}

	public partial class JVDeviceClassSelectionCell : UITableViewCell, IXIMultiSelectButtonDelegate
	{
		private bool First = false;
		public IJVDeviceClassSelectionCellDelegate Delegate { get; set; }
		private XIMultiSelectButton MultiSelectButton;
		private UISegmentedControl SegmentedControl;
		private List<string> Names = new List<string>();

		public JVDeviceClassSelectionCell(IntPtr handle) : base(handle)
		{
			First = true;
			foreach( JVDeviceClassData data in JVShared.Instance.DeviceClassDataContainer.DeviceClassDataList )
			{
				Names.Add(data.Name);
			}
		}

		public void Setup(bool isMultiSelect)
		{
			if( !First )
			{
				return;
			}

			First = false;

			if( isMultiSelect )	// Create Multi Select Button
			{
				MultiSelectButton = new XIMultiSelectButton( new RectangleF(20f, 8f, 280f, 29f), JVShared.Instance.SelectedDeviceClassesIndexSet, Names.ToArray() );
				MultiSelectButton.TintColor = JVShared.Instance.GetUserTintColor();
				MultiSelectButton.Delegate = this;
				AddSubview(MultiSelectButton);
			}
			else				// Create Segmented Control
			{
				SegmentedControl = new UISegmentedControl(new RectangleF(20f, 8f, 280f, 29f));
				SegmentedControl.TintColor = JVShared.Instance.GetUserTintColor();
				for( int i = 0; i < Names.Count; i++ )
				{
					SegmentedControl.InsertSegment(Names[i], i, false);
				}
				SegmentedControl.ValueChanged += (sender, e) => // Segmented Value Changed
				{
					if( Delegate != null )
					{
						Delegate.DeviceClassSelectionDidChange(SegmentedControl.SelectedSegment);
					}
				};
				SegmentedControl.SelectedSegment = 0;
				AddSubview(SegmentedControl);
			}
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}

		#region IMultiSelectButtonDelegate implementation
		public void IndexSetDidChange(XIMultiSelectButton control, HashSet<int> indexSet)
		{
			JVShared.Instance.SelectedDeviceClassesIndexSet.Clear();

			foreach( int index in indexSet )
			{
				JVShared.Instance.SelectedDeviceClassesIndexSet.Add(index);
			}

			if (Delegate != null)
			{
				Delegate.DeviceClassSelectionDidChange(0);
			}
		}
		#endregion IMultiSelectButtonDelegate implementation
	}
}
