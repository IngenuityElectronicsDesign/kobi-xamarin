using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public interface IJVTimeInputCell
	{
		void TimeValueChanged(JVTimeInputCell cell, byte quota);
	}

	[Register ("JVTimeInputCell")]
	public partial class JVTimeInputCell : UITableViewCell
	{
		public const int AbsoluteMaxHours = 60;

		public IJVTimeInputCell Delegate { get; set; }

		public int MaxHours { get; private set; }
		public int Index { get; private set; }

		public UILabel TitleLabel { get; private set; }
		public UILabel SeperatorLabel { get; private set; }
		public UITextField HourTextField { get; private set; }
		public UITextField MinuteTextField { get; private set; }

		private int HourValue;
		private int MinuteValue;

		public JVTimeInputCell(IntPtr handle) : base (handle)
		{
			TitleLabel = new UILabel(new RectangleF(15, 0, 210, 44));
			TitleLabel.Font = UIFont.SystemFontOfSize(17f);
			TitleLabel.TextAlignment = UITextAlignment.Left;
			TitleLabel.TextColor = UIColor.Black;
			AddSubview(TitleLabel);

			SeperatorLabel = new UILabel(new RectangleF(265, 10, 10, 21));
			SeperatorLabel.Font = UIFont.SystemFontOfSize(17f);
			SeperatorLabel.TextAlignment = UITextAlignment.Center;
			SeperatorLabel.TextColor = UIColor.Black;
			SeperatorLabel.Text = ":";
			AddSubview(SeperatorLabel);

			HourTextField = new UITextField(new RectangleF(235, 0, 30, 44));
			HourTextField.Font = UIFont.SystemFontOfSize(17f);
			HourTextField.TextAlignment = UITextAlignment.Right;
			HourTextField.TextColor = JVShared.Instance.GetUserTintColor();
			HourTextField.Placeholder = "hh";
			HourTextField.Text = "00";
			HourTextField.KeyboardType = UIKeyboardType.NumberPad;
			HourTextField.ClearsOnBeginEditing = true;
			HourTextField.EditingChanged += (sender, args) => // TextField Value Changed
			{
				string text = ((UITextField)sender).Text;
				((UITextField)sender).Text = text.Substring(0, Math.Min(text.Length, 2));
			};
			HourTextField.ShouldReturn += (textField) => // TextField Should Return
			{
				textField.ResignFirstResponder();
				return true;
			};
			HourTextField.EditingDidBegin += (sender, args) => // TextField Did Begin Editing
			{

			};
			HourTextField.EditingDidEnd += (sender, args) => // TextField Did End Editing
			{
				string text = ((UITextField)sender).Text;
				if( !string.IsNullOrEmpty(text) )
				{
					HourValue = Convert.ToInt32(text);
				}
				else
				{
					HourValue = 0;
				}

				if( HourValue > MaxHours )
				{
					HourValue = MaxHours;
				}
				if( HourValue == MaxHours && MinuteValue != 0 )
				{
					HourValue = MaxHours-1;
				}

				((UITextField)sender).Text = HourValue.ToString("00");

				if( Delegate != null )
				{
					Delegate.TimeValueChanged(this, JVTimeHandler.GetQuotaChar(HourValue, MinuteValue));
				}
			};
			AddSubview(HourTextField);

			MinuteTextField = new UITextField(new RectangleF(275, 0, 30, 44));
			MinuteTextField.Font = UIFont.SystemFontOfSize(17f);
			MinuteTextField.TextAlignment = UITextAlignment.Left;
			MinuteTextField.TextColor = JVShared.Instance.GetUserTintColor();
			MinuteTextField.Placeholder = "mm";
			MinuteTextField.Text = "00";
			MinuteTextField.KeyboardType = UIKeyboardType.NumberPad;
			MinuteTextField.ClearsOnBeginEditing = true;
			MinuteTextField.EditingChanged += (sender, args) => // TextField Value Changed
			{
				string text = ((UITextField)sender).Text;
				((UITextField)sender).Text = text.Substring(0, Math.Min(text.Length, 2));
			};
			MinuteTextField.ShouldReturn += (textField) => // TextField Should Return
			{
				textField.ResignFirstResponder();
				return true;
			};
			MinuteTextField.EditingDidBegin += (sender, args) => // TextField Did Begin Editing
			{

			};
			MinuteTextField.EditingDidEnd += (sender, args) => // TextField Did End Editing
			{
				string text = ((UITextField)sender).Text;
				if( !string.IsNullOrEmpty(text) )
				{
					MinuteValue = (int)JVTimeHandler.RoundNumber(Convert.ToInt32(text), JVTimeHandler.MinuteRounding);
				}
				else
				{
					MinuteValue = 0;
				}

				if( MinuteValue >= 60 )
				{
					MinuteValue = 0;
				}

				((UITextField)sender).Text = MinuteValue.ToString("00");

				if( Convert.ToInt32(HourTextField.Text) == MaxHours && MinuteValue != 0 )
				{
					HourValue = MaxHours-1;
					HourTextField.Text = HourValue.ToString("00");
				}

				if( Delegate != null )
				{
					Delegate.TimeValueChanged(this, JVTimeHandler.GetQuotaChar(HourValue, MinuteValue));
				}
			};
			AddSubview(MinuteTextField);

			// Add XIKeyboard Toolbar
			HourTextField.InputAccessoryView = new XIKeyboardToolbar(new RectangleF(0f, 0f, Frame.Width, 44.0f), this, MinuteTextField, null);
			MinuteTextField.InputAccessoryView = new XIKeyboardToolbar(new RectangleF(0f, 0f, Frame.Width, 44.0f), this, null, HourTextField);
		}

		public void SetupCell(int index, string title, byte quota)
		{
			SetupCell(index, title, quota, AbsoluteMaxHours);
		}

		public void SetupCell(int index, string title, byte quota, int maxHours)
		{
			Index = index;
			TitleLabel.Text = title;
			MaxHours = Math.Min(maxHours, AbsoluteMaxHours);

			JVTimeHandler.GetHoursAndMins(ref HourValue, ref MinuteValue, quota);

			HourTextField.Text = HourValue.ToString("00");
			MinuteTextField.Text = MinuteValue.ToString("00");
		}

		public void SetupCell(int index, string title, byte quota, int maxHours, bool readOnly)
		{
			SetupCell(index, title, quota, maxHours);

			if( readOnly )
			{
				HourTextField.Enabled = false;
				MinuteTextField.Enabled = false;
			}
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}
