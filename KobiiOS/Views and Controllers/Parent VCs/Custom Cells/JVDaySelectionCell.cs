﻿using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace KobiiOS
{
	public interface IJVDaySelectionCellDelegate
	{
		void DaySelectionDidChange(int index);
	}

	public class JVDaySelectionCell : UITableViewCell, IXIMultiSelectButtonDelegate
	{
		private bool First = false;
		public IJVDaySelectionCellDelegate Delegate { get; set; }
		private XIMultiSelectButton MultiSelectButton;
		private UISegmentedControl SegmentedControl;
		private List<string> Names = new List<string>();

		public JVDaySelectionCell(IntPtr handle) : base(handle)
		{
			First = true;
			for( int i = 0; i < JVShared.NumOfDays; i++ )
			{
				Names.Add(JVShared.Instance.GetDayName(i)[0].ToString());
			}
		}

		public void Setup(bool isMultiSelect)
		{
			if( !First )
			{
				return;
			}

			First = false;

			if( isMultiSelect )	// Create Multi Select Button
			{
				MultiSelectButton = new XIMultiSelectButton( new RectangleF(20f, 8f, 280f, 29f), JVShared.Instance.SelectedDaysIndexSet, Names.ToArray() );
				MultiSelectButton.TintColor = JVShared.Instance.GetUserTintColor();
				MultiSelectButton.Delegate = this;
				AddSubview(MultiSelectButton);
			}
			else				// Create Segmented Control
			{
				SegmentedControl = new UISegmentedControl(new RectangleF(20f, 8f, 280f, 29f));
				SegmentedControl.TintColor = JVShared.Instance.GetUserTintColor();
				for( int i = 0; i < Names.Count; i++ )
				{
					SegmentedControl.InsertSegment(Names[i], i, false);
				}
				SegmentedControl.ValueChanged += (sender, e) => // Segmented Value Changed
				{
					if( Delegate != null )
					{
						Delegate.DaySelectionDidChange(SegmentedControl.SelectedSegment);
					}
				};
				SegmentedControl.SelectedSegment = 0;
				AddSubview(SegmentedControl);
			}
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}

		#region IMultiSelectButtonDelegate implementation
		public void IndexSetDidChange(XIMultiSelectButton control, HashSet<int> indexSet)
		{
			JVShared.Instance.SelectedDaysIndexSet.Clear();

			foreach( int index in indexSet )
			{
				JVShared.Instance.SelectedDaysIndexSet.Add(index);
			}

			if (Delegate != null)
			{
				Delegate.DaySelectionDidChange(0);
			}
		}
		#endregion IMultiSelectButtonDelegate implementation
	}
}

