using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace KobiiOS
{
	public interface IJVUserSelectionCellDelegate
    {
		void UserSelectionDidChange(int index);
    }

	public partial class JVUserSelectionCell : UITableViewCell, IXIMultiSelectButtonDelegate
	{
		private bool First = false;
		public IJVUserSelectionCellDelegate Delegate { get; set; }
		private XIMultiSelectButton MultiSelectButton;
		private UISegmentedControl SegmentedControl;
		private List<string> Names = new List<string>();

		public JVUserSelectionCell(IntPtr handle) : base(handle)
		{
			First = true;
			foreach( JVUserData data in JVShared.Instance.UserDataContainer.ChildUserDataList )
			{
				Names.Add(data.Name);
			}
		}

		public void Setup(bool isMultiSelect)
		{
			if( !First )
			{
				return;
			}

			First = false;

			if( isMultiSelect )	// Create Multi Select Button
			{
				MultiSelectButton = new XIMultiSelectButton( new RectangleF(20f, 8f, 280f, 29f), JVShared.Instance.SelectedUsersIndexSet, Names.ToArray() );
				MultiSelectButton.TintColor = JVShared.Instance.GetUserTintColor();
				MultiSelectButton.Delegate = this;
				AddSubview(MultiSelectButton);
			}
			else				// Create Segmented Control
			{
				SegmentedControl = new UISegmentedControl(new RectangleF(20f, 8f, 280f, 29f));
				SegmentedControl.TintColor = JVShared.Instance.GetUserTintColor();
				for( int i = 0; i < Names.Count; i++ )
				{
					SegmentedControl.InsertSegment(Names[i], i, false);
				}
				SegmentedControl.ValueChanged += (sender, e) => // Segmented Value Changed
				{
					if( Delegate != null )
					{
						Delegate.UserSelectionDidChange(SegmentedControl.SelectedSegment);
					}
				};
				SegmentedControl.SelectedSegment = 0;
				AddSubview(SegmentedControl);
			}
		}

        public override void SetSelected(bool selected, bool animated)
        {
            base.SetSelected(false, false);
        }

		#region IMultiSelectButtonDelegate implementation
		public void IndexSetDidChange(XIMultiSelectButton control, HashSet<int> indexSet)
        {
			JVShared.Instance.SelectedUsersIndexSet.Clear();

			foreach( int index in indexSet )
			{
				JVShared.Instance.SelectedUsersIndexSet.Add(index);
			}

			if (Delegate != null)
			{
				Delegate.UserSelectionDidChange(0);
			}
        }
		#endregion IMultiSelectButtonDelegate implementation
    }
}
