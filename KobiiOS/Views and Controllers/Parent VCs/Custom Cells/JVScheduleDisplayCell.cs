﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class JVScheduleDisplayCell : UITableViewCell
	{
		public const int TimeIndicatorsNum = 9;

		public List<UIView> BlockViewsList { get; private set; }
		private UIView Background;
		private UIView DividerLine;
		private UILabel[] TimeIndicators;

		public JVScheduleDisplayCell(IntPtr handle) : base(handle)
		{
			BlockViewsList = new List<UIView>();

			for( int i = 0; i < JVScheduleData.NumOfBlocks; i++ )
			{
				UIView blockView = new UIView(new RectangleF(16, 16, 0, 36));
				blockView.BackgroundColor = JVShared.Instance.GetUserTintColor();
				BlockViewsList.Add(blockView);

				AddSubview(blockView);
			}

			Background = new UIView(new RectangleF(16, 16, 288, 36));
			Background.BackgroundColor = new UIColor(239/255f, 239/255f, 244/255f, 1f);

			AddSubview(Background);

			DividerLine = new UIView(new RectangleF(16, 56, 288, 1));
			DividerLine.BackgroundColor = UIColor.Black;

			AddSubview(DividerLine);

			TimeIndicators = new UILabel[TimeIndicatorsNum];

			int spacing = (int)(Background.Frame.Width / (TimeIndicatorsNum-1));

			for( int i = 0; i < TimeIndicatorsNum; i++ )
			{
				UILabel indicatorLabel = new UILabel(new RectangleF(6+spacing*i, 60, 20, 16));
				indicatorLabel.TextColor = UIColor.Black;
				indicatorLabel.TextAlignment = UITextAlignment.Center;
				indicatorLabel.Font = UIFont.SystemFontOfSize(14f);
				indicatorLabel.Text = string.Format("{0}", i < TimeIndicatorsNum-1 ? i*3 : 0);
				TimeIndicators[i] = indicatorLabel;

				AddSubview(indicatorLabel);
			}
		}

		public void SetBlockView(byte[] startVals, byte[] endVals, int highlightedIndex)
		{
			if( highlightedIndex >= JVScheduleData.NumOfBlocks || startVals.Length != JVScheduleData.NumOfBlocks || startVals.Length != endVals.Length )
			{
				Console.WriteLine("Warning ScheduleDisplayCell: Incorrect values passed.");
				return;
			}
			for( int i = 0; i < JVScheduleData.NumOfBlocks; i++ )
			{
				if( startVals[i] >= endVals[i] )
				{
					BlockViewsList[i].Frame = new RectangleF(16, 16, 0, 36);
				}
				else
				{
					BlockViewsList[i].Frame = new RectangleF(16+startVals[i]*3, 16, (endVals[i]-startVals[i])*3, 36);
					BlockViewsList[i].BackgroundColor = ( (i == highlightedIndex) ? JVShared.Instance.GetUserTintColor() : JVShared.Instance.GetUserTintColor().ColorWithAlpha(0.2f) );
					BringSubviewToFront(BlockViewsList[i]);
				}
			}
		}
	}
}

