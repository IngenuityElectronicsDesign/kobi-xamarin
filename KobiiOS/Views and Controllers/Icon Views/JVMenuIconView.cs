using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace KobiiOS
{
	public class JVMenuIconView : JVIconView
	{
		public JVMenuIconView(RectangleF frame, string title, UIImage image, int index) : base(frame, index)
		{
			Button.UserInteractionEnabled = true;
			Button.SetImage(image.ImageWithTintColor(JVShared.Instance.GetUserTintColor(JVShared.Instance.LoggedInUserData.TintColorIndex)), UIControlState.Normal);
			Button.ImageView.ImageCroppedToCircle( 2.5f,JVShared.Instance.GetUserTintColor(JVShared.Instance.LoggedInUserData.TintColorIndex) );

			Label.Text = title;
		}
	}
}
