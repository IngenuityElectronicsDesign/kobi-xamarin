using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace KobiiOS
{
    public interface IJVIconViewDelegate
    {
        void DidPressIconWithIndex(int index);
    }

	public class JVIconView : UIView
	{
		public const int ButtonDiameter = 96;
		public const int LabelHeight = 21;
		public const int Spacing = 11;

		public int Index { get; private set; }

		public UIButton Button { get; set; }
		public UILabel Label { get; set; }

        public IJVIconViewDelegate Delegate { get; set; }

		public JVIconView( RectangleF frame, int index )
		{
			base.Frame = frame;

			Index = index;

			Button = new UIButton(UIButtonType.Custom);
			Button.Frame = new RectangleF(0, 0, ButtonDiameter, ButtonDiameter);
			Button.TouchUpInside += (object sender, EventArgs e) =>
			{
                if (Delegate != null)
                    Delegate.DidPressIconWithIndex(Index);
			};
			AddSubview(Button);

			Label = new UILabel( new RectangleF(0, ButtonDiameter+Spacing, ButtonDiameter, LabelHeight) );
			Label.Font = UIFont.SystemFontOfSize(17f);
			Label.TextAlignment = UITextAlignment.Center;
			Label.TextColor = UIColor.Black;
			AddSubview(Label);
		}

	}
}

