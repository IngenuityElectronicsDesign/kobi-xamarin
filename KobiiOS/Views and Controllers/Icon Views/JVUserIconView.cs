using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;

namespace KobiiOS
{
	public class JVUserIconView : JVIconView
	{
		public JVUserData UserData { get; private set; }

		public JVUserIconView(RectangleF frame, JVUserData userData, int index) : base(frame, index)
		{
			SetUserData(userData);
		}

		public JVUserIconView(RectangleF frame, bool blankAsAdmin, int index) : base(frame, index)
		{
			SetBlank(blankAsAdmin);
		}

		public JVUserIconView(RectangleF frame, int index) : base(frame, index)
		{
			SetBlank();
		}

		public void SetUserData( JVUserData userData )
		{
			UserData = userData;

			Button.UserInteractionEnabled = true;
			Button.SetImage(UserData.Portrait, UIControlState.Normal);
			Button.ImageView.ImageCroppedToCircle( 2.5f, JVShared.Instance.GetUserTintColor(UserData.TintColorIndex) );

			Label.Text = UserData.Name;
		}

		public void SetBlank( bool asAdmin )
		{
			UserData = null;

			Button.UserInteractionEnabled = true;
			Button.SetImage(UIImage.FromFile( asAdmin ? "Admin.png" : "Children.png" ).ImageWithTintColor(JVShared.Instance.SystemTintColor), UIControlState.Normal);
			Button.ImageView.ImageCroppedToCircle( 2.5f, JVShared.Instance.SystemTintColor );

			Label.Text = asAdmin ? "Add Admin" : "Add Child";
		}

		public void SetBlank()
		{
			UserData = null;

			Button.UserInteractionEnabled = false;
			Button.SetImage(null, UIControlState.Normal);

			Label.Text = "";
		}
	}
}

