using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}

		[Export ("applicationDidFinishLaunching:")]
		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			JVShared.Instance.SetupTableDataStructures();

			if( launchOptions != null )
			{
				if( launchOptions.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey) )
				{
					var notification = launchOptions[UIApplication.LaunchOptionsLocalNotificationKey] as UILocalNotification;
					if( notification != null )
					{
						// Show the alert
						UIAlertView alert = new UIAlertView(notification.AlertAction, notification.AlertBody, null, "Refresh View", null);
						alert.Clicked += (sender, e) =>
						{
							Console.WriteLine("Got it!");
						};
						alert.Show();

						UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
					}
				}
			}
			return true;
		}

		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
		}
		// This method is called when the application is about to terminate. Save data, if needed.
		public override void WillTerminate (UIApplication application)
		{
		}

		public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
		{
			// Show the alert
			UIAlertView alert = new UIAlertView(notification.AlertAction, notification.AlertBody, null, "Refresh View", null);
			alert.Clicked += (sender, e) =>
			{
				Console.WriteLine("Got it!");
			};
			alert.Show();

			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}
	}
}

