using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;

namespace KobiiOS
{
	static class UIImageViewExtentions
	{
		public static UIImageView ImageCroppedToCircle(this UIImageView imageView, float borderThickness, UIColor borderColor)
		{
			CALayer imageLayer = imageView.Layer;
			imageLayer.CornerRadius = (Math.Min(imageView.Frame.Width, imageView.Frame.Height)/2);
			imageLayer.BorderWidth = borderThickness;
			imageLayer.BorderColor = borderColor.CGColor;
			imageLayer.MasksToBounds = true;

			return imageView;
		}
	}
}
