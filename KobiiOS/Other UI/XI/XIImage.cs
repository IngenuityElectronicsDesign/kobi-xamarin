using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace KobiiOS
{
	static class UIImageExtentions
	{
		public static UIImage ImageWithTintColor(this UIImage image, UIColor tintColor)
		{
			RectangleF rect = new RectangleF(0, 0, image.Size.Width, image.Size.Height);
		
			UIGraphics.BeginImageContextWithOptions(image.Size, false, image.CurrentScale);

			image.Draw(rect);

			CGContext context = UIGraphics.GetCurrentContext();
			context.SetBlendMode(CGBlendMode.SourceIn);
			context.SetFillColorWithColor(tintColor.CGColor);
			context.FillRect(rect);

			image = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return image;
		}
	}
}


