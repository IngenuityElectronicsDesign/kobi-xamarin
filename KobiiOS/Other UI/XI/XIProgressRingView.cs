using System;
using System.Drawing;
using MonoTouch.CoreAnimation;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class XIProgressRingView : UIView
	{
		public XIProgressRingView(RectangleF frame) : base(frame)
		{

		}

		public void Create(float progress, UIColor inactiveColor, UIColor activeColor, float lineWidth, float duration, float delay)
		{
			RectangleF bounds = new RectangleF(new PointF(Bounds.X+lineWidth*0.25f, Bounds.Y+lineWidth*0.25f), new SizeF(Bounds.Width-lineWidth, Bounds.Height-lineWidth));

			CAShapeLayer inactiveRing = new CAShapeLayer();
			CAShapeLayer activeRing = new CAShapeLayer();

			// Make a circle path
			inactiveRing.Path = UIBezierPath.FromRoundedRect(bounds, Math.Min(bounds.Width, bounds.Height)).CGPath;
			activeRing.Path = UIBezierPath.FromRoundedRect(bounds, Math.Min(bounds.Width, bounds.Height)).CGPath;

			// Set inital (pre-animation) values
			inactiveRing.StrokeStart = 0f;
			inactiveRing.StrokeEnd = 1f;
			activeRing.StrokeStart = 0f;
			activeRing.StrokeEnd = 0f;

			// Center the ring in the view
			inactiveRing.Position = bounds.Location;
			activeRing.Position = bounds.Location;

			// Configure the appearance of the ring
			inactiveRing.FillColor = UIColor.Clear.CGColor;		// Empty inside (to create a ring as opposed to a circle)
			inactiveRing.StrokeColor = inactiveColor.CGColor;
			inactiveRing.LineWidth = lineWidth;
			activeRing.FillColor = UIColor.Clear.CGColor;		// Empty inside (to create a ring as opposed to a circle)
			activeRing.StrokeColor = activeColor.CGColor;
			activeRing.LineWidth = lineWidth;

			if( Layer.Sublayers != null )
			{
				foreach( CALayer layer in Layer.Sublayers )
				{
					layer.RemoveFromSuperLayer();
				}
			}

			// Add to parent view layer
			Layer.AddSublayer(inactiveRing);
			Layer.AddSublayer(activeRing);

			// Configure animation
			CABasicAnimation drawAnimation = CABasicAnimation.FromKeyPath("strokeEnd");
			drawAnimation.Duration = duration*progress;
			drawAnimation.BeginTime = CAAnimation.CurrentMediaTime()+delay; // Delay
			drawAnimation.RepeatCount = 1;

			// Animate from nothing to progress
			drawAnimation.From = NSNumber.FromFloat(0f);
			drawAnimation.To = NSNumber.FromFloat(progress);

			// Ensure animation holds its end position
			drawAnimation.RemovedOnCompletion = false;
			drawAnimation.FillMode = CAFillMode.Forwards;

			// Timing
			drawAnimation.TimingFunction = CAMediaTimingFunction.FromName(CAMediaTimingFunction.EaseInEaseOut);

			// Add the animation to the ring
			activeRing.AddAnimation(drawAnimation, "drawCircleAnimation");
		}
	}
}

