using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public interface IXIPercentagePagingViewDelegate
	{
		void OnChangingToIndex(XIPercentagePagingView scrollView, bool entering, float percent);
		void OnChangedToIndex(XIPercentagePagingView scrollView, int pageIndex);
	} 

	public class XIPercentagePagingView : UIScrollView
	{
		public int PageIndex { get; private set; }
		public float Percentage { get; private set; }

		public List<UIView> PageViews;

		public new IXIPercentagePagingViewDelegate Delegate { get; set; }

		public XIPercentagePagingView(RectangleF frame, int pagesNum) : base(frame)
		{
			PageViews = new List<UIView>();

			for( int i = 0; i < pagesNum; i++ )
			{
				UIView view = new UIView(new RectangleF(new PointF(Frame.Width*i, 0), Frame.Size));
				PageViews.Add(view);
				AddSubview(view);
			}

			ContentSize = new SizeF( Bounds.Width*pagesNum, Bounds.Height-20 );
			PagingEnabled = true;
			Bounces = true;
			ShowsHorizontalScrollIndicator = false;
			Scrolled += (sender, e) => // Scroll View Did Scroll
			{
				// Get Page
				float pageWidth = Bounds.Width;
				int page = (int)Math.Floor((ContentOffset.X - pageWidth / 2 ) / pageWidth) + 1;
				if( PageIndex != page )
				{
					PageIndex = page;
					if( Delegate != null )
					{
						Delegate.OnChangedToIndex(this, PageIndex);
					}
				}

				// Get Percent
				Percentage = ((((int)((ContentOffset.X) % (int)pageWidth) / pageWidth) - 0.5f) * 2f);
				bool entering;

				if( Percentage > 0 )
				{
					entering = true;
				}
				else
				{
					entering = false;
				}

				if( Delegate != null )
				{
					Delegate.OnChangingToIndex(this, entering, Math.Abs(Percentage));
				}
			};
		}

		public void SetBackgroundColor(UIColor backgroundColor, int pageIndex)
		{
			PageViews[pageIndex].BackgroundColor = backgroundColor;
		}
	}
}

