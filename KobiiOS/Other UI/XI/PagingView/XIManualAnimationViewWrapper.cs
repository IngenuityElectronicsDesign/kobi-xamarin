using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class XIManualAnimationViewWrapper
	{
		public UIView View { get; private set; }
		public int FocusPageIndex { get; set; }
		public PointF EnterPoint { get; private set; }
		public PointF RestingPoint { get; private set; }
		public PointF ExitPoint { get; private set; }

		public XIManualAnimationViewWrapper( UIView view, PointF enterPoint, PointF restingPoint, PointF exitPoint ) : this(view, enterPoint, restingPoint, exitPoint, 0) { }

		public XIManualAnimationViewWrapper( UIView view, PointF enterPoint, PointF restingPoint, PointF exitPoint, int focusPageIndex )
		{
			View = view;
			FocusPageIndex = focusPageIndex;
			EnterPoint = enterPoint;
			RestingPoint = restingPoint;
			ExitPoint = exitPoint;

			View.Alpha = 0f;
		}

		public void SetAnimationPercentage( int pageIndex, float percentage, bool isEntering )
		{
			if( pageIndex == FocusPageIndex )
			{
				PointF towardsPoint = isEntering ? EnterPoint : ExitPoint;
				PointF resultantPoint = new PointF( (towardsPoint.X + (RestingPoint.X - towardsPoint.X) * percentage), (towardsPoint.Y + (RestingPoint.Y - towardsPoint.Y) * percentage) );
				View.Frame = new RectangleF( resultantPoint, View.Frame.Size );
				View.Alpha = percentage;
			}
			else if( View.Alpha != 0 )
			{
				View.Alpha = 0f;
			}
		}
	}
}
 
