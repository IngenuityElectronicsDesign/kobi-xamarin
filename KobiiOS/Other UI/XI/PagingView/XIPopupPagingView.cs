using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public class XIPopupPagingView : UIView, IXIPercentagePagingViewDelegate
	{
		const int PageControlHeight = 20;

		int PagesNum;

		XIPercentagePagingView PagingView;
		UIPageControl PageControl;

		RectangleF internalFrame;

		List<XIManualAnimationViewWrapper> AnimViewsList;

		public delegate void OnDismissedDelegate();
		public event OnDismissedDelegate OnDismissed;

		public NSTimer repeatingTimer;

		public XIPopupPagingView(SizeF size, int pagesNum, UIColor backgroundColor) : base()
		{
			PagesNum = pagesNum;

			Frame = new RectangleF(0, UIApplication.SharedApplication.StatusBarFrame.Height, UIScreen.MainScreen.Bounds.Width, UIScreen.MainScreen.Bounds.Height-UIApplication.SharedApplication.StatusBarFrame.Height);

			// Make Frame for Subviews
			if( size.Width > Bounds.Width )
			{
				size.Width = Bounds.Width;
			}
			if( size.Height > Bounds.Height)
			{
				size.Height = Bounds.Height;
			}

			internalFrame = new RectangleF( Bounds.Width*0.5f - size.Width*0.5f, Bounds.Height*0.5f - size.Height*0.5f, size.Width, size.Height );

			// Background
			BackgroundColor = UIColor.Clear;

			UIView.Animate(0.25f, () => // Animations
				{
					BackgroundColor = ((backgroundColor != null) ? backgroundColor : UIColor.Clear);
				});

			// Setup View Contents
			PagingView = new XIPercentagePagingView(new RectangleF(internalFrame.X, Frame.Height, internalFrame.Width, internalFrame.Height-PageControlHeight), pagesNum);
			PagingView.BackgroundColor = UIColor.Clear;
			PagingView.Delegate = this;
			AddSubview(PagingView);

			PageControl = new UIPageControl(new RectangleF(internalFrame.X, Frame.Height*1.95f, internalFrame.Width, PageControlHeight));
			PageControl.Pages = pagesNum <= 1 ? 0 : pagesNum;
			PageControl.CurrentPage = 0;
			PageControl.CurrentPageIndicatorTintColor = JVShared.Instance.GetUserTintColor();
			PageControl.PageIndicatorTintColor = UIColor.FromWhiteAlpha(1, 0.5f);
			PageControl.UserInteractionEnabled = false;
			PageControl.BackgroundColor = UIColor.Clear;
			AddSubview(PageControl);

			UIView.AnimateNotify(0.5f, 0f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animations
				{
					PagingView.Frame = new RectangleF(internalFrame.X, internalFrame.Y, internalFrame.Width, internalFrame.Height-PageControlHeight);
					PageControl.Frame = new RectangleF(internalFrame.X, internalFrame.Y+internalFrame.Height-PageControlHeight, internalFrame.Width, PageControlHeight);
				}, (bool finished) => // Completion
				{

				});
			UIView.Animate(0.25f, () => // Animations
				{
					PagingView.BackgroundColor = UIColor.FromWhiteAlpha(0, 0.75f);
					PageControl.BackgroundColor = UIColor.FromWhiteAlpha(0, 0.75f);
				});

			// Anim Views
			AnimViewsList = new List<XIManualAnimationViewWrapper>();
		}

		public void SetupPage(int pageIndex, string title, string description, XIManualAnimationViewWrapper[] animViewsArray)
		{
			float spacer = 5f;
			float runningYOffset = spacer;

			if( !string.IsNullOrEmpty(title) )
			{
				// Setup Title
				UIFont titleFont = UIFont.BoldSystemFontOfSize(28f);

				//NSDictionary titleAttributes = NSDictionary.FromObjectAndKey(titleFont, new NSString("NSFontAttributeName"));
				//float panelTitleHeight = NSExtendedStringDrawing.GetBoundingRect(nsTitle, new SizeF(PagingView.Frame.Width - 2f*10f, 999f), NSStringDrawingOptions.UsesLineFragmentOrigin, titleAttributes, null).Size.Height;
				//panelTitleHeight = Math.Ceiling(panelTitleHeight);

				float titleHeight = new NSString(title).StringSize(titleFont, new SizeF(PagingView.Frame.Width - 2f*10f, 999f), UILineBreakMode.WordWrap).Height;

				UILabel titleLabel = new UILabel(new RectangleF(spacer, PagingView.Frame.Y+runningYOffset, PagingView.Frame.Width - 2f*spacer, titleHeight));
				titleLabel.Lines = 0;
				titleLabel.Text = title;
				titleLabel.Font = titleFont;
				titleLabel.TextColor = UIColor.White;
				titleLabel.TextAlignment = UITextAlignment.Center;

				XIManualAnimationViewWrapper titleWrapper = new XIManualAnimationViewWrapper( titleLabel, new PointF(PagingView.Frame.Width, PagingView.Frame.Y+runningYOffset), new PointF(PagingView.Frame.X+spacer, PagingView.Frame.Y+runningYOffset), new PointF(PagingView.Frame.X-titleLabel.Frame.Width, PagingView.Frame.Y+runningYOffset), pageIndex );
				AnimViewsList.Add( titleWrapper );
				AddSubview( titleWrapper.View );

				runningYOffset += titleHeight + spacer;

				// Setup Line Divider
				UIView lineDivider = new UIView(new RectangleF(spacer, PagingView.Frame.Y+runningYOffset, PagingView.Frame.Width - 2f*spacer, 1f));
				lineDivider.BackgroundColor = JVShared.Instance.GetUserTintColor().ColorWithAlpha(0.75f);

				XIManualAnimationViewWrapper lineWrapper = new XIManualAnimationViewWrapper( lineDivider, new PointF(PagingView.Frame.Width, PagingView.Frame.Y+runningYOffset), new PointF(PagingView.Frame.X+spacer, PagingView.Frame.Y+runningYOffset), new PointF(PagingView.Frame.X-titleLabel.Frame.Width, PagingView.Frame.Y+runningYOffset), pageIndex );
				AnimViewsList.Add( lineWrapper );
				AddSubview( lineWrapper.View );

				runningYOffset += 1 + spacer;
			}

			if( !string.IsNullOrEmpty(description) )
			{
				// Setup Description
				UIFont descriptionFont = UIFont.SystemFontOfSize(18f);

				float descriptionHeight = new NSString(title).StringSize(descriptionFont, new SizeF(PagingView.Frame.Width - 2f*spacer, 999f), UILineBreakMode.WordWrap).Height;

				UILabel descriptionLabel = new UILabel(new RectangleF(spacer*2, PagingView.Frame.Y+runningYOffset, PagingView.Frame.Width - 2f*spacer*2, descriptionHeight));
				descriptionLabel.Lines = 0;
				descriptionLabel.Text = description;
				descriptionLabel.Font = descriptionFont;
				descriptionLabel.TextColor = UIColor.White;
				descriptionLabel.TextAlignment = UITextAlignment.Center;

				XIManualAnimationViewWrapper descriptionWrapper = new XIManualAnimationViewWrapper( descriptionLabel, new PointF(PagingView.Frame.Width, PagingView.Frame.Y+runningYOffset), new PointF(PagingView.Frame.X+spacer*2f, PagingView.Frame.Y+runningYOffset), new PointF(PagingView.Frame.X-descriptionLabel.Frame.Width, PagingView.Frame.Y+runningYOffset), pageIndex );
				AnimViewsList.Add( descriptionWrapper );
				AddSubview( descriptionWrapper.View );

				runningYOffset += descriptionHeight + spacer;
			}

			// Setup Anim Views
			if( animViewsArray != null )
			{
				foreach( XIManualAnimationViewWrapper animView in animViewsArray )
				{
					animView.FocusPageIndex = pageIndex;
					AnimViewsList.Add(animView);
					AddSubview(animView.View);
				}
			}
				
			// Animate first page into view
			if( pageIndex == 0 )
			{

				NSTimer.CreateScheduledTimer(0.4f, () => // Timer Fired
					{
						float percent = 0;
						repeatingTimer = NSTimer.CreateRepeatingScheduledTimer(0.01f, () => // Repeating Timer Fired
							{
								OnChangingToIndex(PagingView, true, percent += 0.1f);

								if( percent > 1 )
								{
									repeatingTimer.Invalidate();
									repeatingTimer = null;
								}
							});
					});
			}
		}

		public void SetupPage(int pageIndex, string title, string description, XIManualAnimationViewWrapper[] animViewsArray, string closeButtonTitle)
		{
			SetupPage(pageIndex, title, description, animViewsArray);

			float closeButtonYPos = Frame.Height-PageControl.Frame.Height-60f;

			UIButton closeButton = new UIButton(new RectangleF(5f, closeButtonYPos, PagingView.Frame.Width - 2f*5f,  40f));

			closeButton.TitleLabel.Font = UIFont.BoldSystemFontOfSize(28f);

			closeButton.SetTitle(closeButtonTitle, UIControlState.Normal);
			closeButton.SetTitle(closeButtonTitle, UIControlState.Highlighted);
			closeButton.SetTitleColor(JVShared.Instance.GetUserTintColor(), UIControlState.Normal);
			closeButton.SetTitleColor(JVShared.Instance.GetUserTintColor().ColorWithAlpha(0.5f), UIControlState.Highlighted);

			XIManualAnimationViewWrapper buttonWrapper = new XIManualAnimationViewWrapper( closeButton, new PointF(PagingView.Frame.Width, closeButtonYPos), new PointF(PagingView.Frame.X+5f, closeButtonYPos), new PointF(PagingView.Frame.X-closeButton.Frame.Width, closeButtonYPos), pageIndex );
			AnimViewsList.Add( buttonWrapper );
			AddSubview( buttonWrapper.View );

			closeButton.TouchUpInside += (sender, e) => // Close Button Pressed
			{
				DismissPagingView();
			};
		}

		public void DismissPagingView()
		{
			NSTimer.CreateScheduledTimer(0.1f, () => // Timer Fired
				{
					float percent = 1;
					repeatingTimer = NSTimer.CreateRepeatingScheduledTimer(0.01f, () => // Repeating Timer Fired
						{
							OnChangingToIndex(PagingView, false, percent -= 0.1f);

							if( percent < 0 )
							{
								repeatingTimer.Invalidate();
								repeatingTimer = null;

								UIView.AnimateNotify(0.5f, 0f, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animations
									{
										PagingView.Frame = new RectangleF(internalFrame.X, Frame.Height, internalFrame.Width, internalFrame.Height-PageControlHeight);
										PageControl.Frame = new RectangleF(internalFrame.X, Frame.Height*1.95f, internalFrame.Width, PageControlHeight);
									}, (bool finished) => { });
								UIView.Animate(0.25f, () => // Animations
									{
										PagingView.BackgroundColor = UIColor.Clear;
										PageControl.BackgroundColor = UIColor.Clear;
										BackgroundColor =  UIColor.Clear;
									}, () => // Completion
									{
										OnDismissed();
									});
							}
						});
				});
		}

		#region IXIPercentagePagingViewDelegate implementation
		public void OnChangingToIndex(XIPercentagePagingView scrollView, bool entering, float percent)
		{
			Console.WriteLine("{0}, {1}%", entering, percent);

			foreach( XIManualAnimationViewWrapper animView in AnimViewsList )
			{
				animView.SetAnimationPercentage(PageControl.CurrentPage, percent, entering);
			}
		}

		public void OnChangedToIndex (XIPercentagePagingView scrollView, int pageIndex)
		{
			PageControl.CurrentPage = pageIndex;
		}
		#endregion IXIPercentagePagingViewDelegate implementation
	}
}

