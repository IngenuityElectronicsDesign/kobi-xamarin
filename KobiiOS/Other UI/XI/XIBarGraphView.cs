using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
	public partial class XIBarGraphView : UIView
	{
		public int LeftPadding { get; private set; }
		public int BottomPadding { get; private set; }
		public int AxisPadding { get; private set; }
		public int HorizSpacing { get; private set; }
		public int LabelHeight { get; private set; }

		private List<RectangleF> AxisFrames;
		private List<UIView> BarGraphViews;
		private List<UILabel> XAxisLabels;
		private List<UILabel> YAxisLabels;
		private List<UIView> YAxisSublines;

		public XIBarGraphView(RectangleF frame) : base(frame)
		{
			BarGraphViews = new List<UIView>();
			XAxisLabels = new List<UILabel>();
			YAxisLabels = new List<UILabel>();
			YAxisSublines = new List<UIView>();

			LeftPadding = 32;
			BottomPadding = 48;
			AxisPadding = 5;
			HorizSpacing = 10;
			LabelHeight = 20;

			AxisFrames = new List<RectangleF>( DrawAxes(new RectangleF(Math.Max(LeftPadding-AxisPadding, 0), (Frame.Height - BottomPadding), (Frame.Width - LeftPadding - AxisPadding + HorizSpacing), 1f), new RectangleF(Math.Max(LeftPadding-AxisPadding, 0), 0, 1f, (Frame.Height - BottomPadding))) );
		}

		public void DrawBarsInGraph(float[] dataArray, string[] labelTextArray)
		{
			ClearBarsInGraph();

			if( dataArray == null )
			{
				return;
			}

			float barWidth = ((Bounds.Width - LeftPadding) - (HorizSpacing * (dataArray.Length + 1)))/dataArray.Length;
			float maxBarHeight = GetLargestFloat(dataArray) + 2.5f;	// Add 2.5
			maxBarHeight = 5 * (int)Math.Round(maxBarHeight / 5f);	// Round to nearest 5

			for( int i = 0; i < dataArray.Length; i++ )
			{
				float barX = (LeftPadding + ((i + 1) * HorizSpacing) + (i * barWidth));
				float barOriginY = (Frame.Height - BottomPadding);
				float barHeight = (dataArray[i] / maxBarHeight);

				BarGraphViews.Add(DrawBarInGraph(new RectangleF(barX, barOriginY, barWidth, 0), new RectangleF(barX, barOriginY*(1-barHeight), barWidth, barOriginY*barHeight), UIColor.White, JVShared.Instance.GetUserTintColor(), 0.5f,  0.1f*(i+1)));
				XAxisLabels.Add(DrawXAxisLabel(new RectangleF(barX, barOriginY+AxisPadding, barWidth, BottomPadding), labelTextArray[i]));
			}

			YAxisLabels = new List<UILabel>(DrawYAxisLabels(AxisFrames[0], AxisFrames[1], maxBarHeight));
			YAxisSublines = new List<UIView>(DrawYAxisSublines(AxisFrames[0], AxisFrames[1], maxBarHeight));
		}

		UIView DrawBarInGraph(RectangleF initialFrame, RectangleF destinationFrame, UIColor initialColor, UIColor destinationColor, float duration, float delay)
		{
			// Setup coloured bar view
			UIView barGraph = new UIView(initialFrame);
			barGraph.BackgroundColor = initialColor.ColorWithAlpha(0.9f);
			AddSubview(barGraph);

			// Add "candy cane" lines above the coloured bar view
			UIView lines = new UIView(barGraph.Bounds);
			lines.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("Lines.png"));
			lines.Alpha = 0.5f;
			barGraph.AddSubview(lines);

			// Handle anim
			UIView.AnimateNotify(duration, delay, 0.7f, 10f, UIViewAnimationOptions.AllowUserInteraction, () => // Animation
				{
					barGraph.Frame = destinationFrame;
					lines.Frame = new RectangleF(0, 0, destinationFrame.Width, destinationFrame.Height);
					if( destinationColor != null )
					{
						barGraph.BackgroundColor = destinationColor.ColorWithAlpha(0.9f);
					}
				}, (bool finished) => // Completion
				{

				});

			return barGraph;
		}

		RectangleF[] DrawAxes(RectangleF xFrame, RectangleF yFrame)
		{
			UIView xAxis = new UIView(xFrame);
			xAxis.BackgroundColor = UIColor.Black;
			AddSubview(xAxis);

			UIView yAxis = new UIView(yFrame);
			yAxis.BackgroundColor = UIColor.Black;
			AddSubview(yAxis);

			return new RectangleF[] { xAxis.Frame, yAxis.Frame };
		}

		UILabel DrawXAxisLabel(RectangleF frame, string text)
		{
			UILabel label = new UILabel(frame);
			label.Font = UIFont.SystemFontOfSize(14f);
			label.TextAlignment = UITextAlignment.Right;
			label.TextColor = JVShared.Instance.GetUserTintColor();
			label.Text = text;

			label.Transform = CGAffineTransform.MakeRotation((float)-Math.PI / 2f);
			label.Frame = frame;

			AddSubview(label);

			return label;
		}

		List<UILabel> DrawYAxisLabels(RectangleF xFrame, RectangleF yFrame, float maxVal)
		{
			int subValCount = (int)(maxVal / 5f) + 1;

			List<UILabel> labels = new List<UILabel>();
			float[] values = new float[subValCount];

			for( int i = 0; i < subValCount; i++ )
			{
				values[i] = ( (float)i / (subValCount - 1) ) * maxVal;

				labels.Add( new UILabel(new RectangleF(0, yFrame.Height - ( (float)i / (subValCount - 1) ) * yFrame.Height - LabelHeight / 2, LeftPadding-AxisPadding*2, LabelHeight)) );
				labels[i].Font = UIFont.SystemFontOfSize(14f);
				labels[i].TextAlignment = UITextAlignment.Right;
				labels[i].TextColor = JVShared.Instance.GetUserTintColor();
				labels[i].Text = values[i].ToString("0.#");
				AddSubview(labels[i]);
			}

			return labels;
		}

		List<UIView> DrawYAxisSublines(RectangleF xFrame, RectangleF yFrame, float maxVal)
		{
			int subValCount = (int)(maxVal / 5f) + 1;

			List<UIView> lines = new List<UIView>();

			for( int i = 0; i < subValCount; i++ )
			{
				lines.Add( new UIView(new RectangleF(new PointF(xFrame.X, yFrame.Height - ( (float)i / (subValCount - 1) ) * yFrame.Height), xFrame.Size)) );
				lines[i].BackgroundColor = UIColor.FromWhiteAlpha(0f, 0.1f);
				AddSubview(lines[i]);
				SendSubviewToBack(lines[i]);
			}

			return lines;
		}

		void ClearBarsInGraph()
		{
			for( int i = 0; i < BarGraphViews.Count; i++ )
			{
				BarGraphViews[i].RemoveFromSuperview();
			}

			for( int i = 0; i < XAxisLabels.Count; i++ )
			{
				XAxisLabels[i].RemoveFromSuperview();
			}

			for( int i = 0; i < YAxisLabels.Count; i++ )
			{
				YAxisLabels[i].RemoveFromSuperview();
			}

			for (int i = 0; i < YAxisSublines.Count; i++ )
			{
				YAxisSublines[i].RemoveFromSuperview();
			}

			BarGraphViews.Clear();
			XAxisLabels.Clear();
			YAxisLabels.Clear();
			YAxisSublines.Clear();
		}

		float GetLargestFloat(float[] array)
		{
			float largestNumber = 0f;
			for( int i = 0; i < array.Length; i++ )
			{
				if( array[i] > largestNumber )
				{
					largestNumber = array[i];
				}
			}
			return largestNumber;
		}

	}
}
