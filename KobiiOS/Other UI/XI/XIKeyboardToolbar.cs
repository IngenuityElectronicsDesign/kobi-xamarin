using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreAnimation;
using MonoTouch.UIKit;
using System.Drawing;

namespace KobiiOS
{
	public class XIKeyboardToolbar : UIToolbar
	{
		private UIBarButtonItem DoneButton;
		private UIBarButtonItem NextButton;
		private UIBarButtonItem PrevButton;

		public XIKeyboardToolbar(RectangleF frame, UIView view, UITextField nextField, UITextField prevField ) : base(frame)
		{
			BarStyle = UIBarStyle.Default;
			TintColor = UIColor.White;
			BarTintColor = JVShared.Instance.SystemTintColor;

			PrevButton = new UIBarButtonItem(UIImage.FromFile("ButtonBarArrowLeft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Prev Button Pressed
				{
					if( prevField != null )
					{
						prevField.BecomeFirstResponder();
					}
				});
			if( prevField == null )
			{
				PrevButton.Enabled = false;
			}

			UIBarButtonItem fixedSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			fixedSpacer.Width = 23;

			NextButton = new UIBarButtonItem(UIImage.FromFile("ButtonBarArrowRight.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Prev Button Pressed
				{
					if( nextField != null )
					{
						nextField.BecomeFirstResponder();
					}
				});
			if( nextField == null )
			{
				NextButton.Enabled = false;
			}

			UIBarButtonItem flexSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);

			DoneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done);
			DoneButton.Clicked += (sender, e) => // Done Button Pressed
			{
				view.EndEditing(true);
			};

			SetItems(new UIBarButtonItem[] { PrevButton, fixedSpacer, NextButton, flexSpacer, DoneButton }, true);
		}
	}
}

