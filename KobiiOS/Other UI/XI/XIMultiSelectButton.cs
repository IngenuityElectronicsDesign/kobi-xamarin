using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreAnimation;
using MonoTouch.UIKit;
using System.Drawing;

namespace KobiiOS
{
	public interface IXIMultiSelectButtonDelegate
	{
		void IndexSetDidChange(XIMultiSelectButton control, HashSet<int> indexSet);
	}

	public class XIMultiSelectButton : UIView
	{
		public UIButton[] SelectionButtonsList { get; private set; }
		public HashSet<int> SelectedIndexSet { get; private set; }
		public IXIMultiSelectButtonDelegate Delegate { get; set; }

		public XIMultiSelectButton(RectangleF frame, HashSet<int> indexSet, string[] names) : base(frame)
		{
			SelectedIndexSet = new HashSet<int>();
			foreach( int index in indexSet )
			{
				SelectedIndexSet.Add(index);
			}

			SelectionButtonsList = new UIButton[names.Length];

			for( int i = 0; i < names.Length; i++ )
			{
				float ButtonWidth = (int)Frame.Width/names.Length;

				UIButton button = new UIButton(new RectangleF( i*ButtonWidth, 0, ButtonWidth, Frame.Height ));
				button.SetTitle(names[i], UIControlState.Normal);
				button.SetTitle(names[i], UIControlState.Selected);
				button.SetTitle(names[i], UIControlState.Highlighted);
				button.SetTitleColor(JVShared.Instance.SystemTintColor, UIControlState.Normal);
				button.SetTitleColor(UIColor.White, UIControlState.Selected);
				button.Font = UIFont.SystemFontOfSize(14f);

				button.Selected = SelectedIndexSet.Contains(i);
				button.Tag = i;

				UIBezierPath maskPath = UIBezierPath.FromRect(button.Bounds);

				if( i == 0 && names.Length-1 == 0)			// Only Button
				{
					maskPath = UIBezierPath.FromRoundedRect( button.Bounds, 4f );
				}
				else if( i == 0 && i != names.Length-1)		// Left Edge Button
				{
					maskPath = UIBezierPath.FromRoundedRect( button.Bounds, (UIRectCorner.TopLeft | UIRectCorner.BottomLeft), new SizeF(4f, 4f) );
				}
				else if( i != 0 && i == names.Length-1 )	// Right Edge Button
				{
					maskPath = UIBezierPath.FromRoundedRect( button.Bounds, (UIRectCorner.TopRight | UIRectCorner.BottomRight), new SizeF(4f, 4f) );
				}

				CAShapeLayer maskLayer = new CAShapeLayer();
				maskLayer.Frame = button.Bounds;
				maskLayer.Path = maskPath.CGPath;
				maskLayer.FillColor = ( button.Selected ? JVShared.Instance.SystemTintColor.CGColor : UIColor.White.CGColor );
				maskLayer.StrokeColor = ( button.Selected ? UIColor.Black.CGColor : JVShared.Instance.SystemTintColor.CGColor );
				maskLayer.LineWidth = 1f;

				button.Layer.AddSublayer(maskLayer);

				button.TouchUpInside += (sender, e) => // Button Pressed
				{
					if( button.Selected )	// Button is currently selected, therefore we want to deselect it and remove it when pressed
					{
						button.Selected = false;
						maskLayer.FillColor = UIColor.White.CGColor;
						maskLayer.StrokeColor = JVShared.Instance.SystemTintColor.CGColor;
						SendSubviewToBack(button);
						SelectedIndexSet.Remove(button.Tag);
					}
					else					// Button is currently deselected, therefore we want to select it and add it when pressed
					{
						button.Selected = true;
						maskLayer.FillColor = JVShared.Instance.SystemTintColor.CGColor;
						maskLayer.StrokeColor = UIColor.Black.CGColor;
						BringSubviewToFront(button);
						SelectedIndexSet.Add(button.Tag);
					}
					Delegate.IndexSetDidChange(this, SelectedIndexSet);
				};

				AddSubview(button);
				SelectionButtonsList[i] = button;
			}

			foreach( UIButton button in SelectionButtonsList )
			{
				if( button.Selected )
				{
					BringSubviewToFront(button);
				}
				else
				{
					SendSubviewToBack(button);
				}
			}
		}
	}
}

