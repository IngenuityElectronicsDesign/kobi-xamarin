using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace KobiiOS
{
    public interface IMultiSelectSegmentedControlDelegate
    {
        void DidChangeValue(MultiSelectSegmentedControl control, int index, bool didChange);
        void IndexSetDidChange(MultiSelectSegmentedControl control, NSIndexSet indexSet);
    }

    public class MultiSelectSegmentedControl : UISegmentedControl
    {

    }
}