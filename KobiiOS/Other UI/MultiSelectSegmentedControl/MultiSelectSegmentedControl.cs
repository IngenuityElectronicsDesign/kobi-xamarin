using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace KobiiOS
{
    public interface IMultiSelectSegmentedControlDelegate
    {
        void DidChangeValue(MultiSelectSegmentedControl control, int index, bool didChange);
        void IndexSetDidChange(MultiSelectSegmentedControl control, NSIndexSet indexSet);
    }

    public class MultiSelectSegmentedControl : UISegmentedControl, IComparer<UIView>
    {
        private bool m_HasBeenDrawn;
		private List<UIView> m_SortedSegments;

        private NSMutableIndexSet m_SelectedIndices = new NSMutableIndexSet();
        public NSMutableIndexSet SelectedIndices
        {
            get
            {
                return m_SelectedIndices;
            }
            set
            {
				var index = value.FirstIndex;
				Console.WriteLine("1 Index: {0}", index);
                
                bool done = false;
                m_SelectedIndices.Clear();
                while (!done)
                {
					Console.WriteLine("2 Index: {0}", index);
                    if (index < NumberOfSegments)
                        m_SelectedIndices.Add(index);
                    index = value.IndexGreaterThan(index);
                    if (index == 2147483647)
                    {
                        done = true;
                    }
                }
                SelectSegmentsOfSelectedIndices();
            }
        }
        public IMultiSelectSegmentedControlDelegate Delegate { get; set; }

        public MultiSelectSegmentedControl(RectangleF frame) : base(frame)
		{
			OnInit();
		}

        public void SelectAllSegments(bool select)
        {
            SelectedIndices.Clear();
            if (select)
            {
                for (int i = 0; i < NumberOfSegments; i++)
                {
                    SelectedIndices.Add((uint)i);
                }
            }
        }

        private void InitSortedSegmentsArray()
        {
			m_SortedSegments = new List<UIView>(Subviews);
			m_SortedSegments.Sort(this);
        }

        private void SelectSegmentsOfSelectedIndices()
        {
			base.SelectedSegment = -1; // UISegmentedControlNoSegment; - Couldn't find porting option for enum.
            for (int i = 0; i < NumberOfSegments; i++)
            {
				m_SortedSegments[i].TintColor = SelectedIndices.Contains((uint)i) ? JVShared.Instance.SystemTintColor : UIColor.Black;
                // Create a slight checkerboard pattern
                if (i % 2 == 1)
                {
                    UIColor tintColor = JVShared.Instance.SystemTintColor;
                    System.Single h, s, b, a;
					m_SortedSegments[i].TintColor.GetHSBA(out h, out s, out b, out a);
                    tintColor = UIColor.FromHSBA(h, s, b + 0.025f, a);
                    m_SortedSegments[i].TintColor = tintColor;
                }
            }
            // TODO: bug = deselect all, when the last tapped segment has a selected segment on its right - right edge of the last tapped segment stays selected
            // in fact, deselecting a single segment that has a selected segment on its right - causes the right segment to widen left
        }

        private new void ValueChanged(object t, EventArgs args)
        {
            int tappedSegmentIndex = SelectedSegment;
            if (SelectedIndices.Contains((uint)tappedSegmentIndex))
            {
                SelectedIndices.Remove((uint)tappedSegmentIndex);
            }
            else
            {
                SelectedIndices.Add((uint)tappedSegmentIndex);
            }
            if (Delegate != null)
            {
                Delegate.IndexSetDidChange(this, SelectedIndices);
            }
            SelectSegmentsOfSelectedIndices();
        }

        #region Initialization

        private void OnInit()
        {
            m_HasBeenDrawn = false;
            AddTarget(new EventHandler(ValueChanged), UIControlEvent.ValueChanged);
            SelectedIndices.Clear();
        }

        #endregion

        #region Overrides

        public override void DrawRect(System.Drawing.RectangleF area, UIViewPrintFormatter formatter)
        {
            if (!m_HasBeenDrawn)
            {
                InitSortedSegmentsArray();
                m_HasBeenDrawn = true;
                SelectSegmentsOfSelectedIndices();
            }
            base.DrawRect(area, formatter);
        }

        public override bool Momentary
        {
            get
            {
                return base.Momentary;
            }
            set
            {
                // Won't work with momentary selection
                //base.Momentary = value;
            }
        }

        public override int SelectedSegment
        {
            get
            {
                return base.SelectedSegment;
            }
            set
            {
                if (SelectedIndices == null)
                {
                    base.SelectedSegment = value;
                }
                if (SelectedSegment == -1)
                {
                    SelectedIndices.Clear();
                }
                else
                {
                    SelectedIndices.Clear();
                    SelectedIndices.Add((uint)value);
                }
            }
        }



        private int SelectedSegmentIndex()
        {
            if (SelectedIndices.Count == 0)
                return -1;
            // TODO : Check if this is expected behaviour.
            // The Hashset is unsorted, so First will return the first number added
            // to the set rather than the first in numerical order.
            return (int)SelectedIndices.FirstIndex;
        }

        private void OnInsertSegmentAtIndex(int segment)
        {
            SelectedIndices.ShiftIndexes((uint)segment, 1);
            InitSortedSegmentsArray();
        }

        public override void InsertSegment(string title, int pos, bool animated)
        {
            base.InsertSegment(title, pos, animated);
            OnInsertSegmentAtIndex(pos);
        }

        public override void InsertSegment(UIImage image, int pos, bool animated)
        {
            base.InsertSegment(image, pos, animated);
            OnInsertSegmentAtIndex(pos);
        }

        public override void RemoveSegmentAtIndex(int segment, bool animated)
        {
            // bounds check to avoid exceptions
            int n = NumberOfSegments;
            if (n == 0) return;
            if (segment >= n) segment = n - 1;

            // Store multiple selection
            NSMutableIndexSet newSelectedIndices = new NSMutableIndexSet(SelectedIndices);
            newSelectedIndices.Add((uint)segment);
            newSelectedIndices.ShiftIndexes((uint)segment, -1);

            base.SelectedSegment = segment;
            RemoveSegmentAtIndex(segment, animated);

            // Restore multiple selection after animation ends
            SelectedIndices = newSelectedIndices;
            double delayInSeconds = animated ? 0.45 : 0.0;

            var popTime = new DispatchTime(DispatchTime.Now, (long)(delayInSeconds * 1000000000 /* NSEC_PER_SEC */));
            DispatchQueue.MainQueue.DispatchAfter(popTime,
                new NSAction(() =>
                {
                    InitSortedSegmentsArray();
                    SelectSegmentsOfSelectedIndices();
                }));
        }

        public override void RemoveAllSegments()
        {
            base.SelectedSegment = 0;
            base.RemoveAllSegments();
            SelectedIndices.Clear();
            m_SortedSegments = null;
        }

        #endregion // Overrides

        #region IComparer<UIView>
        public int Compare(UIView x, UIView y)
        {
            // NOTE : this comparer will require testing.
            // Pretty sure I ported correctly though - JM.
            var x1 = x.Frame.Location.X;
            var x2 = y.Frame.Location.X;
            return (int)((x1 > x2 ? 1 : 0) - (x1 < x2 ? 1 : 0));
        }
        #endregion // IComparer<UIView>
    }
}